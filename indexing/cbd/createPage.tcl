# Open the connection to SQL Server
set handle [sybconnect xls xls data1]

# Process the List of Spreadsheets
sybsql $handle "select id,title from ss where source=42 order by title"
sybnext $handle {
		puts [format "<LI><A HREF=\"/cgi-bin/spreaddetail.exe?id=%s\">%s</A>" @1 @2]
}

# Close the connection to SQL Server
sybclose $handle
