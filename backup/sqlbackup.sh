TAPENAME=$1

# Backup command file name
COMMANDFILE=command.tmp
rm -f $COMMANDFILE

# The server containing the dba database
DBASERVER=`$XLS/src/scripts/utility/dbserver.sh dba`
DBAUSER=dba
DBAPASSWORD=dba

isql /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} -n >${COMMANDFILE} << EOF

set nocount on
go

declare @dbname varchar(32)
declare @message varchar(255)
declare @dbcount int

select @dbcount=0

DECLARE db_cursor SCROLL CURSOR for
select b.name from dbtapedatabase d, dbtape t, dbase b
where
t.name = "${TAPENAME}"
and
d.dbtape = t.id
and
d.dbase = b.id

OPEN db_cursor

/* Process the first one specially */
fetch next from db_cursor into @dbname 
if (@@fetch_status = 0)
begin
	PRINT "PRINT 'Initializing tape ${TAPENAME}'"
	select @message="PRINT 'Adding database " + @dbname + "'"
	PRINT @message
	select @message="dump database "+@dbname+" to tape0 with init,nounload"
	PRINT @message
	select @dbcount=@dbcount+1
end

fetch next from db_cursor into @dbname 
while (@@fetch_status = 0)
begin
	select @message="PRINT 'Adding database " + @dbname + "'"
	PRINT @message
	select @message="dump database "+@dbname+" to tape0 with noinit,nounload"
	PRINT @message
	fetch next from db_cursor into @dbname
	select @dbcount=@dbcount+1
end

CLOSE db_cursor
DEALLOCATE db_cursor

select @message="PRINT '"+CONVERT(varchar(12),@dbcount)+" databases added to tape ${TAPENAME}'"
PRINT @message
go

EOF
if [ $? != 0 ]
then
	echo "Error building backup command file"
	exit 1
fi

# Execute the command file
isql /Ubackupdbo /Pbackupdbo < ${COMMANDFILE}
if [ $? != 0 ]
then
	echo "Error executing backup command file"
	exit 1
fi

# Remove the temporary command file
rm -f ${COMMANDFILE}

# Update the dba database with the last backup date for the databases
isql /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} -n << EOF

set nocount on
go

declare @dbname varchar(32)
declare @dbservername varchar(32)

DECLARE db_cursor SCROLL CURSOR for
select b.name, s.name from dbtapedatabase d, dbtape t, dbase b, dbserver s
where
t.name = "${TAPENAME}"
and
d.dbtape = t.id
and
d.dbase = b.id
and
t.dbserver = s.id

OPEN db_cursor

fetch next from db_cursor into @dbname,@dbservername 
while (@@fetch_status = 0)
begin
	exec logDBBackup "${TAPENAME}", @dbname, @dbservername
	fetch next from db_cursor into @dbname,@dbservername
end

CLOSE db_cursor
DEALLOCATE db_cursor

EOF
if [ $? != 0 ]
then
	echo "Error updating backup status in DBA database"
	exit 1
fi
