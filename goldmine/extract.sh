# Script to extract client data for import into Goldmine
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
if [ $# -lt 3 ]
then
	echo "Usage extract.sh xls_server xls_login xls_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3

tmpfile=gmextract.txt

# extract the charter of accounts
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} /w1000 /s"	" /n /h-1 > ${tmpfile} << HERE
SET NOCOUNT ON
select 
	a.description
from
	chartOfAccounts a
order by a.description asc
HERE

# trim spaces
sed -f massage.sed < ${tmpfile} > chartOfAccounts.tab

# extract the content providers
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} /w1000 /s"	" /n /h-1 > ${tmpfile} << HERE
SET NOCOUNT ON
select 
	i.name
from
	ip i
where
	i.active = 1
order by i.name asc
HERE

# trim spaces
sed -f massage.sed < ${tmpfile} > contentProvider.tab

# extract the currencies
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} /w1000 /s"	" /n /h-1 > ${tmpfile} << HERE
SET NOCOUNT ON
select 
	c.description
from
	currency c
order by c.description asc
HERE

# trim spaces
sed -f massage.sed < ${tmpfile} > currency.tab

# Extract the account information
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} /w1000 /s"	" /n /h-1 > ${tmpfile} << HERE
SET NOCOUNT ON
select 
	a.id,
	a.name,
	a.contact,
	a.address1,
	a.address2,
	a.address3,
	a.city,
	s.name,
	c.name,
	a.postal_code,
	a.tel_country+' '+a.tel_area+' '+a.tel_number,
	a.fax_country+' '+a.fax_area+' '+a.fax_number,
	r.first_name+' '+r.last_name
from 
	account a, state_prov s, country c, salesperson r
where
	a.state_prov *= s.id
	and
	a.country *= c.id
	and
	a.id in (select distinct account from charge where end_date >= DATEADD(month,-3,GETDATE()))
	and
	a.salesperson *= r.id
HERE
if [ $? -ne 0 ]
then
	echo "Error extracting account information, exiting"
	exit 1
fi

# trim spaces
sed -f massage.sed < ${tmpfile} > account.tab

# Extract the charge information
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} /w1000 /s"	" /n /h-1 > ${tmpfile} << HERE
SET NOCOUNT ON
select 
	c.account,
	c.description,
	CONVERT (varchar(12),c.start_date,107),
	CONVERT (varchar(12),c.end_date,107),

	CASE 
	WHEN c.how_billed = 1
	THEN c.amount / 12
	WHEN c.how_billed = 2
	THEN c.amount / 3
	WHEN c.how_billed = 3
	THEN c.amount / 1
	WHEN c.how_billed = 8
	THEN c.amount / 6
	WHEN c.how_billed = 5
	THEN
		CASE WHEN DATEDIFF(month,c.start_date,c.end_date)=0 THEN c.amount ELSE c.amount/DATEDIFF(month,c.start_date,c.end_date) END
	END,

	i.name,
	r.first_name+' '+r.last_name,
	a.description,
	x.description
from
	charge c, ip i, salesperson r, chartOfAccounts a, currency x
where
	c.ip *= i.id
	and
	c.ddl_salesperson *= r.id
	and
	c.araccount *= a.araccount
	and
	c.how_billed in (1,2,3,5,8)
	and
	c.end_date >= DATEADD(month,-3,GETDATE()) 
	and
	c.currency *= x.code
HERE

# trim spaces
sed -f massage.sed < ${tmpfile} > product.tab

# Extract the contact information
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} /w1000 /s"	" /n /h-1 > ${tmpfile} << HERE
SET NOCOUNT ON
select 
	u.id,
	u.account,
	u.login,
	u.first_name+' '+u.middle_name+' '+u.last_name,
	u.honorific,
	u.company,
	u.address1,
	u.address2,
	u.address3,
	u.city,
	s.name,
	c.name,
	u.postal_code,
	u.tel_country+' '+u.tel_area+' '+u.tel_number,
	u.fax_country+' '+u.fax_area+' '+u.fax_number,
	u.email,
	r.first_name+' '+r.last_name,
	u.title
from
	users u, state_prov s, country c, salesperson r
where
	u.state_prov *= s.id
	and
	u.country = c.id
	and
	u.salesrep *= r.id
	and
	(u.demo_flag is null or u.demo_flag="")
	and
	u.account in (select distinct account from charge where end_date >= DATEADD(month,-3,GETDATE()))
	and
	u.account not in (1747)
HERE

# trim spaces
sed -f massage.sed < ${tmpfile} > contact.tab


# Clean up
rm -f ${tmpfile}

exit 0
