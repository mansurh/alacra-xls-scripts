XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: $0 database"
	exit 1
fi

storeflag=""
if [ $# -gt 1 ]
then
  storeflag=$2
fi

emaillist="accounting@opus.com,tami.sitman@opus.com"

database=$1
# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
dataserver=`get_xls_registry_value ${database} Server`

regkey=billing
ftpserver=`get_xls_registry_value ${regkey} Server 6`
ftpuser=`get_xls_registry_value ${regkey} User 6`
ftppass=`get_xls_registry_value ${regkey} Password 6`

SCRIPTDIR=$XLS/src/scripts/billing

main() {

# Remove any old spreadsheets
rm -f *.xl{s,w}

# Remove any old rtf files
rm -f *.rtf

# Figure out what the last month and year were
YEAR=`date +%Y`
MONTH=`date +%m | sed -e "s/^0//"`
if [ ${MONTH} = "1" ]
then
	YEAR=`expr $YEAR - 1`
	MONTH="12"
else
	MONTH=`expr $MONTH - 1`
fi
if [ ${MONTH} = "1" ]
then
	MONTH="January"
fi
if [ ${MONTH} = "2" ]
then
	MONTH="February"
fi
if [ ${MONTH} = "3" ]
then
	MONTH="March"
fi
if [ ${MONTH} = "4" ]
then
	MONTH="April"
fi
if [ ${MONTH} = "5" ]
then
	MONTH="May"
fi
if [ ${MONTH} = "6" ]
then
	MONTH="June"
fi
if [ ${MONTH} = "7" ]
then
	MONTH="July"
fi
if [ ${MONTH} = "8" ]
then
	MONTH="August"
fi
if [ ${MONTH} = "9" ]
then
	MONTH="September"
fi
if [ ${MONTH} = "10" ]
then
	MONTH="October"
fi
if [ ${MONTH} = "11" ]
then
	MONTH="November"
fi
if [ ${MONTH} = "12" ]
then
	MONTH="December"
fi

if [ "${storeflag}" = "1" ]
then
  filter="isstore"
  extraparam="-f 1"
else
  filter="isalacra"
  extraparam=""
fi

archfile=IP-${MONTH}-${YEAR}.zip

# Execute the ipgen program
ipgen -m ${MONTH} -y ${YEAR} -U ${logon} -P ${password} -S ${dataserver} ${extraparam}
if [ $? != 0 ]
then
	echo "Error generating statements, aborting"
	return 1
fi

# Get the list of email recipients
bcp "select CASE compress WHEN 1 THEN 'compress' ELSE 'nocompress' END, i.id,i.name, m.email from ip i, ipstatementmail m where i.id=m.ip and m.${filter} is not null and i.active > 0 order by i.id asc" queryout mailcommand /U${logon} /P${password} /S${dataserver} /c /t"|" /CRAW
if [ $? != 0 ]
then
	echo "Error getting list of mail addresses, aborting"
	return 1
fi

rm -f mailcommand.sh
IFS="|"
while read compress ip vendor email
do
  filename="IP$ip-$MONTH-$YEAR.xlw"
  #echo $filename
  if [ -s $filename ]
  then
    if [ $compress == "compress" ]
    then
	echo "rm -f usage.zip; zip usage.zip $filename" >> mailcommand.sh
        filename="usage.zip"
    fi
    echo "blat.exe -body \"Usage Report from Alacra\" -attach $filename -subject \"$vendor Usage Report from Alacra - $MONTH - $YEAR\" -to \"$email\" -from statements@alacra.com -nomps -mime; sleep 30s" >> mailcommand.sh

  else
    echo "blat.exe -body \"No Usage for $MONTH, $YEAR from Alacra\" -subject \"$vendor Usage Report from Alacra - $MONTH - $YEAR\" -to \"$email\" -from statements@alacra.com -nomps -mime; sleep 30s" >> mailcommand.sh
  fi
done < mailcommand 

chmod a+x mailcommand.sh

# Execute the mail command
./mailcommand.sh
if [ $? != 0 ]
then
	echo "Error mailing, aborting"
	return 1
fi

# archive the results
#tar cvfz ${archfile} *.xl{s,w} *.rtf
zip ${archfile} *.xl{s,w} *.rtf
if [ $? != 0 ]
then
	echo "zip error, aborting"
	return 1
fi

# FTP the results over to the billing archive
rm -f ipstatement.ftp
echo "user ${ftpuser} ${ftppass}" > ipstatement.ftp
echo "binary" >> ipstatement.ftp
if [ ${storeflag} -eq 1 ]
then
	echo "cd \"/billing/IP Store/\"" >> ipstatement.ftp
else
	echo "cd /billing/IP/" >> ipstatement.ftp
fi
echo "put ${archfile}" >> ipstatement.ftp
echo "mls *.zip filelist" >> ipstatement.ftp
echo "quit" >> ipstatement.ftp
ftp -i -n -s:ipstatement.ftp ${ftpserver}
if [ $? != 0 ]
then
	echo "FTP error, aborting"
	return 1
fi

grep -q ${archfile} filelist
if [ $? -ne 0 ]
then
	echo "$archfile not found on ${ftpserver} after ftp"
	return 1
fi
}

logfile=ipstatement`date +%y%m`.log
(main) > ${logfile} 2>&1
if [ $? -ne 0 ]
then
	blat $logfile -t administrators@alacra.com -s "IP Statement failed"
	exit 1
else
	blat -body "IP Statements generated" -attach ${archfile} -t ${emaillist} -s "IP Statements ${filter} for ${MONTH}"
fi

exit 0
