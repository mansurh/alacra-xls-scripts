print 'Current charges for - ' + convert(char(9), @currentdate, 1)

print ''
select distinct a.id"ID", a.name"Name", co.name"Country", c.currency"Currency", 
c.amount"Amount", b.name"Billing cycle", convert(char(9),c.start_date, 1)"Start Date", convert(char(9), c.end_date, 1)"End Date",
convert(char(100),c.description)"Description", c.araccount"Araccount"
from account a, charge c, country co, billable_type b
where a.id=c.account 
and co.id=a.country
and b.id=c.how_billed
and c.end_date>=@currentdate
go

