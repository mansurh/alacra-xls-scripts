# Split the command line arguments.
#	1 = month 
#	2 = year
#	3 = first invoice number
#	4 = server
#	5 = user
#	6 = password
set arglist [split $argv]
if {[llength $arglist] != 6} {
	puts "Usage: genbills.tcl month year invoice server user password"
	exit
}
set month [lindex $arglist 0]
set year [lindex $arglist 1]
set invoice [lindex $arglist 2]
set server [lindex $arglist 3]
set user [lindex $arglist 4]
set password [lindex $arglist 5]

# Open the connection to SQL Server
set handle [sybconnect $user $password $server]

# Select the active accounts
sybsql $handle "select id,name from account where id in (select a.id from account a, charge c where c.account = a.id and c.start_date < DATEADD (month, 1, \"$month 1, $year\") and c.end_date >= \"$month 1, $year\") order by id"

sybnext $handle {

	puts "Generating invoice for @2,  account # @1"

	exec /users/xls/debug-bin/billgen.exe -a @1 -m $month -y $year -U $user -P $password -S $server -i $invoice > ${invoice}a.xls 2>${invoice}b.xls
	incr invoice

}

# Close the connection to SQL Server
sybclose $handle
