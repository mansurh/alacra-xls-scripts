XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Parse the command line arguments
#	1 - database
if [ $# -lt 1 ]
then
	echo "Usage: $0 database"
	exit 1
fi

database=$1
# Save the runstring parameters
logon=`get_xls_registry_value ${database} User`
password=`get_xls_registry_value ${database} Password`
dataserver=`get_xls_registry_value ${database} Server`


# Remove any old spreadsheets
rm -f IP*.xls

# Remove any old rtf files
rm -f IP*.rtf

# Figure out what the last month and year were
YEAR=`date +%Y`
MONTH=`date +%m | sed -e "s/^0//"`
if [ ${MONTH} = "1" ]
then
	YEAR=`expr $YEAR - 1`
	MONTH="12"
else
	MONTH=`expr $MONTH - 1`
fi
if [ ${MONTH} = "1" ]
then
	MONTH="January"
fi
if [ ${MONTH} = "2" ]
then
	MONTH="February"
fi
if [ ${MONTH} = "3" ]
then
	MONTH="March"
fi
if [ ${MONTH} = "4" ]
then
	MONTH="April"
fi
if [ ${MONTH} = "5" ]
then
	MONTH="May"
fi
if [ ${MONTH} = "6" ]
then
	MONTH="June"
fi
if [ ${MONTH} = "7" ]
then
	MONTH="July"
fi
if [ ${MONTH} = "8" ]
then
	MONTH="August"
fi
if [ ${MONTH} = "9" ]
then
	MONTH="September"
fi
if [ ${MONTH} = "10" ]
then
	MONTH="October"
fi
if [ ${MONTH} = "11" ]
then
	MONTH="November"
fi
if [ ${MONTH} = "12" ]
then
	MONTH="December"
fi

# Get the list of ALM Accounts
isql /n /w500 /h-1 /s"|" /U${logon} /P${password} /S${dataserver} > maillist << HERE
set nocount on
select distinct u.account from users u where 
u.login like 'lmi-%'
and
u.terminate > DATEADD(month, -2, GETDATE())
and
u.demo_flag IS NULL
HERE
if [ $? != 0 ]
then
	echo "Error getting list of mail addresses, aborting"
	exit 1
fi

# Loop through the list of accounts and extract the usage
for a in `cat maillist`
do
	echo "Generating ALM Usage for account # ${a}"
isql /n /w500 /h-1 /s"|" /U${logon} /P${password} /S${dataserver} > ALM-${a}-${YEAR}-${MONTH}.txt << HERE
set nocount on
select 
p.first_name 'First Name',
p.last_name 'Last Name',
a.name 'Firm',
u.description 'Download',
u.access_time 'Date'
from usage u, users p, account a
where
u.access_time >= 'September 1, 2007'
and
u.userid = p.id
and
p.account = a.id
and
a.id = ${a}
HERE

done

# Get the email distribution list
#isql /n /w500 /h-1 /s"|" /U${logon} /P${password} /S${dataserver} > almmailcommand << HERE
#set nocount on
#select m.account,"${MONTH}","${YEAR}",a.name,"${MONTH}","${YEAR}",m.email from account a, almstatementmail m where a.id=m.account
#order by #m.account asc
#HERE
#if [ $? != 0 ]
#then
	#echo "Error getting list of mail addresses, aborting"
	#exit 1
#fi
isql /n /w500 /h-1 /s"|" /U${logon} /P${password} /S${dataserver} > almmailcommand << HERE
set nocount on
select 3859,"${YEAR}","${MONTH}",'my firm',"${MONTH}","${YEAR}",'michael.angle@alacra.com'  
HERE
if [ $? != 0 ]
then
	echo "Error getting list of mail addresses, aborting"
	exit 1
fi


# Convert the list into shell commands
sed -f almmailcommand.sed < almmailcommand >almmailcommand.sh
if [ $? != 0 ]
then
	echo "Error processing list of mail addresses, aborting"
	exit 1
fi

exit 1

# Execute the mail commands
./almmailcommand.sh
if [ $? != 0 ]
then
	echo "Error mailing, aborting"
	exit 1
fi

# Clean up
rm -f ALM*.txt
rm -f almmailcommand
rm -f almmailcommand.sh
rm -f maillist

exit 0
