

select "Merrill Lynch Global Usage"
select 
	i.ipcode, p.account, p.login, u.project,u.project2,u.project3,u.project4, u.project5,u.access_time,u.description,u.list_price 
from 
	usage u, users p, ip i
where 
	userid in (select id from users where account in (select id from account where name like "%Merrill Lynch%") and demo_flag is NULL)
	and 
	access_time >= "September 1, 2003" 
	and 
	access_time < "October 1, 2003"
	and 
	no_charge_flag is null
	and 
	u.userid=p.id
	and
	u.ip = i.id

order by 
	i.ipcode, p.account, p.login, u.ip, u.access_time asc



