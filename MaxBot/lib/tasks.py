import copy
import functools
import os
import pprint
import pyodbc
import sys
import time
import zipfile
import zlib
import subprocess as sproc
from .colors import colors
from .print import cprint
from .sourcesafe import ss
from .globals import globals, whereisenv, identityenv
from .yaml_parser import replace_tokens
from .sqlfile import SqlFile
from .configs import get_ace_pods, get_registry_data_server, get_cred_from_user, creds


class Manifest(list):
    def set_db_name(self, db_name):
        self.db_name = db_name
    def set_copy_type(self, copy_type):
        self.copy_type = copy_type
    def any_failure(self):
        for sqlf in self:
            if hasattr(sqlf, 'failure') and sqlf.failure:
                return True
        return False
    def print_failures(self):
        for sqlf in self:
            if hasattr(sqlf, 'failure') and sqlf.failure:
                cprint.print(sqlf.relpath)
    def print_successes(self):
        for sqlf in self:
            if not (hasattr(sqlf, 'failure') and sqlf.failure):
                cprint.print(sqlf.relpath)
    def __str__(self):
        return "db: {0}\ntype:{1}\n{2}".format(self.db_name, self.copy_type, super().__str__())


class Task:
    def __init__(self, cmd, opts):
        self.cmd = cmd
        self.opts = opts

    def init(self):
        pass
        
    def execute(self):
        if globals.args.verbose:
            # print(colors.PURPLE + "Command: ", self.cmd + colors.END)
            cprint.info("Command:", self.cmd)
            # print(colors.PURPLE + "Parameters: " + colors.END, end="")
            cprint.info("Parameters: ", end='')
            pprint.pprint(self.opts)

    
class Wait(Task):
    def execute(self):
        super().execute()
        if not globals.args.no_wait:
            input(colors.GREEN + self.opts + colors.END)


class Sleep(Task):
    def execute(self):
        super().execute()
        if not globals.args.no_wait:
            cprint.info("Sleeping for {0} seconds".format(self.opts))
            time.sleep(float(self.opts))


class RunSql(Task):
    def __init__(self, cmd, opts):
        super().__init__(cmd, opts)
        self.sub_manifests = []
        self.raw_manifest = opts["manifest"]

    def init(self):
        self.build_sql_files()
    
    def get_cred_from_user(self, db_name, copy_type):
        get_cred_from_user(db_name, 'User')
        get_cred_from_user(db_name, 'Pass')
        if copy_type == 'MASTER':
            get_cred_from_user(db_name, 'Master')

        ver_key = "{0}_Verified".format(db_name)
        if not creds.get(ver_key, False):
            if self.verify_cred(db_name, copy_type):
                creds[ver_key] = True
            else:
                creds.pop("{0}_{1}".format(db_name, 'User'), None)
                creds.pop("{0}_{1}".format(db_name, 'Pass'), None)
                creds.pop("{0}_{1}".format(db_name, 'Master'), None)
                self.get_cred_from_user(db_name, copy_type)
    
    def verify_cred(self, db_name, copy_type):
        if copy_type == 'MASTER':
            servers = [creds[db_name + "_Master"]]
        else:
            conn = pyodbc.connect('DRIVER={{SQL Server}};SERVER={0};DATABASE=dba2;UID={1};PWD={2}'.format(
                creds["dba_server"], creds["dba_user"], creds["dba_pass"]
            ))
            c = conn.cursor()
            c.execute("exec whereis ?, ?", db_name, whereisenv[globals.env])
            servers = [row.server for row in c.fetchall() if row.status != 'offline']

        if len(servers) == 0:
            cprint.warning("WARNING: No online servers for database '{db}' found in {env} environment, so it will not being executed.".format(db=db_name, env=globals.env))
            return True

        server = servers[0]
            
        cmdmsg = 'osql -l10 -b -n -d {database} -U {user} -P {password} -S {server} -I -Q "select \'connect test\'"'.format(
            database=db_name, 
            user=creds[db_name + "_User"], 
            # password=creds[db_name + "_Pass"], 
            password='******',
            server=server
        )
        cmd = ['osql', '-l10', '-b', '-n', '-I', 
               '-d', db_name, 
               '-U', creds[db_name + "_User"],
               '-P', creds[db_name + "_Pass"], 
               '-S', server, 
               '-Q', "select 'connect test'"]
        cprint.declare(cmdmsg)
        if not globals.args.dry_run:
            isql = sproc.Popen(cmd, stdout=sproc.DEVNULL, stderr=sproc.STDOUT)
            # cprint.tee(isql, globals.logfile)
            isql.communicate()
            if isql.returncode != 0:
                cprint.info('Connect test failed, try again')
                return False
        return True

    def build_sql_files(self):
        cprint.info("Getting last {} labelled '{}' with suffix '{}'".format(self.raw_manifest, globals.fromenv, globals.env_suffix))
        relpath = os.path.relpath(self.raw_manifest, '$/xls/src/scripts/packageSQL/')
        reltmpdir = os.path.dirname(relpath)
        abstmpdir = globals.tmpbasedir + reltmpdir
        got = ss.get_file_tmp(self.raw_manifest, globals.fromenv, globals.env_suffix, abstmpdir)
        if not got:
            cprint.error("ss get failed, see above output")
            sys.exit()

        if globals.args.dry_run:
            prompt = "We're in dry-run mode. If you want to edit {0} the time is now. [any key to continue]".format(os.path.join(abstmpdir, os.path.basename(self.raw_manifest)))
            globals.logfile.write(prompt + '\n')
            input(colors.GREEN + prompt + colors.END)

        sqlfiles = []
        mf = os.path.join(globals.tmpbasedir, relpath)
        # print('sqlmanifest path', mf)
        with open(mf, 'r') as mf:
            cprint.info("Getting SQL files labelled '{0}' with suffix '{1}'".format(globals.fromenv, globals.env_suffix))
            for line in mf:
                l = line.strip()
                if len(l) == 0 or l[0] == '#':
                    cprint.print('skipping {0}'.format(l))
                    continue
                sqlf = SqlFile(l)
                sqlf.get_file(globals.fromenv, globals.env_suffix)
                sqlf.validate()
                sqlfiles.append(sqlf)
            cprint.print()

        db_name_prev, copy_type_prev = None, None
        sub_manifest = Manifest()

        for sqlf in sqlfiles:
            if sqlf.db_name == 'ace':
                ace_pods = get_ace_pods()
                for db in ace_pods:
                    self.get_cred_from_user(db, sqlf.copy_type)
            else:
                self.get_cred_from_user(sqlf.db_name, sqlf.copy_type)

            if globals.args.verbose:
                cprint.info("db_name: {0}, copy_type: {1}".format(sqlf.db_name, sqlf.copy_type))
                
            if db_name_prev is not None and (sqlf.db_name != db_name_prev or sqlf.copy_type != copy_type_prev):
                self.sub_manifests.append(sub_manifest)
                sub_manifest = Manifest()

            sub_manifest.append(sqlf)
            sub_manifest.set_db_name(sqlf.db_name)
            sub_manifest.set_copy_type(sqlf.copy_type)

            db_name_prev = sqlf.db_name
            copy_type_prev = sqlf.copy_type

        if len(sub_manifest) > 0:
            self.sub_manifests.append(sub_manifest)

    def execute(self):
        super().execute()
        for m in self.sub_manifests:
            self.exec_sub_manifest(m)

        failed = False
        for m in self.sub_manifests:
            if m.any_failure():
                failed = True
                
        if not globals.args.dry_run:
            if failed:
                # cprint.warning("Failed SQL files' labels not updated. \n{0} label also not updated.".format(self.raw_manifest))
                cprint.warning("The following SQL files failed:")
                for m in self.sub_manifests:
                    m.print_failures()

                if not globals.args.rerun:
                    do_label = 'init'
                    while True:
                        do_label = input(colors.GREEN + "Do you want to label everything to {0} despite the failures? [y/n] ".format(identityenv[globals.env]) + colors.END)
                        if do_label in ('y', 'n'):
                            break
            else:
                do_label = 'y'

            if globals.args.rerun:
                do_label = 'n'

            if do_label == 'y':
                for m in self.sub_manifests:
                    for sqlf in m:
                        ss.label(sqlf.sspath, globals.fromenv, '{0}'.format(identityenv[globals.env]), globals.env_suffix)
                ss.label(self.raw_manifest, globals.fromenv, '{0}'.format(identityenv[globals.env]), globals.env_suffix)

            if failed and do_label == 'y':
                cprint.warning('This is a reminder that the following files failed:')
                for m in self.sub_manifests:
                    m.print_failures()
                print()
                
    def exec_sub_manifest(self, sub_manifest):
        cprint.info("Executing the following SQLs: \n(db = {m.db_name}, type = {m.copy_type})".format(m=sub_manifest))
        # pprint.pprint([sqlf.relpath for sqlf in sub_manifest])
        for sqlf in sub_manifest:
            cprint.special(sqlf.relpath)
        
        if sub_manifest.db_name == 'ace':
            ace_pods = get_ace_pods()
            for db in ace_pods:
                self.exec_sub_manifest_low(sub_manifest, db)
        else:
            self.exec_sub_manifest_low(sub_manifest)

        if sub_manifest.copy_type == 'MASTER':
            if 'wait' in self.opts:
                w = Wait('wait', self.opts["wait"])
                w.execute()
            elif 'sleep' in self.opts:
                s = Sleep('sleep', self.opts["sleep"])
                s.execute()

    def exec_sub_manifest_low(self, manifest, db=None):
        if db is None:
            db = manifest.db_name
        for sqlf in manifest:
            globals.zipfile.write(sqlf.tmppath, compress_type=zipfile.ZIP_DEFLATED)
            file = sqlf.tmppath.replace('/cygdrive/c', 'c:')
            if manifest.copy_type == 'MASTER':
                servers = [creds[db + "_Master"]]
            else:
                conn = pyodbc.connect('DRIVER={{SQL Server}};SERVER={0};DATABASE=dba2;UID={1};PWD={2}'.format(
                    creds["dba_server"], creds["dba_user"], creds["dba_pass"]
                ))
                c = conn.cursor()
                c.execute("exec whereis ?, ?", db, whereisenv[globals.env])
                servers = [row.server for row in c.fetchall() if row.status != 'offline']
                # servers = [s.replace('\\', '\\\\') for s in servers]
            # pprint.pprint(servers)

            if len(servers) == 0:
                cprint.warning("WARNING: No online servers for database '{db}' found in {env} environment, so it is not being executed. The file label will still be updated as if it ran successfully.".format(db=db, env=globals.env))
            
            # failed = False
            for server in servers:
                cmdmsg = 'osql -l10 -b -n -d {database} -U {user} -P {password} -S {server} -I -i {file}'.format(
                    database=db, user=creds[db + "_User"], 
                    # password=creds[db + "_Pass"], 
                        password='******',
                    server=server, file=file
                )
                cmd = ['osql', '-l10', '-b', '-n', '-I', 
                       '-d', db, 
                       '-U', creds[db + "_User"],
                       '-P', creds[db + "_Pass"], 
                       '-S', server, 
                       '-i', file]
                # cprint.declare(cmd)
                cprint.declare(cmdmsg)
                if not globals.args.dry_run:
                    isql = sproc.Popen(cmd, stdout=sproc.PIPE, stderr=sproc.STDOUT)
                    cprint.tee(isql, globals.logfile)
                    isqlout, isqlerr = isql.communicate()
                    if isql.returncode != 0:
                        # failed = True
                        # manifest.failed = True
                        sqlf.failure = True
                        val = 'init'
                        while val not in ('y', 'n'):
                            globals.logfile.write("A sql file just failed. Do you want to proceed? [y/n] ")
                            val = input(colors.RED + "A sql file just failed. Do you want to proceed? [y/n] " + colors.END)
                            globals.logfile.write(val)
                            if val == 'n':
                                sys.exit()
            # if not failed and not globals.args.dry_run:
            #     ss.label(sqlf.sspath, globals.fromenv, '{0}'.format(identityenv[globals.env]))


def create_task(task):
    if len(task.keys()) != 1:
        raise ValueError("task {0} should have only one cmd key".format(task))

    for k, v in task.items():
        cmd = k
        
        if isinstance(v, int):
            opts = str(v)
        elif isinstance(v, dict):
            myv = copy.deepcopy(v)
            for opt, opt_val in myv.items():
                myv[opt] = replace_tokens(myv[opt])
            opts = myv
        elif isinstance(v, str):
            opts = replace_tokens(v)
        else:
            raise ValueError("Unknown task option type for task {cmd}".format(cmd=cmd))
        
        if cmd == "wait":
            return Wait(cmd, opts)
        elif cmd == "runsql":
            return RunSql(cmd, opts)
        elif cmd == "sleep":
            return Sleep(cmd, opts)
        else:
            raise ValueError("Unknown command {0}".format(cmd))
