import os
import re
from .globals import globals
from .colors import colors
from .print import cprint
from .sourcesafe import ss


class SqlFile:
    def __init__(self, relpath):
        self.relpath = relpath  # web/ace/myfile.txt
        self.sspath = '$/xls/src/scripts/packageSQL/' + relpath
        self.tmpbasedir = globals.tmpbasedir
        self.tmppath = self.tmpbasedir + relpath  # /cygdrive/c/temp/web/ace/myfile.txt
        self.tmpdir = os.path.dirname(self.tmppath)  # /cygdrive/c/temp/web/ace
        self.basename = os.path.basename(relpath)  # myfile.txt

        self.db_name, self.copy_type = self.get_db_info_from_filename()

    def get_file(self, label, label_suffix):
        """Gets file to tmpdir location. Returns location of the file"""
        if ss.get_file_tmp(self.sspath, label, label_suffix, self.tmpdir):
            return self.tmppath
        else:
            raise Exception(colors.RED + "Error getting file, see SourceSafe output above" + colors.END)

    def validate(self):
        """Finds naughty strings"""
        with open(self.tmppath, 'rb') as sqlf:
            sqlf = sqlf.read().decode('utf-8', 'ignore')
            if re.search('disable(\s)+trigger', sqlf, flags=re.I) is not None:
                raise ValueError(colors.RED + \
                                 'Found "disable trigger" in {0}. Albert is shaking his head. Exiting..'.format(self.basename) + \
                                 colors.END)

    def get_relative_path(self, prefix='$/xls/src/scripts/packageSQL/'):
        """$/xls/src/scripts/packageSQL/web/ace/file.txt ==> web/ace/file.txt"""
        return os.path.relpath(self.ss_filepath, prefix);

    def get_db_info_from_filename(self):
        toks = self.basename.split('__')
        if globals.args.verbose:
            cprint.info("Parsing", self.relpath)
            cprint.special(toks)
        try:
            if re.match('^\d+$', toks[1]):
                db_name, copy_type = toks[2].lower(), toks[3].upper()
            else:
                db_name, copy_type = toks[1].lower(), toks[2].upper()
        except IndexError:
            cprint.error("Filename error: {0}".format(self.basename))
            raise
        if re.match('^MASTER', copy_type):
            copy_type = 'MASTER'
        elif re.match('^ALL', copy_type):
            copy_type = 'ALL'
        else:
            raise ValueError(colors.RED + "Expected MASTER[ONLY] or ALL[COPIES]: {0}".format(self.relpath) + colors.END)
        return db_name, copy_type
