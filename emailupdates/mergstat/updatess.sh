XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

# Execute the sql queries to insert the entry in the database
thismonth=`date +%m`
year=`date +20%y`
month=""
if [ "${thismonth}" = "01" ]
then
	month=January
elif [ "${thismonth}" = "02" ]
then
	month=February
elif [ "${thismonth}" = "03" ]
then
	month=March
elif [ "${thismonth}" = "04" ]
then
	month=April
elif [ "${thismonth}" = "05" ]
then
	month=May
elif [ "${thismonth}" = "06" ]
then
	month=June
elif [ "${thismonth}" = "07" ]
then
	month=July
elif [ "${thismonth}" = "08" ]
then
	month=August
elif [ "${thismonth}" = "09" ]
then
	month=September
elif [ "${thismonth}" = "10" ]
then
	month=October
elif [ "${thismonth}" = "11" ]
then
	month=November
elif [ "${thismonth}" = "12" ]
then
	month=December
fi

if [ $# -gt 0 ]
then
	month=${1}
fi

filename=${month}${year}.pdf

server=`get_xls_registry_value xlsdbo Server`
logon=`get_xls_registry_value xlsdbo User`
password=`get_xls_registry_value xlsdbo Password`

isql /U${logon} /P${password} /S${server} << HERE

DECLARE  @nextid int
exec next_sequence_p "ss", @nextid OUTPUT
select @nextid

insert into ss (
	id,
	host,
	volume,
	path,
	title,
	publication,
	alphasort,
	description,
	source,
	cost,
	entered,
	expires,
	master_index,
	finmkt_index,
	demgrph_index,
	econ_index,
	type,
	ready_to_go
) values (
	@nextid,
	15,
	"/data/mergstat",
	\"${filename}\",
	\"Mergerstat eMonthly Newsletters - ${month} ${year}\",
	4715,
	\"Mergerstat eMonthly Newsletters",
	\"Mergerstat eMonthly Report - ${month} ${year}\",
	20,
	100.0,
	\"${month} 1, ${year}\",
	NULL,
	0,0,0,0,
	5,
	1
)
	
go

quit
HERE
if [ $? != 0 ]
then
	echo "Error in isql, exiting"
	exit 1
fi

echo "Checkin complete"
