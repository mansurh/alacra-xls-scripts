# Command file to update a custom alacra book on a web server
# Parse the command line arguments.
#	1 = user
#   2 = ftpserver
#	3 = ftpbasedocsdir
#   4 = ftpuser
#   5 = ftppassword
shname=updatebooklow.sh
if [ $# -lt 5 ]
then
	echo "Usage: ${shname} user ftpserver ftpbasedocsdir ftpuser ftppassword"
	exit 1
fi

# Set up vars
user=$1
ftpserver=$2
data6docsdir=$3
ftpuser=$4
ftppassword=$5

todaysdate=$(date +%y%m%d)

ftpfilename=updatebook_${user}_${todaysdate}.ftp

bookdir=c:/usr/netscape/server/docs/alacrabook/${user}
bookxsldir=${bookdir}/xsl
bookhelpdir=${bookdir}/help
bookasyncdir=${bookdir}/async

data6bookdir=${data6docsdir}/alacrabook/${user}
data6bookxsldir=${data6bookdir}/xsl
data6bookhelpdir=${data6bookdir}/help
data6asyncdir=${data6bookdir}/xsl/async

typeset -i ishelp
typeset -i isxsl
typeset -i isasync

# ----------------------------


function testremotedir # xremotedir xftpserver xftpuser xftppassword
{
	xremotedir=${1}
	xftpserver=${2}
	xftpuser=${3}
	xftppassword=${4}

	xftpfilename=testremotedir$(date +%Y%m%d%H%M%S).ftp
	xftptestfilename=testremotedir$(date +%Y%m%d%H%M%S)_test.out
	# build an ftp command file
	rm -f ${xftpfilename}
	echo "user ${xftpuser} ${xftppassword}" > ${xftpfilename}
	echo "cd ${xremotedir}" >> ${xftpfilename}
	echo "bye" >>${xftpfilename}

	#echo "----"
	#cat ${xftpfilename}
	#echo "----"

	# run ftp
	ftp -i -n -s:${xftpfilename} ${xftpserver} > ${xftptestfilename} 2>&1
	# Test the exit status of ftp
	if [ $? != 0 ]
	then
		echo "${shname}: Error in ftp, Exiting"
		exit 1
	fi

	# look for a line with 250 at the start - if found, the cd worked
	grep -q "^250 " ${xftptestfilename}
	if [ $? != 0 ]
	then
		xishelp=0
	else
		xishelp=1
	fi
	rm -f ${xftptestfilename} ${xftpfilename}
	return $xishelp
}


# ----------------------------

# start in root dir
cd c:/

# validate that the remote book directory exists
testremotedir ${data6bookdir} ${ftpserver} ${ftpuser} ${ftppassword}
if [ $? != 1 ]
then
	echo "${shname}: Subdirectory ${data6bookdir} does not appear to exist on ftp server ${ftpserver}, Exiting"
	exit 1
fi


# ----------------------------


# create the book directory
if [ ! -d ${bookdir} ]
then
	echo "${shname}: Creating ${bookdir}"
	mkdir -p ${bookdir}
fi

# change to book directory
cd ${bookdir}
if [ $? != 0 ]
then
    echo "${shname}: Could not cd to ${bookdir}"
    exit 1
fi


# ------------

testremotedir ${data6bookhelpdir} ${ftpserver} ${ftpuser} ${ftppassword}
ishelp=$?

testremotedir ${data6bookxsldir} ${ftpserver} ${ftpuser} ${ftppassword}
isxsl=$?

testremotedir ${data6asyncdir} ${ftpserver} ${ftpuser} ${ftppassword}
isasync=$?

if [ $isxsl -gt 0 ]
then
	if [ ! -d ${bookxsldir} ]
	then
		echo "${shname}: Creating ${bookxsldir}"
		mkdir -p ${bookxsldir}
	fi
fi

if [ $ishelp -gt 0 ]
then
	if [ ! -d ${bookhelpdir} ]
	then
		echo "${shname}: Creating ${bookhelpdir}"
		mkdir -p ${bookhelpdir}
	fi
fi

if [ $isasync -gt 0 ]
then
	if [ ! -d ${bookasyncdir} ]
	then
		echo "${shname}: Creating ${bookasyncdir}"
		mkdir -p ${bookasyncdir}
	fi
fi

# ------------


# build the ftp command file
rm -f ${ftpfilename}
echo "user ${ftpuser} ${ftppassword}" > ${ftpfilename}
echo "binary" >> ${ftpfilename}
echo "cd ${data6bookdir}" >> ${ftpfilename}
echo "lcd ${bookdir}" >> ${ftpfilename}
echo "mget *.*" >>${ftpfilename}

if [ $isxsl -gt 0 ]
then
	echo "cd ${data6bookxsldir}" >> ${ftpfilename}
	echo "lcd ${bookxsldir}" >> ${ftpfilename}
	echo "mget *.*" >>${ftpfilename}
fi

if [ $ishelp -gt 0 ]
then
	echo "cd ${data6bookhelpdir}" >> ${ftpfilename}
	echo "lcd ${bookhelpdir}" >> ${ftpfilename}
	echo "mget *.*" >>${ftpfilename}
fi

if [ $isasync -gt 0 ]
then
	echo "cd ${data6asyncdir}" >> ${ftpfilename}
	echo "lcd ${bookasyncdir}" >> ${ftpfilename}
	echo "mget *.*" >>${ftpfilename}
fi

echo "bye" >>${ftpfilename}

#echo "${shname}: ---------- ftp command file ----------"
#cat ${ftpfilename}
#echo "${shname}: ---------- ftp command file ----------"


# run Ftp (3)
ftp -i -n -s:${ftpfilename} ${ftpserver}
# Test the exit status of ftp
if [ $? != 0 ]
then
	echo "${shname}: Error in ftp, Exiting"
	rm -f ${ftpfilename}
	exit 1
fi

cd  ${bookdir}
chmod -R a+r *

rm -f ${ftpfilename}


# remove any source safe files
rmscc.sh ${bookdir}

exit 0
