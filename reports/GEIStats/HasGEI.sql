set nocount on
set transaction isolation level read uncommitted

select CASE WHEN GEI IS NULL THEN 'No' ELSE 'Yes' END AS Has_GEI, COUNT(*) as [Count]
FROM
	ipid_gei
GROUP BY CASE WHEN GEI IS NULL THEN 'No' ELSE 'Yes' END
ORDER BY COUNT(*) desc
