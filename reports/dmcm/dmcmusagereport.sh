# Parse the command line arguments.
#	1 = server
#	2 = user
#	3 = password
if [ $# != 3 ]
then
	echo "Usage: dmcmreport.sh server user password"
	exit 1
fi

# Save the runstring parameters in local variables
logon=$2
password=$3
dataserver=$1

isql /U${logon} /P${password} /S${dataserver} /n /w 500 < dmcmusagereport.sql | blat - -t rmilburn@datamonitor.com -s "Datamonitor Daily Usage Report"
