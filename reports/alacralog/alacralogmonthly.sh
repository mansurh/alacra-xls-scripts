XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/get_specific_date.fn

# Parse the optional command line arguments
#   1 = year as YYYY
#   2 = month as M
if [ $# -gt 1 ]
then

	# year and month on command line
	year=$1
	month=$2

else
	prevMonth=`get_specific_date -10 112 6`
#	year=`date +%Y`
#	month=`date +%m`
	year=${prevMonth:0:4}
	month=${prevMonth:4:2}
fi


script_dir=$XLS/src/scripts/reports/alacralog
input_file=${script_dir}/monthly.dat



while read account emaillist useridexcludelist
do
#	echo "${account} ==> ${emaillist} except [${useridexcludelist}]"
	$XLS/src/scripts/reports/alacralog/alacralog.sh ${account} ${year} ${month} ${emaillist} ${useridexcludelist}
done < ${input_file}

exit 0
