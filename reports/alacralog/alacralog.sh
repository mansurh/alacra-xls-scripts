TITLEBAR="Alacralog Extract Report"

# Parse the command line arguments.
#   1 = account number
#   2 = year as YYYY
#   3 = month as MM
#	4 = email To: list
#	5 = userid exclude list (comma separated) - optional
if [ $# -lt 4 ]
then
	echo "Usage: alacralog.sh account year month emailtolist"
	exit 1
fi

# Save the runstring parameters in local variables
account=$1
year=$2
month=$3
emailtolist=$4
if [ $# -lt 5 ]
then
	useridexcludelist=""
else
	useridexcludelist=$5
fi


# create log dir if not exist
if [ ! -d $XLSDATA/alacralog ]
then
	mkdir -p $XLSDATA/alacralog
fi

# create unique log file name, based on date and time
datestr=`date +%H%M%S`
logfilename=$XLSDATA/alacralog/update${year}_${month}_${datestr}.log

echo "Extracting alacralog report for account ${account}, year ${year}, month ${month}" > ${logfilename}

# Perform the update
$XLS/src/scripts/reports/alacralog/alacraloglow.sh ${account} ${year} ${month} ${emailtolist} ${useridexcludelist} >> ${logfilename} 2>&1
returncode=$?

# Mail the results to the administrators if update was no good
if [ ${returncode} != 0 ]
then
	echo "Bad return code from alacraloglow.sh - mailing log file" >> ${logfilename}
	blat ${logfilename} -t "administrators@xls.com" -s "Alacralog report for account ${account} Failed"
else
	# good return code
	echo "done"
fi

exit $returncode
