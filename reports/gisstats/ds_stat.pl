
use strict;

use XML::DOM::XPath;

use Win32::TieRegistry 0.20 qw($Registry KEY_READ);
sub getXlsRegistryValue {
	my $keyname = shift;
	my $valname = shift;
	my $HKLMKey = $Registry->Open( "LMachine/", {Access=>KEY_READ(),Delimiter=>"/"} );
	my $RequestedKey = $HKLMKey->{$keyname};
	my $x = $RequestedKey->{"/$valname"};
	$x;
}

sub getXlsRandomizedRegistryValue {
	my $keyname = shift;
	my $valname = shift;
	my $val = getXlsRegistryValue($keyname, $valname);
	my @xlist = split /;/, $val;
	my $x = $xlist[int(rand(scalar(@xlist)))];
	$x;
}

my @regkey_input_list = ();

my $server = @ARGV[0];

my $requested_regkeys = @ARGV[1];
@regkey_input_list = split( /,/, $requested_regkeys);

my @retry_list = ();
my $in_retry = 0;
my $rerequest_nostats = @ARGV[2];

if( grep( /dnb2/, @regkey_input_list ) ) {  # dnb2store is not listed in dba2 at all and so must be requested manually when dnb2 is found
	push @regkey_input_list, "dnb2store";
}

my $first_idx = ${ @regkey_input_list }[0];
GISREQUEST: my $data = `perl gisget_xml.pl $server \"regkey=$first_idx&topic=_getstatistic&app=_statistic\"`;

use Win32::OLE;
my $srv = getXlsRandomizedRegistryValue("SOFTWARE/XLS/Data Servers/alacralog","Server");
my $uid = getXlsRegistryValue("SOFTWARE/XLS/Data Servers/alacralog","User");
my $pwd = getXlsRegistryValue("SOFTWARE/XLS/Data Servers/alacralog","Password");
#print "<!-- using alacralog server:$srv,uid:$uid,pwd:$pwd-->\n";
my $cn = Win32::OLE->new("ADODB.Connection");
my $dsn = "PROVIDER=SQLOLEDB;SERVER=$srv;DATABASE=alacralog;UID=$uid;PWD=$pwd";
$cn->open($dsn);
if( !$cn ) {
	die "Could not open connection to DSN because of [$!]";
}
 
if( length($data) < 1 ) {
	print "<!-- gis error: Zero length response from $server -->\n";
} else {

		my $parser= XML::DOM::Parser->new();
		my $dom;
		eval { $dom = $parser->parse ($data,ProtocolEncoding => 'ISO-8859-1'); };
		if( $@ ) {
			print "<!-- parse err: $@ -->\n";
			print "<!-- for data : $data -->\n";
		} else {
			INVINDEX: foreach my $idx (@regkey_input_list) {
				my @databasenodes = $dom->findnodes( "/DOCUMENT/RESULTSET/RESULTS/DATABASE[NAME = '" .lc($idx). "']" );
				if( @databasenodes lt 1 ) {
					if( $in_retry eq 1 ) {
						print "<!-- ON RETRY: could not find invindex $idx in gis query to $server for $first_idx -->\n";
					} else {
						#print "<!-- could not find invindex $idx in gis query to $server for $first_idx -->\n";
					}
				} else {
					$in_retry = 0;
				}
				my $idx_avg_parse_time = 0;
				my $idx_avg_eval_time = 0;
				my $idx_avg_getdoc_time = 0;
				my $idx_max_qry_time = 0;
				my $idx_min_qry_time = 99999;
				my $idx_num_files = 0;
				my $idx_num_queries = 0;
				foreach my $databasenode (@databasenodes) {
					my $name = $databasenode->findvalue( "NAME" )->string_value;
					my $num_files = $databasenode->findvalue( "FILES" )->string_value;
					my $num_queries = $databasenode->findvalue( "NUM_QUERIES_SERVICED" )->string_value;

					my $last_max_query = $databasenode->findvalue( "MAX/QUERYSTRING" )->string_value;
					my $last_max_query_time=0;
					my $last_max_parse = $databasenode->findvalue( "MAX/PARSE" )->string_value;
					$last_max_query_time+=$last_max_parse;
					my $last_max_eval = $databasenode->findvalue( "MAX/EVAL" )->string_value;
					$last_max_query_time+=$last_max_eval;
					my $last_max_getdoc = $databasenode->findvalue( "MAX/GETDOC" )->string_value;
					$last_max_query_time+=$last_max_getdoc;
					if( $last_max_query_time gt $idx_max_qry_time ) {
						$idx_max_qry_time = $last_max_query_time;
					}

					my $last_min_query = $databasenode->findvalue( "MIN/QUERYSTRING" )->string_value;
					my $last_min_query_time=0;
					my $last_min_parse = $databasenode->findvalue( "MIN/PARSE" )->string_value;
					$last_min_query_time+=$last_min_parse;
					my $last_min_eval = $databasenode->findvalue( "MIN/EVAL" )->string_value;
					$last_min_query_time+=$last_min_eval;
					my $last_min_getdoc = $databasenode->findvalue( "MIN/GETDOC" )->string_value;
					$last_min_query_time+=$last_min_getdoc;
					if( $last_min_query_time lt $idx_min_qry_time && $last_min_query_time ne 2147483646 ) {
						$idx_min_qry_time = $last_min_query_time;
					}

					my $avg_qry_time=0;
					my $avg_parse = $databasenode->findvalue( "AVG/PARSE" )->string_value;
					$avg_qry_time+=$avg_parse;
					my $avg_eval = $databasenode->findvalue( "AVG/EVAL" )->string_value;
					$avg_qry_time+=$avg_eval;
					my $avg_getdoc = $databasenode->findvalue( "AVG/GETDOC" )->string_value;
					$avg_qry_time+=$avg_getdoc;
					my $num_docs = $databasenode->findvalue( "INDEXED_DOCS" )->string_value;
					$idx_avg_parse_time += $avg_parse;
					$idx_avg_eval_time += $avg_eval;
					$idx_avg_getdoc_time += $avg_getdoc;
					$idx_num_queries += $num_queries;
					$idx_num_files = $num_docs;
				}

#				my $dsstatreccount = $cn->Execute("
#				  select count(*) from dsstats where server='$server' and invidx='$idx'
#			 	");
#				if( !$dsstatreccount ) {
#					print "dsstatreccount failure--";
#					my $Err = $cn->Errors();
#					print "Errors: \n";
#					foreach my $e (keys %$Err) {
#						print $e->{Description} . "\n";
#					}
#					die "Could not execute SQL.";
#				}
#				my $reccount = $dsstatreccount->Fields(0)->{Value};
				if( @databasenodes gt 0 ) {
					$idx_avg_parse_time /= @databasenodes;
					$idx_avg_eval_time /= @databasenodes;
					$idx_avg_getdoc_time /= @databasenodes;
				} else {
					if( $rerequest_nostats ne 0 && $in_retry eq 0 ) {
						push @retry_list, $idx;
					}
				}
#				if( $reccount gt 0 ) {
#					$cn->Execute("
#		update dsstats set avgparsetime=$idx_avg_parse_time, avgevaltime=$idx_avg_eval_time, avggetdoctime=$idx_avg_getdoc_time, numdocs=$idx_num_files, lastupdateddate=GETDATE() where server='$server' and invidx='$idx'
#						");
#				} else {

#					print
					$cn->Execute
						("
		insert into dsstats (server,invidx,minqrytime,maxqrytime,avgparsetime,avgevaltime,avggetdoctime,numqueries,numdocs,lastupdateddate) 
		values ('$server','$idx',$idx_min_qry_time,$idx_max_qry_time,$idx_avg_parse_time,$idx_avg_eval_time,$idx_avg_getdoc_time,$idx_num_queries,$idx_num_files,GETDATE())
						");
#					print "\n";

#				}
			}
			if( scalar(@retry_list) gt 0 ) {
				$first_idx = pop @retry_list;
			       	$in_retry = 1;
				@regkey_input_list = ($first_idx);
				goto GISREQUEST;
			}
		}
}

$cn->Close;

__END__

