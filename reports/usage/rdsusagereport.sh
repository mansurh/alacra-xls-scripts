# Parse the command line arguments.
#	1 = server
#	2 = user
#	3 = password
if [ $# != 3 ]
then
	echo "Usage: rdsusagereport.sh server user password"
	exit 1
fi

# Save the runstring parameters in local variables
logon=$2
password=$3
dataserver=$1

isql /U${logon} /P${password} /S${dataserver} /n /w 500 < rdssubs.sql | blat - -t mangle@xls.com,remsen_harris@rdsinc.com,meg_harris@rdsinc.com -s "Monthly Subscriber Report"
