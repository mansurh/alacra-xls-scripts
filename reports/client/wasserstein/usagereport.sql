declare @startdate smalldatetime
declare @enddate smalldatetime

SET NOCOUNT ON

/* Run for previous month */
select @startdate = DATEADD(month, -1, GETDATE())

/* Move to beginning of the month */
select @startdate = DATEADD(day, -1 * (DATEPART(day, @startdate) -1), @startdate)
select @startdate = DATEADD(hour, -1 * DATEPART(hour, @startdate), @startdate)
select @startdate = DATEADD(minute, -1 * DATEPART(minute, @startdate), @startdate)

/* Calculate the end of the monthly period */
select @enddate = DATEADD (month, 1, @startdate)


PRINT ".XLS Usage Report for Wasserstein & Perrella"
PRINT ""
PRINT ""
select CONVERT(varchar(16),@startdate,107) "For Month Starting"
PRINT ""
PRINT ""

PRINT "Downloads by User, Content Provider"
PRINT "==================================="
select 
	convert (varchar(16),p.last_name) "Last Name",
	convert (varchar(16),p.first_name) "First Name",
	convert (varchar(12),p.login) "User-ID",
	convert (varchar(16),u.project) "Project",
	convert (varchar(20),u.access_time, 1) "Date",
	convert (varchar(16),i.name) "Content Provider", 
	convert (varchar(10),u.list_price) "List Price",
	convert (varchar(10),u.price) "Price"
from 
	usage u, users p, ip i, account a
where
	u.access_time >= @startdate
and
	u.access_time < @enddate
and
	u.userid = p.id
and
	u.ip = i.id
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	p.account = a.id
and
	p.account = 36
order by
	p.last_name, i.id


PRINT ""
PRINT "Pay-per-view cost of downloads"
PRINT "=============================="
select 
	convert (varchar(10),sum(u.price)) "Total $"
from 
	usage u, users p, ip i, account a
where
	u.access_time >= @startdate
and
	u.access_time < @enddate
and
	u.userid = p.id
and
	u.ip = i.id
and
	u.access_time >= p.commence
and
	p.demo_flag is null
and
	p.account = a.id
and
	p.account = 36
go
