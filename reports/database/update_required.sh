LATELIST='./latelist'

rm ${LATELIST}
rm -f update_required.txt

# The server containing the dba database
DBASERVER=`$XLS/src/scripts/utility/dbserver.sh dba`
DBAUSER=dba
DBAPASSWORD=dba


#create list of databases whose updates are delinquents.
#this is only true with respect to the dbupdate table.

isql /U${DBAUSER} /P${DBAPASSWORD} /S${DBASERVER} -n /w 500 << EOF > ${LATELIST}
use dba
go

set nocount on
go

print "DATABASES REQUIRING AN UPDATE"
print "-----------------------------"
print ""
print ""

select 	"Server" = CONVERT(char(8),ds.name),
	"Dbname" = CONVERT(char(16),db.name),
	"Component" = CONVERT(char(24),dc.name),
	"Last update" = CONVERT(char(12),du.updatetime,107),
	"Update freq" = "      " + (CONVERT(char(4),dc.freq_in_days)),
	"Overdue by" = datediff(dy,updatetime,getdate())

from dbupdate du, dbcomponent dc, dbinstance di, dbase db, dbserver ds

where 
	(datediff(dy,du.updatetime,getdate()) -
        (datediff(wk,du.updatetime,getdate()) * 2 )) > dc.freq_in_days
	and
	du.dbinstance = di.id
	and 
	du.dbcomponent = dc.id
  	and 
	du.updatetime = ( select max(d2.updatetime ) from dbupdate d2
                          where d2.dbcomponent = dbcomponent
                          and d2.dbinstance = du.dbinstance)
	and 
	dc.freq_in_days is not null
	and 
	di.dbserver = ds.id
	and 
	di.dbase = db.id

UNION

select 	"Server" = CONVERT(char(8),ds.name),
	"Dbname" = CONVERT(char(16),db.name),
	"Component" = CONVERT(char(24),dc.name),
	"Last update" = CONVERT(char(12),NULL),
	"Update freq" = "      " + (CONVERT(char(4),dc.freq_in_days)),
	"Overdue by" = 9999
from dbcomponent dc, dbinstance di, dbase db, dbserver ds
where 
di.dbase=dc.dbase
and
(select count(*) from dbupdate du where du.dbinstance = di.id and du.dbcomponent=dc.id) = 0
and dc.freq_in_days is not null
and di.dbase = db.id
and di.dbserver = ds.id

order by Dbname, Component, Server

go

EOF

cat ${LATELIST} | sed 's/^ //' > update_required.txt


blat update_required.txt -t administrators@xls.com -s "UPDATES REQUIRED"
