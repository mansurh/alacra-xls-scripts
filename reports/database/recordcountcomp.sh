# recordcountcomp.sh
# Script to compare record counts from database tables
#
# Usage:
#	recordcountcomp.sh server1 server2 logon password table"
#
if [ $# -lt 5 ]
then
	echo "Usage: recordcountcomp.sh server1 server2 logon password table"
	exit 1
fi

# Read params
server1=$1
server2=$2
logon=$3
password=$4
table=$5

# get count from server 1
rawout=$(isql /S ${server1} /U${logon} /P${password} /n /h-1 /Q"SELECT COUNT(*) FROM ${table}" | grep -v "(" )
for tok in $rawout
do
	count1=${tok}
done

# get count from server 2
rawout=$(isql /S ${server2} /U${logon} /P${password} /n /h-1 /Q"SELECT COUNT(*) FROM ${table}" | grep -v "(" )
for tok in $rawout
do
	count2=${tok}
done

# They should be the same


if [ "${count1}" = "${count2}" ]
then
	echo "Record count matches between ${server1} and ${server2}, table ${table}=[${count1}]"
else
	echo "Record count mismatch! Table ${table}, ${server1}=[${count1}], ${server2}=[${count2}]"
	exit 1
fi

exit 0
