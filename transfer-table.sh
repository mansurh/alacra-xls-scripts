#parse the command line arguments
# 1 = table name
# 2 = sourcedb
# 3 = source server
# 4 = source login
# 5 = source passwd
# 6 = dest db
# 7 = dest server
# 8 = dest user
# 9 = dest passwd

if [ $# -lt 9 ]
then
    echo "Usage: transfer-table.sh table srcdb srcserver srclogin srcpasswd destdb destserver destlogin destpasswd"
    exit 1
fi

table=$1
srcdb=$2
srcserver=$3
srcuser=$4
srcpasswd=$5
destdb=$6
destserver=$7
destuser=$8
destpasswd=$9

TEMPDIR=${XLSDATA}

rm -f ${TEMPDIR}/transfer.dat

echo "\nTransfer of ${table} table from ${srcdb} (${srcserver}) to ${destdb} (${destserver})"

bcp ${srcdb}.dbo.${table} out ${TEMPDIR}/transfer.dat /S${srcserver} /U${srcuser} /P${srcpasswd} /c /t"\001" /r"\002"
if [ $? != 0 ]
then
    echo "Error saving source data to file"
    exit 1
fi

${XLS}/bin/cleanuptable ${destserver} ${destuser} ${destpasswd} ${destdb} ${table} droptableindexes
if [ $? != 0 ]
then
    echo "Error dropping Destination indices"
    exit 1
fi

${XLS}/bin/cleanuptable ${destserver} ${destuser} ${destpasswd} ${destdb} ${table} truncatetable
if [ $? != 0 ]
then
    echo "Error truncating Destination table"
    exit 1
fi

echo "\nLoading data into destination..."

bcp ${destdb}.dbo.${table} in ${TEMPDIR}/transfer.dat /S${destserver} /U${destuser} /P${destpasswd} /c /t"\001" /r"\002"

# rebuild the destination indexes
