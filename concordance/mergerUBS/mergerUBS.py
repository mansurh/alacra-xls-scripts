﻿#!/usr/bin/python2.7
from _winreg import *

import pymssql
import sys, getopt
import uuid

class Entity:
	def __init__(self, xlsid=None, survivor=False, url=None, ult_parent=None ):
		self.xlsid = xlsid
		self.survivor = survivor
		self.url = url
		self.ult_parent = ult_parent
		self.isDupe = False
		self.hasDeadMapping = False
		self.address_list = []
		self.security_list = []
		self.alias_list = []
		self.mappings = []
		self.mappingsFast = set()
		self.baseFeed = False
		self.baseWeight = 0
		self.statusQueries = []
		self.industry_list = []
		self.sic_list = []
		self.naics_list = []


class BaseFeed:
	def __init__(self, source, name, weight, check_dupe_query=None, protected_flag=None, check_dead_mapping=None ):
		self.source = source
		self.name = name
		self.weight = weight
		self.check_dupe_query = check_dupe_query
		self.protected_flag = protected_flag
		self.check_dead_mapping = check_dead_mapping

class MultipleMapsSource:
	def __init__(self, source, name):
		self.source = source
		self.name = name

class Merge:
	def __init__(self, xlsid1=None, xlsid2=None):
		self.xlsid1 = xlsid1
		self.xlsid2 = xlsid2

class Telephone:
	def __init__(self, type, number):
		self.type = type
		self.number = number

class Address:
	def __init__(self, id, type):
		self.id = id
		self.type = type
		self.primary_flag = 0
		self.address1 = self.address2 = self.address3 = self.address4 = self.address5 = self.address6 = ""
		self.city = ""
		self.state_prov = ""
		self.postal_code = ""
		self.country = None
		self.date_effective = None
		self.telephone_list = []

	def __weight__(self):
		weight = 0
		if self.address1 != None and len(self.address1) > 0:
			weight += 1
		if self.address2 != None and  len(self.address2) > 0:
			weight += 1
		if self.address3 != None and  len(self.address3) > 0:
			weight += 1
		if  self.address4 != None and len(self.address4) > 0:
			weight += 1
		if self.address5 != None and  len(self.address5) > 0:
			weight += 1
		if self.address6 != None and  len(self.address6) > 0:
			weight += 1
		if self.city != None and  len(self.city) > 0:
			weight += 8
		if self.state_prov != None and  len(self.state_prov) > 0:
			weight += 16
		if self.country != None:
			weight += 32

		return weight

	def __gt__(self, other):
		if isinstance(other, Address):
			return (self.__weight__() > other.__weight__())
		else:
			return NotImplemented

class Industry:
	def __init__(self, code, revenue_percent=None, p_flag=None ):
		self.code = code
		self.revenue_percent = revenue_percent
		self.p_flag = p_flag

class SecurityExchange:
	def __init__(self, secid, exchange, sedol=None, ticker=None, primary_flag=None, intl_ticker=None):
		self.secid = secid
		self.exchange = exchange
		self.sedol = sedol
		self.ticker = ticker
		self.primary_flag = primary_flag
		self.intl_ticker = intl_ticker

class Security:
	def __init__(self, id, name, type, cusipcins=None, isin=None, research_status=None, primary_flag=None, coupon=None, maturity=None):
		self.id = id
		self.name = name
		self.type = type
		self.cusipcins = cusipcins
		self.isin = isin
		self.research_status = research_status
		self.primary_flag = primary_flag
		self.coupon = coupon
		self.maturity = maturity
		self.exchange_list = []

class Alias:
	def __init__(self, xlsid, alias, purpose, display_priority=None, index_flag=None):
		self.xlsid = xlsid
		self.alias = alias
		self.purpose = purpose
		self.display_priority = display_priority
		self.index_flag = index_flag

class ConnectionParams:
	def __init__(self):

		self.server = "datatest4"
		self.user = "concordancedbo"
		self.password = "concordancedbo"

		aReg = ConnectRegistry(None,HKEY_LOCAL_MACHINE)
		aKey = OpenKey(aReg, r"SOFTWARE\XLS\Data Servers\concordance")

		try:
			self.server = QueryValueEx(aKey, "Server")[0]
			self.user = QueryValueEx(aKey, "User")[0]
			self.password = QueryValueEx(aKey, "Password")[0]
		except WindowsError:
			print "Unable to retrieve data server information from the registry. Using default values."

class BbgEntity:
	def __init__(self, bbgid=None, nametype=None, ctprelationship=None, parentid=None):
		self.bbgid = bbgid
		self.nametype = nametype
		self.ctprelationship = ctprelationship
		self.parentid = parentid

baseFeeds = []
multipleMapsSources= []
merges = []
protectedMappings = set([11060,472])
mergedXlsids = set()

bbgEntities = []

def execAndCommit(query, errmsg, succsmsg, cursor):
	ok = True
	msg = succsmsg
	try:
		cursor.execute(query)
	except pymssql.DatabaseError, ex:
		exc_type, exc_obj, exInfo = sys.exc_info()
		ok = False
		msg = '%s. Database Exception (Message) : %s' % (errmsg, ex[1])
	except:
		ok = False
		msg = '%s. Error : %s' % (errmsg, sys.exc_info()[0])

	if ok:
		cursor.connection.commit()

	print msg

	return ok,msg

def execNoCommit(query, errmsg, succsmsg, cursor, params=None):
	ok = True
	msg = succsmsg
	try:
		if params != None:
			cursor.execute(query, params)
		else:
			cursor.execute(query)
	except pymssql.DatabaseError, ex:
		exc_type, exc_obj, exInfo = sys.exc_info()
		ok = False
		msg = '%s. Database Exception (Message) : %s' % (errmsg, ex[1])
	except:
		ok = False
		msg = '%s. Error : %s' % (errmsg, sys.exc_info()[0])

	print msg

	return ok,msg

def first(iterable, default=None):
	for item in iterable:
		return item
	return default

#####This function grabs the data that is relevant to the merge, run for each entity####
def cursorRoutine(entity, cursor, helperCursor):
	ok = True
	msg = ""
	try:
		#checks if the entity still exists
		cursor.execute("SELECT id from pta_entity where id = %d and date_end is null", entity.xlsid)
		if cursor.rowcount != 0:
			cursor.execute("SELECT id, address_type, address1, address2, address3, address4, address5, address6, city, state_prov, postal_code, country_id, date_effective, primary_flag FROM pta_address WHERE xlsid = %d AND date_end is null", entity.xlsid)
			if cursor.rowcount != 0:
				for row in cursor:
					a = Address(str(row["id"]).upper(), row["address_type"])
					a.address1 = row["address1"]
					a.address2 = row["address2"]
					a.address3 = row["address3"]
					a.address4 = row["address4"]
					a.address5 = row["address5"]
					a.address6 = row["address6"]
					a.city = row["city"]
					a.state_prov = row["state_prov"]
					a.postal_code = row["postal_code"]
					a.country = row["country_id"]
					a.date_effective = row["date_effective"]
					a.primary_flag = row["primary_flag"]

					entity.address_list.append(a)

			for a in entity.address_list:
				query = "SELECT telephone_type, telephone_number FROM pta_telephone where address_id = '%s' and date_end is null" % a.id
				cursor.execute(query)
				if cursor.rowcount != 0:
					for row in cursor:
						t = Telephone(row["telephone_type"], row["telephone_number"])
						a.telephone_list.append(t)


			#Gets the url for the entity
			cursor.execute("SELECT url from pta_entity where id = %d and date_end is null", entity.xlsid)
			if cursor.rowcount != 0:
				for row in cursor:
					entity.url = row["url"]

			#Gets the ult_parent for the entity
			cursor.execute("SELECT ult_parent from company where id = %d", entity.xlsid)
			if cursor.rowcount != 0:
				for row in cursor:
					entity.ult_parent = row["ult_parent"]

			#Gets the security data for the entity
			cursor.execute("SELECT id, name, type, cusipcins, isin, research_status, primary_flag, coupon, maturity FROM pta_security where issuer = %d and date_end is null", entity.xlsid)
			if cursor.rowcount != 0:
				for row in cursor:
					s = Security(row["id"], row["name"], row["type"])
					s.cusipcins = row["cusipcins"]
					s.isin = row["isin"]
					s.research_status = row["research_status"]
					s.primary_flag = row["primary_flag"]
					s.coupon = row["coupon"]
					s.maturity = row["maturity"]
					entity.security_list.append(s)

			#for s in entity.security_list:
			#	cursor.execute("SELECT secid, exchange, sedol, ticker, primary_flag, intl_ticker FROM pta_security where secid = %d and date_end is null", s.id)
			#	if cursor.rowcount != 0:
			#		for row in cursor:
			#			se = SecurityExchange(row["secid"], row["exchange"])
			#			se.sedol = row["sedol"]
			#			se.ticker = row["ticker"]
			#			se.intl_ticker = row["intl_ticker"]
			#			se.sedol = row["primary_flag"]
			#			s.exchange_list.append(se)

			#Gets the alias data for the entity
			cursor.execute("SELECT xlsid, alias, purpose, display_priority, index_flag FROM pta_entity_alias where xlsid = %d and date_end is null", entity.xlsid)
			if cursor.rowcount != 0:
				for row in cursor:
					a = Alias(row["xlsid"], row["alias"], row["purpose"])
					a.display_priority = row["display_priority"]
					a.index_flag = row["index_flag"]
					entity.alias_list.append(a)

			#Gets the industry data for the entity
			#industry
			cursor.execute("SELECT industry, revenue_percent, p_flag FROM pta_entity_industry where xlsid = %d and date_end is null", entity.xlsid)
			if cursor.rowcount != 0:
				for row in cursor:
					i = Industry(row["industry"],row["revenue_percent"],row["p_flag"])
					entity.industry_list.append(i)
			#sic
			cursor.execute("SELECT sic, revenue_percent, p_flag FROM pta_entity_sic where xlsid = %d and date_end is null", entity.xlsid)
			if cursor.rowcount != 0:
				for row in cursor:
					i = Industry(row["sic"],row["revenue_percent"],row["p_flag"])
					entity.sic_list.append(i)
			#naics
			cursor.execute("SELECT naics, revenue_percent, p_flag FROM pta_entity_naics where xlsid = %d and date_end is null", entity.xlsid)
			if cursor.rowcount != 0:
				for row in cursor:
					i = Industry(row["naics"],row["revenue_percent"],row["p_flag"])
					entity.naics_list.append(i)


			#Get the mappings for the entity
			cursor.execute("SELECT xlsid, source, sourcekey FROM pta_entity_map WHERE xlsid = %d and date_end is null", entity.xlsid)
			for row in cursor:
				mapping = {"xlsid" : row['xlsid'], "source" : row['source'], "sourcekey" : row['sourcekey'], "basefeed" : False, "dead" : False}
				entity.mappingsFast.add(row['sourcekey'])
				bfeed = first(feed for feed in baseFeeds if feed.source == row['source'])
				if bfeed != None:
					entity.baseFeed = True
					if bfeed.check_dead_mapping != None:
						if checkDeadMapping(bfeed.check_dead_mapping % (row['sourcekey']), helperCursor) == 1:
						  mapping["dead"] = True
						  entity.hasDeadMapping = True

					if mapping["dead"] == False:
						entity.baseWeight += bfeed.weight
					mapping["basefeed"] = True
					if bfeed.check_dupe_query != None:
						entity.statusQueries.append(bfeed.check_dupe_query % (row['sourcekey']))

				entity.mappings.append(mapping)


		else:
			msg = 'Unable to load merge related data for xlsid = %d. NOT FOUND in pta_entity table.' % (entity.xlsid)		
			ok = False
			
	except pymssql.DatabaseError, ex:
		msg = 'Unable to load merge related data for xlsid = %d. Database Exception (Message) : %s' % (entity.xlsid, ex[1])
		ok = False
	except:
		msg = 'Unable to load merge related data for xlsid = %d. Error : %s' % (entity.xlsid, sys.exc_info()[0])
		ok = False

	return ok, msg


def setSwitchSurvivorFlag(entity1, entity2):
	switchSurvivor = False
	# if entity2 has more security_list switch survivor
	if len(entity2.security_list) > len(entity1.security_list):
		switchSurvivor = True
	elif len(entity2.security_list) == len(entity1.security_list):
		# if entity2 has more complete address switch survivor
		if len(entity2.address_list) > len(entity1.address_list):
			switchSurvivor = True
		elif len(entity2.address_list) == len(entity1.address_list):
			# if entity2 has more mappings switch survivor
			if len(entity2.mappingsFast) > len(entity1.mappingsFast):
				switchSurvivor = True
			elif len(entity1.mappingsFast) == len(entity2.mappingsFast):
				# entity with the lowest numerical Alacra ID will be the survivor
				if entity2.xlsid < entity1.xlsid:
					switchSurvivor = True

	return switchSurvivor

def checkDuplicateStatus(entity, cursor):
	result = 0
	for query in entity.statusQueries:
		cursor.execute(query)
		for row in cursor:
			result += row["dupe"]
	return result

def checkDeadMapping(query, cursor):
	result = 0
	cursor.execute(query)
	for row in cursor:
		result += row["dead"]
	return result

def setSurvivor(entity1, entity2, cursor):
	ok = True
	msg = ""
	switchSurvivor = False
	#checks to make sure that at least one of the entities is not in the baseFeed
	if entity1.baseFeed == False or entity2.baseFeed == False:
		# both entities are not in the baseFeed
		if entity1.baseFeed == False and entity2.baseFeed == False:
			switchSurvivor = setSwitchSurvivorFlag(entity1,entity2)
		# entity2 is in the baseFeed
		elif entity2.baseFeed == True:
			switchSurvivor = True
	#both entities are in the baseFeed
	else:
		# entity2 has higher priority base feeds
		if entity2.baseWeight > entity1.baseWeight:
			switchSurvivor = True
		# both entities have same base mappings
		elif entity2.baseWeight == entity1.baseWeight:
			if entity1.hasDeadMapping == True or entity2.hasDeadMapping == True:
				if entity1.hasDeadMapping == True:
					switchSurvivor = True
			else:
				dupStatus1 = dupStatus2 = 0
				try:
					dupStatus1 = checkDuplicateStatus(entity1, cursor)
				except pymssql.DatabaseError, ex:
					ok = False
					msg = 'Unable to check duplicate status for xlsid = %d. Database Exception (Message) : %s' % (entity1.xlsid, ex[1])
				except:
					ok = False
					msg = 'Unable to check duplicate status for xlsid = %d. Error : %s' % (entity1.xlsid, sys.exc_info()[0])
				if ok == False:
					return ok, msg

				try:
					dupStatus2 = checkDuplicateStatus(entity2, cursor)
				except pymssql.DatabaseError, ex:
					ok = False
					msg = 'Unable to check duplicate status for xlsid = %d. Database Exception (Message) : %s' % (entity2.xlsid, ex[1])
				except:
					ok = False
					msg = 'Unable to check duplicate status for xlsid = %d. Error : %s' % (entity2.xlsid, sys.exc_info()[0])

				if ok == False:
					return ok, msg

				if dupStatus2 < dupStatus1:
					switchSurvivor = True
				elif dupStatus2 == dupStatus1:
					switchSurvivor = setSwitchSurvivorFlag(entity1,entity2)

	if switchSurvivor == True:
		entity2.survivor = True
		entity1.survivor = False

	return ok, msg

def AddToUBS_Parking_dups(survivor, victim, cursor):
	class Dupe:
		def __init__(self, id=None, source=None, name=None, date=None, RelatedKey=None):
			self.id = id
			self.source = source
			self.name = name
			self.date = date
			self.RelatedKey = RelatedKey
	dupes = []
	
	cursor.callproc('spCreateUBSParkingDupsInserts', (survivor.xlsid, victim.xlsid,))
	for row in cursor:
		dupes.append(Dupe(row['id'], row['source'], row['name'], row['date'], row['RelatedKey']))

	result = True
	msg = "%d records added to ipid_UBS_Parking_removedups" % (len(dupes))
	for d in dupes:
		query = "DELETE ipid_UBS_Parking_removedups WHERE id = '%s' and source = %s and RelatedKey = '%s'" % (d.id, d.source, d.RelatedKey)
		result, msg1 = execNoCommit(query, "Unable to delete records from ipid_UBS_Parking_removedups table", "Successfully deleted records from ipid_UBS_Parking_removedups table", cursor)
		if result == False:
			msg = msg1
			break

		if d.name != None:
			query = "INSERT ipid_UBS_Parking_removedups(id,source,name,code,date,notes,analyst,RelatedKey) select '%s', %s, '%s', 17, '%s', 'Duplicate of %s', 'BM', '%s'" % (d.id, d.source, d.name.replace('\'', '\'\''), d.date, d.RelatedKey, d.RelatedKey)
		else:
			query = "INSERT ipid_UBS_Parking_removedups(id,source,name,code,date,notes,analyst,RelatedKey) select '%s', %s, NULL, 17, '%s', 'Duplicate of %s', 'BM', '%s'" % (d.id, d.source, d.date, d.RelatedKey, d.RelatedKey)
		result, msg1 = execNoCommit(query, "Unable to add records to ipid_UBS_Parking_removedups table", "Successfully added records to ipid_UBS_Parking_removedups table", cursor)
		if result == False:
			msg = msg1
			break

	return result, msg

class MergeResults:
	pass

def mergeEntities(survivor, victim, cp, cursor):

	result = MergeResults()

    #validation check on xlsids
	if (not(xlsidsValid(survivor, victim, cursor))):
		result.ok = False
		result.msg = "XLSIDs not valid, Unable to merge xlsid = %d and xlsid = %d" % (victim.xlsid, survivor.xlsid)
		result.status = -2
		return result

    #check nonduplicates for xlsids with mappings to bloomberg, fitch, moodys, and S&P
	if (isBBGNonduplicate(survivor, victim, cursor) or isRatedNonduplicate(survivor, victim, cursor)):
		result.ok = False
		result.msg = "Nonduplicate mappings in bloomberg, fitch, moodys, or S&P. Unable to merge xlsid = %d and xlsid = %d" % (victim.xlsid, survivor.xlsid)
		result.status = -2
		return result

	protected_feed = first(m["source"] for m in victim.mappings if m["source"] in set(b.source for b in baseFeeds if b.protected_flag == 1))
	if protected_feed != None:
		if any(m for m in survivor.mappings if m["source"] in set(b.source for b in baseFeeds if b.protected_flag == 1) and m["source"] != protected_feed):
			result.ok = False
			result.msg = "Unable to move protected mappings from victim xlsid = %d to survivor xlsid = %d" % (victim.xlsid, survivor.xlsid)
			result.status = -2
			return result

	# If survivor and victim both have securities, the merge should stop. Not merging issuers.
	if len(victim.security_list) > 0 and len(survivor.security_list) > 0:
		result.ok = False
		result.msg = "Unable to merge two security issuers xlsid = %d and xlsid = %d" % (victim.xlsid, survivor.xlsid)
		result.status = -2
		return result

	#unable to move protected mappings
	#if any(m for m in victim.mappings if m["source"] in set(b.source for b in baseFeeds if b.protected_flag == 1)):
	#	result.ok = False
	#	result.msg = "Unable to move protected mappings from victim xlsid = %d to survivor xlsid = %d" % (victim.xlsid, survivor.xlsid)
	#	result.status = -2
	#	return result

	result.ok = True
	result.msg = "Merged xlsid = %d into survivor xlsid = %d" % (victim.xlsid, survivor.xlsid)
	result.updOk = True
	result.updMsg = "Success"
	sourcesAdded = set()

	with pymssql.connect(cp.server, cp.user, cp.password, "concordance") as conn:
		with conn.cursor(as_dict=True) as cursor:
			try:
				# update survivor's research status so mappings can be added if it was previously inactive
				query = "UPDATE pta_entity set research_status=NULL where id=%d and date_end is null" % (survivor.xlsid)
				result.ok, result.msg = execNoCommit(query, "Error reseting survivor's research status.", "Successfully reset survivor's research status", cursor)
				if result.ok == False:
					return result
				# Moving all but protected mappings over to the survivor entity
				for mapping in victim.mappings:
					if mapping["source"] not in set(b.source for b in baseFeeds if b.protected_flag == 1):
						# don't delete victim's base feed mapping if survivor already has that mapping
						if not (mapping["basefeed"] == True and mapping["source"] in set(m['source'] for m in survivor.mappings)):
							query = "UPDATE pta_entity_map set date_end = GETDATE(), notes = 'Deleted as a result of merge with xlsid=%d' where xlsid = %d and source = %d and date_end is null" % (survivor.xlsid, victim.xlsid,  mapping["source"])
							result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to delete merged mappings", "Successfully deleted merged mappings", cursor)
							if result.ok == False:
								return result


						# delete from company just in case it was not properly cleaned up before insertion
						query = "DELETE from company_map where xlsid = %d and source = %d and sourcekey = '%s'" % (survivor.xlsid, mapping["source"], mapping["sourcekey"].replace("'","''"))
						result.ok, result.msg = execNoCommit(query, "Error deleting mapping from company.", "Successfully deleted mapping from company", cursor)
						if result.ok == False:
							return result
						# insert mappings
						if mapping["source"] not in set(m['source'] for m in survivor.mappings) and mapping["source"] not in sourcesAdded:
							sourcesAdded.add(mapping["source"])
							msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
							query = "INSERT INTO pta_entity_map (xlsid,source,sourcekey,notes) values(%d, %d,'%s','%s')" % (survivor.xlsid, mapping["source"], mapping["sourcekey"].replace("'","''"), msg)
							result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged mappings", "Successfully added merged mappings", cursor)
							if result.ok == False:
								return result
						elif mapping["source"] in set(s.source for s in multipleMapsSources):
							msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
							query = "INSERT INTO pta_entity_map (xlsid,source,sourcekey,notes) values(%d, %d,'%s','%s')" % (survivor.xlsid, mapping["source"], mapping["sourcekey"].replace("'","''"), msg)
							result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged mappings", "Successfully added merged mappings", cursor)
							if result.ok == False:
								return result


					if (mapping["basefeed"] == True and mapping["source"] in set(m['source'] for m in survivor.mappings)):
						victim.isDupe = True

					if mapping["dead"] == True:
						query = "UPDATE pta_entity_map set correction_flag = 10, notes = 'Marked as a Dead mapping!!! As a result of merge with xlsid=%d' where xlsid = %d and source = %d and date_end is null" % (survivor.xlsid, victim.xlsid,  mapping["source"])
						result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to mark dead mapping", "Successfully deleted merged mappings", cursor)
						if result.ok == False:
							return result

				# Moving the industries over to the survivor entity
				query = "UPDATE pta_entity_industry set date_end = GETDATE(), notes = 'Deleted as a result of merge with xlsid=%d' where xlsid = %d and date_end is null" % (survivor.xlsid,victim.xlsid)
				result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to delete merged industries", "Successfully deleted merged industries", cursor)
				if result.ok == False:
					return result
				for industry in victim.industry_list:
					if industry.code not in set(i.code for i in survivor.industry_list):
						msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
						query = "INSERT INTO pta_entity_industry (xlsid,industry,revenue_percent,p_flag,notes) values(%d, %d, %d, %s, %s)"
						params = (survivor.xlsid, industry.code, industry.revenue_percent, industry.p_flag, msg)
						result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged industries", "Successfully added merged industries", cursor, params)
						if result.ok == False:
							return result

				# Moving the sic codes over to the survivor entity
				query = "UPDATE pta_entity_sic set date_end = GETDATE(), notes = 'Deleted as a result of merge with xlsid=%d' where xlsid = %d and date_end is null" % (survivor.xlsid,victim.xlsid)
				result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to delete merged sic codes", "Successfully deleted merged sic codes", cursor)
				if result.ok == False:
					return result
				for sic in victim.sic_list:
					if sic.code not in set(s.code for s in survivor.sic_list):
						msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
						query = "INSERT INTO pta_entity_sic (xlsid,sic,revenue_percent,p_flag,notes) values(%d, %d, %d, %s, %s)"
						params = (survivor.xlsid, sic.code, sic.revenue_percent, sic.p_flag, msg)
						result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged sic codes", "Successfully added merged sic codes", cursor, params)
						if result.ok == False:
							return result

				# Moving the naics codes over to the survivor entity
				query = "UPDATE pta_entity_naics set date_end = GETDATE(), notes = 'Deleted as a result of merge with xlsid=%d' where xlsid = %d and date_end is null" % (survivor.xlsid,victim.xlsid)
				result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to delete merged naics codes", "Successfully deleted merged naics codes", cursor)
				if result.ok == False:
					return result
				for naics in victim.naics_list:
					if naics.code not in set(n.code for n in survivor.naics_list):
						msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
						query = "INSERT INTO pta_entity_naics (xlsid,naics,revenue_percent,p_flag,notes) values(%d, %s, %d, %s, %s)" 
						params = (survivor.xlsid, naics.code, naics.revenue_percent, naics.p_flag, msg)
						result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged naics codes", "Successfully added merged naics codes", cursor, params)
						if result.ok == False:
							return result

				# Moving the security_list
				query = "UPDATE pta_security set date_end = GETDATE(), notes = 'Deleted as a result of merge with issuer=%d' where issuer = %d and date_end is null" % (survivor.xlsid,victim.xlsid)
				result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to delete merged securities", "Successfully deleted merged securities", cursor)
				if result.ok == False:
					return result
				for security in victim.security_list:
					if security.isin not in set(s.isin for s in survivor.security_list):
						if security.cusipcins not in set(s.cusipcins for s in survivor.security_list):
							msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
							query = "INSERT INTO pta_security (id, issuer, name, type, cusipcins, isin, research_status, primary_flag, coupon, maturity, notes) values(%d, %d, %s, %d, %s, %s, %s, %d, %d, %s, %s)"
							params = (security.id, survivor.xlsid, security.name, security.type, security.cusipcins, security.isin, security.research_status, security.primary_flag, security.coupon, security.maturity, msg)
							result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged securities", "Successfully added merged securities", cursor, params)
							if result.ok == False:
								return result

				# Moving the alias_list
				query = "UPDATE pta_entity_alias set date_end = GETDATE(), notes = 'Deleted as a result of merge with xlsid=%d' where xlsid = %d and date_end is null" % (survivor.xlsid,victim.xlsid)
				result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to delete merged aliases", "Successfully deleted merged aliases", cursor)
				if result.ok == False:
					return result
				for alias in victim.alias_list:
					msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
					query = "INSERT INTO pta_entity_alias (xlsid, alias, purpose, display_priority, index_flag, notes) values(%d, %s, %d, %d, %d, %s)"
					params = (survivor.xlsid, alias.alias, alias.purpose, alias.display_priority, alias.index_flag, msg)
					result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged aliases", "Successfully added merged aliases", cursor, params)
					if result.ok == False:
					    return result

				# Moving the subsidiaries over to the survivor entity
				if (survivor.ult_parent == survivor.xlsid and victim.ult_parent != victim.xlsid):
					query = "UPDATE pta_entity SET ult_parent=%d where id=%d and date_end is null" % (victim.ult_parent, survivor.xlsid)
					result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged subsidiaries", "Successfully moved ult_parent", cursor)
					if result.ok == False:
						return result
				else:
					query = "UPDATE pta_entity SET ult_parent=%d where ult_parent=%d and date_end is null" % (survivor.ult_parent, victim.xlsid)
					result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged subsidiaries", "Successfully added subsidiaries", cursor)
					if result.ok == False:
						return result

				# Moving the corporate events over to the survivor entity
				query = "UPDATE corporate_events set entityid = %d, notes = 'Added as a result of merge with xlsid=%d' where entityid = %d" % (survivor.xlsid, victim.xlsid , victim.xlsid)
				result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged corporate events", "Successfully added corporate events", cursor)
				if result.ok == False:
					return result

				# Merging the address list type by type
				for va in victim.address_list:
					primary_flag = va.primary_flag
					if any (a for a in survivor.address_list if a.primary_flag is not None):
						primary_flag = None
					sa = first(a for a in survivor.address_list if a.type == va.type)
					if sa != None:
						if va > sa:
							#query = "UPDATE pta_address SET address1 = '%s', address2 = '%s', address3 = '%s', address4 = '%s', address5 = '%s', address6 = '%s', city = '%s', state_prov = '%s', postal_code = '%s', country_id = %d, primary_flag = %s WHERE id = '%s' AND date_end is null" % (va.address1,va.address2,va.address3,va.address4,va.address5,va.address6,va.city,va.state_prov,va.postal_code,va.country,va.primary_flag,sa.id)
							msg = "Updated as a result of merge with xlsid=%d" % (victim.xlsid)
							query = "UPDATE a set a.address1=a1.address1,a.address2=a1.address2,a.address3=a1.address3,a.address4=a1.address4,a.address5=a1.address5,a.address6=a1.address6,a.city=a1.city,a.state_prov=a1.state_prov, a.postal_code=a1.postal_code, a.country_id=a1.country_id, a.primary_flag=%s, a.notes = '%s' from pta_address a, pta_address a1 where a.date_end is null and a1.date_end is null and a.id = '%s' and a1.id = '%s'" % (primary_flag,msg,sa.id,va.id)
							query = query.replace("None", "NULL")
							result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to update survivor address", "Successfully updated survivor address", cursor)
							if result.ok == False:
								return result
							for t in va.telephone_list:
								if not any(st for st in sa.telephone_list if st.number == t.number):
									msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
									query = "INSERT INTO pta_telephone (address_id, telephone_type, telephone_number, notes) values('%s', %d,'%s','%s')" % (sa.id, t.type, t.number, msg)
									result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged telephone", "Successfully added merged telephone", cursor)
									if result.ok == False:
										return result

					else:
						msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
						address_id = str(uuid.uuid4()).upper()
						query = "INSERT INTO pta_address (id, xlsid, address_type, address1, address2, address3, address4, address5, address6, city, state_prov, postal_code, country_id, primary_flag, notes) values (%s, %d, %d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %d, %s, %s)"
						query = query.replace("None", "NULL")
						params = (address_id, survivor.xlsid, va.type,va.address1,va.address2,va.address3,va.address4,va.address5,va.address6,va.city,va.state_prov,va.postal_code,va.country, primary_flag, msg)
						result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged address", "Successfully added merged address", cursor,params)
						if result.ok == False:
							return result

						for vt in va.telephone_list:
							msg = "Added as a result of merge with xlsid=%d" % (victim.xlsid)
							query = "INSERT INTO pta_telephone (address_id, telephone_type, telephone_number, notes) values('%s', %d,'%s','%s')" % (address_id, vt.type, vt.number, msg)
							result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged telephone", "Successfully added merged telephone", cursor)
							if result.ok == False:
								return result


				# Deleting victim entity if it's not in a base feed or it is but those base feeds are not protected and survivor been mapped to a higher priority base feeds
				#if (victim.baseFeed == False) or (not any(m for m in victim.mappings if m["source"] in set(b.source for b in baseFeeds if b.protected_flag == 1)) and (victim.baseWeight < survivor.baseWeight)):
				if (victim.baseFeed == False or (victim.isDupe == False and not any(m for m in victim.mappings if m["source"] in set(b.source for b in baseFeeds if b.protected_flag == 1))) ):
					query = "UPDATE pta_entity set date_end = GETDATE(), notes = 'Deleted as a result of merge with xlsid=%d', forward_ptr = %d  where id = %d AND date_end is null" % (survivor.xlsid, survivor.xlsid, victim.xlsid)
					result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to delete victim entity", "Successfully deleted victim entity", cursor)
					if result.ok == False:
						return result

				if (victim.isDupe == True):
					# Marking victim entity as a duplicate if it has been mapped to the equal priority base feeds.
					query = "UPDATE pta_entity set research_status=11,  is_dupe_of = %d, notes = 'Marked as a dupe as a result of merge with xlsid=%d' where id = %d AND date_end is null" % (survivor.xlsid, survivor.xlsid, victim.xlsid)
					result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to label victim entity as a dupe", "Successfully labeled victim entity as a dupe", cursor)
					if result.ok == False:
						return result
					result.updOk,result.updMsg = AddToUBS_Parking_dups(survivor, victim, cursor)
					if result.updOk == True:
						result.updOk,result.updMsg = AddToUBS_Parking_dups(victim, survivor, cursor)

				# Moving the URL
				if (survivor.url == None and victim.url != None):
					query = "UPDATE pta_entity set url = '%s' where id = %d and date_end is null" % (victim.url,survivor.xlsid)
					result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to add merged URL", "Successfully added merged URL", cursor)
					if result.ok == False:
						return result

				# Removing research status on the entity being kept (survivor)
				query = "UPDATE pta_entity set research_status=NULL, is_dupe_of=NULL where id = %d AND date_end is null" % (survivor.xlsid)
				result.ok, result.msg = execNoCommit(query, "Error merging entities. Unable to remove research status from the survivor entity", "Successfully removed research status from the survivor entity", cursor)
				if result.ok == False:
					return result

			except pymssql.DatabaseError, ex:
				exc_type, exc_obj, exInfo = sys.exc_info()
				result.ok = False
				result.msg = "Unable to merge entities. Database Exception (Message) : %s" % (ex[1])
			except:
				result.ok = False
				result.msg = "Unable to merge entities. Error : %s" % (sys.exc_info()[0])

			if result.ok == True:
				cursor.connection.commit()

	mergedXlsids.add(survivor.xlsid)
	mergedXlsids.add(victim.xlsid)

	print result.msg

	return result

def InExtract(xlsid, cursor):
	result = False
	msg = ""
	query = "if exists(select 1 from ipid_ubs_entity_extract where xlsid = %d) select 1 as yes else select 0 as yes" % (xlsid)

	try:
		cursor.execute(query)
		for row in cursor:
			result = True if row['yes'] == 1 else False

	except pymssql.DatabaseError, ex:
		msg = 'Unable to check precondition for xlsid = %d. Database Exception (Message) : %s' % (xlsid, ex[1])
	except:
		msg = 'Unable to check precondition for xlsid = %d. Error : %s' % (xlsid, sys.exc_info()[0])

	print msg

	return result


def PreconditionViolated(merge, cursor):
	msg = ""
	result = False
	result1 = InExtract(merge.xlsid1, cursor)
	result2 = InExtract(merge.xlsid2, cursor)
	if result1 == True and result2 == True:
		result = True
		msg = "Couldn't merge because both xlsid=%d and xlsid=%d exist in ipid_ubs_entity_extract table." % (merge.xlsid1, merge.xlsid2)
	elif result1 == True:
		result = True
		msg = "Couldn't merge because xlsid=%d exists in ipid_ubs_entity_extract table." % (merge.xlsid1)
	elif result2 == True:
		result = True
		msg = "Couldn't merge because xlsid=%d exists in ipid_ubs_entity_extract table." % (merge.xlsid2)

	return result, msg

def LogNotMerged(bulkid, xlsid1, xlsid2, msg, cursor):
	query = "insert bulk_merge_log_details(bulkid,targetId,mergedId,log_date,status,status_descr,error_msg) select '%s', %d, %d, getdate(), %d, '%s'" % (bulkid, xlsid1, xlsid2, -2, 'did not merge')
	query = query + ", '%s'" % (msg.replace('\'', '\'\''))
	return execAndCommit(query, "Unable to report not merged condition", "Successfully reported not merged condition", cursor)

def LogMergeDetails(bulkid, survivor, victim, result, cursor):
	ok = True
	msg = ""
	if hasattr(result, 'status'):
		if result.status == -2:
			return LogNotMerged(bulkid, survivor.xlsid, victim.xlsid, result.msg, cursor)

	if result.ok and result.updOk:
		status = 2
		desc = "merged"
	elif result.ok:
		status=3
		desc = "merged"
		msg = result.updMsg
	else:
		status = -1
		desc = "failed"
		msg = result.msg

	query = "insert bulk_merge_log_details(bulkid,targetId,mergedId,log_date,status,status_descr,error_msg) select '%s', %d, %d, getdate(), %d, '%s'" % (bulkid, survivor.xlsid, victim.xlsid, status, desc)
	if status == 2:
		query += ", null"
	else:
		query = query + ", '%s'" % (msg.replace('\'', '"'))

	ok, msg = execAndCommit(query, "Unable to save merge details", "Successfully saved merge details", cursor)

	return

def LogFinishMerge(bulkid, status, msg, cursor):
	ok = True
	if status != -1:
		query = "UPDATE bulk_merge_log set date_finished = getdate(), status = %d, msg = '%s' where session = '%s'" % (status, msg, bulkid)
	else:
		query = "UPDATE bulk_merge_log set status = %d, msg = '%s' where session = '%s'" % (status, msg, bulkid)

	ok, msg = execAndCommit(query, "Unable to save bulk merge results", "Successfully saved bulk merge results", cursor)

	print msg
			
	return ok

def LoadMergePairs(bulkid, cursor):
	ok = True
	msg = ""
	try:
		cursor.callproc('spGetBulkMergePairs', (bulkid,))
		for row in cursor:
			m = Merge(row['xlsid1'], row['xlsid2'])
			merges.append(m)
	except pymssql.DatabaseError, ex:
		ok = False
		msg = 'Unable to get bulk_merge pairs. Database Exception (Message) : %s' % (ex[1])
	except:
		ok = False
		msg = 'Unable to get bulk_merge pairs. Error : %s' % (sys.exc_info()[0])

	return ok, msg

def LoadBaseFeeds(cursor):
	ok = True
	msg = ""
	try:
		cursor.callproc('spGetBaseFeeds')
		for row in cursor:
			b = BaseFeed(row['source'], row['name'], row['weight'], row['check_dupe_query'], row['protected_flag'], row['check_dead_mapping'])
			baseFeeds.append(b)
	except pymssql.DatabaseError, ex:
		ok = False
		msg = 'Unable to load base feeds. Database Exception (Message) : %s' % (ex[1])
	except:
		ok = False
		msg = 'Unable to load base feeds. Error : %s' % (sys.exc_info()[0])

	return ok, msg

def LoadMultipleMapsSources(cursor):
	ok = True
	msg = ""
	try:
		cursor.callproc('spGetMultipleMapsSources')
		for row in cursor:
			s = MultipleMapsSource(row['source'], row['name'])
			multipleMapsSources.append(s)
	except pymssql.DatabaseError, ex:
		ok = False
		msg = 'Unable to load multiple maps sources. Database Exception (Message) : %s' % (ex[1])
	except:
		ok = False
		msg = 'Unable to load multiple maps sources. Error : %s' % (sys.exc_info()[0])

	return ok, msg

def LoadBbgMappings(bbgMapping1, bbgMapping2, cursor):
	ok = True
	msg = ""
	try:
		cursor.callproc('spGetBbgEntities', (bbgMapping1, bbgMapping2))
		for row in cursor:
			b = BbgEntity(row['bbgid'], row['nametype'], row['ctprelationship'], row['parentid'])
			bbgEntities.append(b)
	except pymssql.DatabaseError, ex:
		ok = False
		msg = 'Unable to load bbg entities. Database Exception (Message) : %s' % (ex[1])
	except:
		ok = False
		msg = 'Unable to load bbg entities. Error : %s' % (sys.exc_info()[0])

	return ok, msg

def isBBGNonduplicate(survivor, victim, cursor):
	# check to see if both xlsids have bloomberg mappings
	bbgMapping1 = getMapping(survivor, 472)
	if bbgMapping1==None:
		return False
	bbgMapping2 = getMapping(victim, 472)
	if bbgMapping2==None:
		return False
	print("bbgMapping1=%s, bbgMapping2=%s" % (bbgMapping1, bbgMapping2))
	#since both entities have bloomberg mappings, load the mappings
	LoadBbgMappings(bbgMapping1, bbgMapping2, cursor);
	print("Successfully loaded bloomberg mappings")
    #check both mappings were successfully loaded
	if len(bbgEntities) != 2:
		return False	
    #compare issuer name types
	if namePairsNonduplicate(bbgEntities[0].nametype, bbgEntities[1].nametype):
		return True	
    #compare company to parent relationship (relationship not null and parent is other id)	
	if (bbgEntities[0].ctprelationship!=None and bbgEntities[0].parentid==bbgEntities[1].bbgid) or (bbgEntities[1].ctprelationship!=None and bbgEntities[1].parentid==bbgEntities[0].bbgid):	
		return True	
	return False

def isRatedNonduplicate(survivor, victim, cursor):
	# check to see if fitch, moodys, or S&P mappings are nonduplicates
	if isFitchNonduplicate(survivor, victim, cursor) or isMoodysNonduplicate(survivor, victim, cursor) or isSPNonduplicate(survivor, victim, cursor):
		return True
	return False

def getMapping(entity, source):
	for i in range (0, len(entity.mappings)):
		if entity.mappings[i]["source"]==source:
		    return entity.mappings[i]["sourcekey"]
		    break
	return None

def namePairsNonduplicate(nametype1, nametype2):
	if("FundMgr" in nametype1 and "Fund" in nametype2) or ("Fund" in nametype1 and "FundMgr" in nametype2):
		return True
	if("Fundfamily" in nametype1 and "Fund" in nametype2) or ("Fund" in nametype1 and "Fundfamily" in nametype2):
		return True
	return False

def isFitchNonduplicate(survivor, victim, cursor):
	fitchMapping1 = getMapping(survivor, 229)
	if fitchMapping1==None:
		return False
	fitchMapping2 = getMapping(victim, 229)
	if fitchMapping2==None:
		return False
	print("fitchMapping1=%s, fitchMapping2=%s" % (fitchMapping1, fitchMapping2))
	cursor.callproc('spGetRatedFitch', (fitchMapping1,))
	for row in cursor:
		rated1=row['rated_entity']
	cursor.callproc('spGetRatedFitch', (fitchMapping2,))
	for row in cursor:
		rated2=row['rated_entity']
	if rated1=="true" and rated2=="true":
		return True
	return False

def isMoodysNonduplicate(survivor, victim, cursor):
	moodysMapping1 = getMapping(survivor, 56)
	if moodysMapping1==None:
		return False
	moodysMapping2 = getMapping(victim, 56)
	if moodysMapping2==None:
		return False
	print("moodysMapping1=%s, moodysMapping2=%s" % (moodysMapping1, moodysMapping2))
	cursor.callproc('spGetRatedMoodys', (moodysMapping1,))
	for row in cursor:
		rated1=row['rated_entity']
	cursor.callproc('spGetRatedMoodys', (moodysMapping2,))
	for row in cursor:
		rated2=row['rated_entity']
	if rated1=="true" and rated2=="true":
		return True
	return False

def isSPNonduplicate(survivor, victim, cursor):
	SPMapping1 = getMapping(survivor, 232)
	if SPMapping1==None:
		return False
	SPMapping2 = getMapping(victim, 232)
	if SPMapping2==None:
		return False
	print("SPMapping1=%s, SPMapping2=%s" % (SPMapping1, SPMapping2))
	cursor.callproc('spGetRatedSP', (SPMapping1,))
	for row in cursor:
		rated1=row['rated_entity']
	cursor.callproc('spGetRatedSP', (SPMapping2,))
	for row in cursor:
		rated2=row['rated_entity']
	if rated1=="true" and rated2=="true":
		return True
	return False

def xlsidsValid(survivor, victim, cursor):
	#stop merge if same pair
	if survivor.xlsid == victim.xlsid:
		return False
	#stop merge if xlsid has already been previously merged in this set
	if survivor.xlsid in mergedXlsids or victim.xlsid in mergedXlsids:
		return False
	#stop merge if either alacraid inactive
	cursor.callproc('spGetInactiveDate', (survivor.xlsid,))
	for row in cursor:
		inactiveDate1=row['inactive_date']
	cursor.callproc('spGetInactiveDate', (victim.xlsid,))
	for row in cursor:
		inactiveDate2=row['inactive_date']
	if inactiveDate1 != None or inactiveDate2 != None:
		return False
	return True

def Initialize(bulkid, cursor):
	ok = True
	msg = ""
	query = "Update bulk_merge_log Set status=1, date_started = getdate() Where session = '%s'" % (bulkid)
	ok, msg = execAndCommit(query, "Unable to update bulk_merge_log", "Successfully updated bulk_merge_log", cursor)
	if ok == False:
		return ok, msg

	ok, msg = LoadBaseFeeds(cursor)
	if ok == False:
		return ok, msg

	ok, msg = LoadMultipleMapsSources(cursor)
	if ok == False:
		return ok, msg

	ok, msg = LoadMergePairs(bulkid, cursor)
	if ok == False:
		return ok, msg

	query = "delete bulk_merge_log_details where bulkid = '%s'" % (bulkid)
	ok, msg = execAndCommit(query, "Unable to clear merge details", "Successfully cleared merge details", cursor)

	return ok, msg



def main(argv):
	bulkid = ''
	cp = ConnectionParams()

	try:
		opts, args = getopt.getopt(argv,"hi:s:u:p:",["bulkid=","server=","user=","password="])
	except getopt.GetoptError:
		print 'mergerUBS.py -i <bulkid> [-s <server>], [-u <user>], [-p <password>]'
		sys.exit(2)
	for opt, arg in opts:
		if opt == '-h':
			print 'mergerUBS.py -i <bulkid> [-s <server>], [-u <user>], [-p <password>]'
			sys.exit()
		elif opt in ("-i", "--bulkid"):
			bulkid = arg
		elif opt in ("-s", "--server"):
			cp.server = arg
		elif opt in ("-u", "--user"):
			cp.user = arg
		elif opt in ("-p", "--password"):
			cp.password = arg

	print 'Bulkid is ', bulkid
	print 'Server is ', cp.server
	print 'User is ', cp.user
	print 'Password is ', cp.password

	helperConn = pymssql.connect(cp.server, cp.user, cp.password, "concordance",as_dict = True)
	helperCursor = helperConn.cursor()

	ok = True
	with pymssql.connect(cp.server, cp.user, cp.password, "concordance") as conn:
		with conn.cursor(as_dict=True) as cursor:

			ok, msg = Initialize(bulkid, cursor)
			if ok == False:
				status = -1
				return LogFinishMerge(bulkid, status, msg, cursor)

			total = len(merges)
			succeded = 0
			failed = 0

			for m in merges:
				print("xlsid1=%d, xlsid2=%d" % (m.xlsid1, m.xlsid2))
				#skip, msg = PreconditionViolated(m, cursor)
				#if skip:
				#	LogNotMerged(bulkid, m.xlsid1, m.xlsid2, msg, cursor)
				#	continue

				entity1 = Entity(m.xlsid1, True)
				entity2 = Entity(m.xlsid2, False)

				ok, msg = cursorRoutine(entity1, cursor, helperCursor)
				if ok == False:
					LogNotMerged(bulkid, m.xlsid1, m.xlsid2, msg, cursor)
					continue

				ok, msg = cursorRoutine(entity2, cursor, helperCursor)
				if ok == False:
					LogNotMerged(bulkid, m.xlsid1, m.xlsid2, msg, cursor)
					continue
		
				ok, msg = setSurvivor(entity1, entity2, cursor)
				if ok == False:
					LogNotMerged(bulkid, m.xlsid1, m.xlsid2, msg, cursor)
					continue

				survivor = entity1 if (entity1.survivor) else entity2
				victim = entity2 if (entity1.survivor) else entity1
				result = mergeEntities(survivor, victim, cp, cursor)
				LogMergeDetails(bulkid, survivor, victim, result, cursor)
				if result.ok and result.updOk:
					succeded += 1
				elif result.ok != True:
					failed += 1

			if total == succeded:
				status = 2
				msg = "All %d pairs have been merged successfully" % (total)
			elif total == failed:
				status = -1
				msg = "All %d merges have failed" % (failed)
			elif total == succeded + failed:
				status = 3
				msg = "%d pairs out of %d have been merged. %d merges have failed." % (succeded, total, failed)
			elif total > succeded + failed:
				status = 3
				msg = "%d pairs out of %d have been merged. %d pairs couldn''t be merged. %d merges have failed." % (succeded, total, total - succeded - failed, failed)

			helperConn.close()

			return LogFinishMerge(bulkid, status, msg, cursor)



	
if __name__ == "__main__":
	ok = main(sys.argv[1:]) 
	result = 0 if (ok) else 1
	sys.exit(result)

