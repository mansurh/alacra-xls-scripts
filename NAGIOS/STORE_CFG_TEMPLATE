################################################################################
# Sample object config file for Nagios 
#
# Read the documentation for more information on this configuration file.  I've
# provided some comments here, but things may not be so clear without further
# explanation, so make sure to read the HTML documentation!
# 
################################################################################


################################################################################
# SERVICE DEFINITIONS
#
# SYNTAX:
#
################################################################################

# Generic service definition template
define service{
        name                            generic-service ; The 'name' of this service template, referenced in other service definitions
        active_checks_enabled           1       ; Active service checks are enabled
        passive_checks_enabled          1       ; Passive service checks are enabled/accepted
        parallelize_check               1       ; Active service checks should be parallelized (disabling this can lead to major performance problems)
        obsess_over_service             1       ; We should obsess over this service (if necessary)
        check_freshness                 0       ; Default is to NOT check service 'freshness'
        notifications_enabled           1       ; Service notifications are enabled
        event_handler_enabled           1       ; Service event handler is enabled
        flap_detection_enabled          1       ; Flap detection is enabled
        process_perf_data               1       ; Process performance data
        retain_status_information       1       ; Retain status information across program restarts
        retain_nonstatus_information    1       ; Retain non-status information across program restarts

        register                        0       ; DONT REGISTER THIS DEFINITION - ITS NOT A REAL SERVICE, JUST A TEMPLATE!
        }
#---------------------  [HOSTNAME] --------------------------
# Service definition
define service{
        use                             generic-service         ; Name of service template to use

        host_name                       [HOSTNAME]
        service_description             HTTP
        is_volatile                     0
        check_period                    24x7
        max_check_attempts              3
        normal_check_interval           5
        retry_check_interval            1
        contact_groups                  MSWin-admins
        notification_interval           120
        notification_period             24x7
        notification_options            c,r
        check_command                   check_http
        }
# Service definition
define service{
        use                             generic-service         ; Name of service template to use

        host_name                       [HOSTNAME]
        service_description             PING
        is_volatile                     0
        check_period                    24x7
        max_check_attempts              3
        normal_check_interval           5
        retry_check_interval            1
        contact_groups                  MSWin-admins
        notification_interval           120
        notification_period             24x7
        notification_options            c,r
        check_command                   check_ping!100.0,20%!500.0,60%
        }
# Service definition
define service{
	use                             generic-service         ; Name of service template to use

	host_name                       [HOSTNAME]
	service_description             CPU Load
	is_volatile                     0
	check_period                    24x7
	max_check_attempts              3
	normal_check_interval           5
	retry_check_interval            1
	contact_groups                  MSWin-admins
	notification_interval           120
	notification_period             24x7
	notification_options            c,r
	check_command                   check_nt_cpu!5,80,95
}
define service{
	use                             generic-service         ; Name of service template to use

	host_name                       [HOSTNAME]
	service_description             Web CGI Requests per Second
	is_volatile                     0
	check_period                    24x7
	max_check_attempts              3
	normal_check_interval           5
	retry_check_interval            1
	contact_groups                  MSWin-admins
	notification_interval           120
	notification_period             24x7
	notification_options            c,r
	check_command                   check_nt_cgi
}
define service{
	use                             generic-service         ; Name of service template to use

	host_name                       [HOSTNAME]
	service_description             Web MAX CGI Requests
	is_volatile                     0
	check_period                    24x7
	max_check_attempts              3
	normal_check_interval           5
	retry_check_interval            1
	contact_groups                  MSWin-admins
	notification_interval           120
	notification_period             24x7
	notification_options            c,r
	check_command                   check_nt_max_cgi
}
define service{
	use                             generic-service         ; Name of service template to use

	host_name                       [HOSTNAME]
	service_description             Web MAX Connections
	is_volatile                     0
	check_period                    24x7
	max_check_attempts              3
	normal_check_interval           5
	retry_check_interval            1
	contact_groups                  MSWin-admins
	notification_interval           120
	notification_period             24x7
	notification_options            c,r
	check_command                   check_nt_max_conn
}
define service{
	use                             generic-service         ; Name of service template to use

	host_name                       [HOSTNAME]
	service_description             Web Current Connections
	is_volatile                     0
	check_period                    24x7
	max_check_attempts              3
	normal_check_interval           5
	retry_check_interval            1
	contact_groups                  MSWin-admins
	notification_interval           120
	notification_period             24x7
	notification_options            c,r
	check_command                   check_nt_conn
}

# Service definition
define service{
        use                             generic-service         ; Name of service template to use

        host_name                       [HOSTNAME]
        service_description             WIN SPACE C:
        is_volatile                     0
        check_period                    24x7
        max_check_attempts              3
        normal_check_interval           5
        retry_check_interval            1
        contact_groups                  MSWin-admins
        notification_interval           120
        notification_period             24x7
        notification_options            c,r
        check_command                   check_nt_disk!C!70!90
        }
define service{
        use                             generic-service         ; Name of service template to use

        host_name                       [HOSTNAME]
        service_description             Consumed Handles
        is_volatile                     0
        check_period                    24x7
        max_check_attempts              3
        normal_check_interval           15
        retry_check_interval            1
        contact_groups                  MSWin-admins
        notification_interval           120
        notification_period             24x7
        notification_options            c,r
        check_command                   check_nt_handles!19000!25000
        }
define service{
        use                             generic-service         ; Name of service template to use

        host_name                       [HOSTNAME]
        service_description             Resource Online in DBA2
        is_volatile                     0
        check_period                    24x7
        max_check_attempts              3
        normal_check_interval           15
        retry_check_interval            1
        contact_groups                  MSWin-admins
        notification_interval           120
        notification_period             24x7
        notification_options            c,r
        check_command                   check_online![HOSTNAME]
        }


# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		BaMP Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!bamp!135059784!'Northwest Airlines'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Business and Industry Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!bni!135115212!'Parthenon Still Biting'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Dun and Bradstreet Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!dnb2!5356613!'GENERAL MOTORS CORPORATION'!60!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Credit Sights Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!cs!29775!'Suicide Watch'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Tablebase Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!tblbase!132126058!'top five convenience store operators'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Amlaw News Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!almnews!498587!'Boost Profits'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Moodys Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!moodys!RU_808254224_15677523!'AFFIRMS'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Fitch Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!fitch!195610_pr_frame!'Waxahachie'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Forrester Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!forrester!33750!'Icing On A Half-Baked Cake'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		SourceMedia Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!tfmedia!20050815hyr_00006!'US Company News'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		MarkIntel Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!markintel!5643114!'DRINK PACKAGING'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		StreetEvents Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!ccbn!T1066828!'BRAVO FOODS INTL CORP'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		TFShare Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!tfshare!24620!'General Motors'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		IBISWorld Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!iworld!US44529!'Other Specialty Food Stores'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Oxford Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!oxford!DB108668!'Food security'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		SharkRepellent Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!srp!23274!'Snack Foods'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Snapshots Intl Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!snapshots!uk311783!'UK Electrical Retailing'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Datamonitor Company Profile Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!datamonitor!685!'General Motors Corporation'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Gale Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!bir!1143172!'Check Point'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Hemscott Companies Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!hmsc!484-xls!'Pearson PLC'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Hemscott People Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!hmsp!43341!'Scardino'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Softbase Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!softbase!154497!'Macintosh'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Freedonia Focus Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!ffocus!10015!'Freedonia Focus on Frozen Foods'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Reed Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!rbi!RI081505_14!'Creating an Oasis'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Datamonitor Country Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!datamonitorcntry!ohec4667!'Switzerland'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Datamonitor Industry Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!datamonitormarket!OHME2214!'Chemicals in India'!20!
	}

# Service definition
define service{
	use				generic-service		; Name of service template to use
	host_name			[HOSTNAME]
	service_description		Datamonitor Premium Landing Page on server [HOSTNAME]
	is_volatile			0
	check_period			24x7
	max_check_attempts		3
	normal_check_interval		10
	retry_check_interval		1
	contact_groups			STORE-admins
	notification_interval		120
	notification_period		24x7
	notification_options		c,r
	check_command			check_landing_page![HOSTNAME]p!datamonitoress!ohec4909!'MarketWatch'!20!
	}
