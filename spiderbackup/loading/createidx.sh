index=$1
indexpath=$XLSDATA/index

if [ $# != 1 ]
then
	echo "Usage: $0 index "
	echo "ex. $0 co "
	exit 1
fi

cd $indexpath

if [ $? != 0 ]
then
	mkdir $indexpath
	cd $indexpath
fi

logfile=${indexpath}/${index}.log
echo "" > ${logfile} 2>&1

gisget $COMPUTERNAME "regkey=${index}&topic=_lockfileset" >> ${logfile} 2>&1
gisget $COMPUTERNAME -p 4888 "regkey=${index}&topic=_lockfileset" >> ${logfile} 2>&1

buildinv -${index} >> ${logfile} 2>&1

if [ $? != 0 ]
then
	echo "Error creating index, exiting ... "
	blat ${logfile} -t "administrators@xls.com" -s "Buildinv Index error: ${logfile}"
	exit 1
fi
gisget $COMPUTERNAME "regkey=${index}&topic=_unlockfileset" >> ${logfile} 2>&1
gisget $COMPUTERNAME -p 4888 "regkey=${index}&topic=_unlockfileset" >> ${logfile} 2>&1
exit 0
