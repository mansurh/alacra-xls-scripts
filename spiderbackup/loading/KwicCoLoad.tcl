#KwicCoLoad.tcl			build kwic tables kwic tk 1/22/97
# sybtcl kwiccoload.tcl <source db> <source table> <source keyfield> <source searchfield> <server> <user> <pass>
# output to stdout
# Connect to the data server
# set dbhandle [sybconnect xls xls hawaii]
set dbhandle [sybconnect [string trim [lindex $argv 5 ] ] [string trim [ lindex $argv 6 ] ] [string trim [lindex $argv 4] ] ]

# build the string to select out of existing table
set selstr [concat "select " [string trim [string trim [lindex $argv 2] ] ] ]
set selstr [concat $selstr, [string trim [string trim [lindex $argv 3] ] ] ]
set selstr [concat $selstr from [string trim [lindex $argv 0] ]..[string trim [lindex $argv 1] ] ]
set selstr [concat $selstr [string trim [lindex $argv 7] ] ]

#REMOVE IN FINAL PROGRAM
#set selstr [concat $selstr where account < '0100' ]

# Select all ids and names
# puts "selecting data from source table"
# puts $selstr
sybsql $dbhandle $selstr
sybnext $dbhandle {
#	set source([string trim @1]) "[string toupper [string trim @2] ]"
#	set name [array next source $ahandle]
#	puts "$name $source($name)"
	set name [string trim @1]
        set PARTS [string toupper @2]
	regsub -all {[{}(),;:]} $PARTS { } PARTS
	set PARTS [string trim $PARTS]
	regsub -all {[ ]+} $PARTS { } PARTS
	set MessOParts [split $PARTS { }]
	set endo [llength $PARTS]
#	incr endo -1
	set parts [lrange $MessOParts 0 $endo]
	
	set numparts [llength $parts]
	set forbidden {- / & A/S AB A.B. AG A.G. AKA BHD BHD. BV B.V. CO CO. CO. CORP CORP. COMPANY CORPORATION CV C.V. DE F.K.A FKA GMBH GMBH. I II III INC INC. IV KFT KG K.G. LDA LLC LLC. LP L.P. LTD LTD. MBH NV N.V. OF PLC PLC. PTE PTE. PTE., PTY PTY. PTY SA S.A. SDN SDN. SP S.P.A. SPA SRL S.R.L. SRO S.R.O V VI THE ZOO}
					#$numparts
	for {set i [ expr 0 ] } { $i < $endo } {incr i} {
                set goodrec [lsearch $forbidden [string trim [lindex $parts $i] ] ]
		if {$goodrec == -1} {
			set junk [lrange $parts $i end]
			if {$junk != ""} {
				puts "$name|$junk"		
			}
		}
	}
}


#clean up
sybclose $dbhandle
