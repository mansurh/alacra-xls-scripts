DECLARE @formid int
DECLARE @uuid varchar(128)


SELECT @formid = 1, @uuid = '${_uuid,s}'

INSERT formsubmits VALUES (@uuid, @formid, '${_ip,s}', GETDATE())

INSERT formstrvars VALUES (@uuid, @formid, 'first', '${first,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'last', '${last,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'company', '${company,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'email', '${email,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'job', '${job,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'otherjob', '${otherjob,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'easy2use', '${easy2use,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'logicallyorganized', '${logicallyorganized,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'reliable', '${reliable,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'searchscreens', '${searchscreens,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'ifindwhatineed', '${ifindwhatineed,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'hasimproved', '${hasimproved,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'customercare', '${Customercare,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'updates', '${updates,s}')

INSERT formtextvars VALUES (@uuid, @formid, 'OneChange', '${OneChange,s}')
INSERT formtextvars VALUES (@uuid, @formid, 'enhancements', '${enhancements,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'isearchfor', '${isearchfor,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'copageempsize', '${copageempsize,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'copagecharts', '${copagecharts,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'copagetopcomp', '${copagetopcomp,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'copagenews', '${copagenews,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'copageir', '${copageir,s}')
INSERT formtextvars VALUES (@uuid, @formid, 'othercopgdata', '${othercopgdata,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'useDBMIorCR', '${useDBMIorCR,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'getDBfrom', '${getDBfrom,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'otherdbprovider', '${otherdbprovider,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'ABI', '${ABI,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'BarraTR', '${BarraTR,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Cahners', '${Cahners,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Compustat', '${Compustat,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'DBMI', '${DBMI,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'DBCR', '${DBCR,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'DBBIR', '${DBBIR,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Hoovers', '${Hoovers,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Lionshares', '${Lionshares,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Moody', '${Moody,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Sectorbase', '${Sectorbase,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'SPRD', '${SPRD,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'ValueLine', '${ValueLine,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Worldscope', '${Worldscope,s}')

INSERT formstrvars VALUES (@uuid, @formid, 'FactSet', '${FactSet,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'Piranha', '${Piranha,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'investin', '${investin,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'AlacraModel', '${AlacraModel,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'heardofAlacrabook', '${heardofAlacrabook,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'wantdemo', '${wantdemo,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'prod_ind', '${prod_ind,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'opersys', '${opersys,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'excelversion', '${excelversion,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'browser', '${browser,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'otherbrowser', '${otherbrowser,s}')
INSERT formstrvars VALUES (@uuid, @formid, 'browserversion', '${browserversion,s}')
INSERT formtextvars VALUES (@uuid, @formid, 'comments', '${comments,s}')

