# Script file to generate the IP matching reports for extel
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_ibis.sh reportfile IPID_server IPID_login IPID_password"
	exit 1
fi

REPORTFILE=$1
IPIDserver=$2
IPIDlogin=$3
IPIDpassword=$4

ipname=ibes

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${IPIDserver} -U${IPIDlogin} -P${IPIDpassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in ibes"
select * from ipid_ibes
where ticker not in (select sourcekey from company_map where source=29)


/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from ibes"
select x.id, x.name from company x, company_map m
where
x.id = m.xlsid and m.source=29
and
m.sourcekey not in (select ticker from ipid_ibes)

/* Optional report - new information available in database */

HERE
