IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
				 WHERE
                 TABLE_NAME = 'ipid_onesource' and TABLE_TYPE = 'BASE TABLE'))
BEGIN
	drop table ipid_onesource
END

Create Table [dbo].[ipid_onesource](
       [matchkey] varchar (12) not null,
	   [entity_type] varchar(100) null,
       [name] [varchar](256) null,
	   [RegAddress1] varchar(200),
	   [City] varchar(100),
	   [state_prov] varchar(100) null,
       [Country] varchar(100) null,
	   [ISIN] [varchar](100) NULL,
       [Ticker] varchar(100) null
) ON [PRIMARY]