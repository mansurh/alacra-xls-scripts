XLSUTILS=$XLS/src/scripts/loading/dba

if [ $# -lt 5 ]
then
	echo "usage: $0 server login password ipid_table filecount"
	exit 1
fi

IPIDserver=$1
IPIDlogin=$2
IPIDpassword=$3
TABNAME=$4
filecount=$5

${XLSUTILS}/reccount.sh ${IPIDserver} ${IPIDlogin} ${IPIDpassword} ${TABNAME}_new ${TABNAME}_cnt.tmp
if [ $? -ne 0 ]
then
	echo "error in reccount.sh, exiting ..."
	exit 1
fi

typeset -i tablecount=`cat ${TABNAME}_cnt.tmp`
echo "Row count for ${TABNAME}_new: ${tablecount}"
echo "File row count: ${filecount}"

# do a silly sanity check
if [ ${filecount} -gt `expr ${tablecount} + 10` ]
then
	echo "Counts different: $filecount $tablecount, exiting "
	exit 1
fi

if [ ${tablecount} -lt 10 ]
then
	echo "Count for new table is $tablecount (less than 10), exiting "
	exit 1
fi

${XLSUTILS}/reccount.sh ${IPIDserver} ${IPIDlogin} ${IPIDpassword} ${TABNAME} ${TABNAME}_old.tmp
if [ $? -ne 0 ]
then
	echo "error in reccount.sh (old table), exiting ..."
	exit 1
fi
typeset -i tablecount_old=`cat ${TABNAME}_old.tmp`
echo "Row count for ${TABNAME}: ${tablecount_old}"

#Check for a major change in row counts between old and new tables
typeset -i diff=20
typeset -i max=`expr 100 + $diff`
typeset -i min=`expr 100 - $diff`
if [ ${tablecount} -ge `expr ${tablecount_old} \* $max \/ 100` ] || [ ${tablecount} -le `expr ${tablecount_old} \* $min \/ 100` ]
then
	echo "Counts between old ($tablecount_old) and new ($tablecount) ipid tables are different by ${diff} %, exiting "
	exit 1
fi

if [ ${tablecount} -eq ${tablecount_old} ]
then
	blat - -body "Counts between new and old $TABNAME are the same ($tablecount)" -s "$TABNAME counts are the same after update" -t ipidalerts@alacra.com
fi
