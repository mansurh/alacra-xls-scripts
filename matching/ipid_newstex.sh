# Parse the command line arguments.
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password

ARGS=4
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_newstex.sh IPID_server IPID_database IPID_login IPID_password server user pwd"
	exit 1
fi


# Save the runstring parameters in local variables
ipid_server=$1
ipid_db=$2
ipid_user=$3
ipid_pwd=$4

#-------------------------------------------------------

dir="$XLSDATA/newstex"
datadir="F:/data/newstex"

#-------------------------------------------------------

check_return_code()
{
ERROR_CODE=$1
MESSAGE=$2
EXIT_CODE=$3
if [ $ERROR_CODE != 0 ]
then
	echo "$MESSAGE"
	rm $UPD_PROGRESS
	exit $EXIT_CODE
fi
}

#----------------------------------------------

echo " "
echo "TRUNCATE IPID TABLE"
echo " "

isql /U${ipid_user} /P${ipid_pwd} /S${ipid_server} -Q "truncate table ipid_newstex"
check_return_code $? "Failed to truncate IPID table" 1

echo " "
echo "BUILD BCP FILE"
echo " "

nxshred.exe -ipid ${datadir}
check_return_code $? "nxshred.exe failed" 1

echo " "
echo "BCP"
echo " "

cd ${datadir}
check_return_code $? "Failed to change directory to ${datadir}" 1

echo "bcp..."
bcp ipid_newstex in ipid.dat -S${ipid_server} -U${ipid_user} -P${ipid_pwd} -c -t"|"
check_return_code $? "Failed to BCP" 1

exit 0
 
