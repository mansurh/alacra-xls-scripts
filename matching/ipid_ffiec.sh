XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 1 ]
then
	echo "Usage: ipid_ffiec.sh server"
	exit 1
fi

cserver=$1

aname="ffiec"

dbname="concordance"
cuser=`get_xls_registry_value ${dbname} "User" "0"`
cpassword=`get_xls_registry_value ${dbname} "Password" "0"`

prepare_empty_backup_table()
{
	if [ $# -lt 1 ]
	then
		exit 1
	fi
	THETABLE=$1
	THETEMPTABLE=${THETABLE}${tempsuffix}

	echo "Initializing temp table ${THETEMPTABLE}"
	isql /b /S${cserver} /U${cuser} /P${cpassword} /Q "select * into ${THETEMPTABLE} from ${THETABLE} where 1 = 0"
	if [ $? -ne 0 ]; then echo "error creating ${THETEMPTABLE} table, exiting ..."; exit 1; fi;

	isql /b /S${cserver} /U${cuser} /P${cpassword} /Q "create index ${THETABLE}_01 on ${THETEMPTABLE}(FED_RSSD)"
	if [ $? -ne 0 ]; then echo "error creating index on ${THETEMPTABLE} table, exiting ..."; exit 1; fi;

}


accept_new_data()
{
	if [ $# -lt 1 ]
	then
		exit 1
	fi
	THETABLE=$1
	THETEMPTABLE=${THETABLE}${tempsuffix}

	isql /b /S${cserver} /U${cuser} /P${cpassword} /Q "drop table ${THETABLE}_backup"

	echo "Renaming ${THETEMPTABLE} to ${THETABLE}"
	isql /b /S${cserver} /U${cuser} /P${cpassword} /Q "exec sp_rename ${THETABLE}, ${THETABLE}_backup; exec sp_rename ${THETEMPTABLE}, ${THETABLE}"
	if [ $? -ne 0 ]; then echo "error creating ${THETEMPTABLE} table, exiting ..."; exit 1; fi;
}


#  SCRIPT BEGINNING

run() {


tempsuffix="_new"

ipidtable=ipid_${aname}
tempipidtable=${ipidtable}${tempsuffix}
ipidhistorytable=ipid_${aname}_history
ipidfailuretable=ipid_${aname}_failures

loaddatafile=${ipidtable}.txt
loadhistoryfile=${ipidhistorytable}.txt
loadfailurefile=${ipidfailuretable}.txt


echo "Concordance Server: $cserver"


# THESE ARE THE TABLE/FILE PAIRS to process
STANDARDLIST="${ipidhistorytable}/${loadhistoryfile} ${ipidfailuretable}/${loadfailurefile}"


#  CLEAN TEMP FILES AND RUN THE SCRAPING PROGRAM
echo "cd ${out_dir}"
cd ${out_dir}
# rm *.*
for c in $STANDARDLIST
do
	t="${c%%/*}"
	f="${c##*/}"

	if [ -e ${out_dir}/${f} ]
	then
		echo "rm ${out_dir}/${f}"
		rm ${out_dir}/${f}
	fi
done




#  CREATE EMPTY TEMP TABLES FOR CRAWLER PROGRAM TO FILL
for c in $STANDARDLIST
do
	t="${c%%/*}"
	f="${c##*/}"

	# Prepare temp table
	prepare_empty_backup_table "${t}"
done



# convert all forward slashes to backslashes
fwslashpath1=$(sed -e"s/\//\\\\/g" << HERE
${out_dir}
HERE
)


#  GET DATA
FfiecHistoryCrawler.exe "${fwslashpath1}"
if [ $? -ne 0 ]
then
	echo "Error from FfiecHistoryCrawler.exe, exiting"
	exit 1
fi



for c in $STANDARDLIST
do
	t="${c%%/*}"
	f="${c##*/}"

	# Get BEFORE count
	mysql="set nocount on select 8*count(*) from ${t}"
	isql /Q "${mysql}" /S${cserver} /U${cuser} /P${cpassword} -b -n -h-1 -w4096 -s"|" | sed -e "s/ //g" | grep -v "^$" > ${t}_row_count.tmp
	read rowcountbefore < ${t}_row_count.tmp
	rm ${t}_row_count.tmp
	echo "At start, 8 x ${t} row count = $rowcountbefore ..."

	# Get AFTER count
	mysql="set nocount on select 10*count(*) from ${t}${tempsuffix}"
	isql /Q "${mysql}" /S${cserver} /U${cuser} /P${cpassword} -b -n -h-1 -w4096 -s"|" | sed -e "s/ //g" | grep -v "^$" > ${t}${tempsuffix}_row_count.tmp
	read rowcount < ${t}${tempsuffix}_row_count.tmp
	rm ${t}${tempsuffix}_row_count.tmp
	echo "After load, 10 x new row count in ${t}${tempsuffix} = $rowcount ..."

	echo " "
	echo "-----------------------------------------------------"

	# don't check for minimum failure counts
	if [ "${t}" != "${ipidfailuretable}" ]
	then
		# check that the number of records has not decreased by more than 20%
		if [ $rowcount -lt $rowcountbefore ]
		then
			echo "There were less rows loaded then expected. Please review the data in ${t}${tempsuffix} table."
			blat ${logout} -t administrators@alacra.com -s "ERROR: ${aname} number of records is too small in table ${t}${tempsuffix}"
			exit 1
		fi
	fi
done


# NO RECORD COUNT ISSUES, MAIL REPORT & COMMIT THE NEW DATA
updatedate=`date +%Y%m%d`
reportfile=${out_dir}/${updatedate}.xlsx

emailDest="concordance_int@alacra.com"
desc="New FFIEC Records"

if [ -e ${reportfile} ]
then
	# Mail out the report.  Must be a binary attachment as UTF-8 uses the high bit
	cd ${out_dir}

	blat -to "${emailDest}" -subject "${desc}" -body "${desc}.  See Attached." -uuencode -attach ${reportfile}
	if [ $? != 0 ]
	then
		echo "Error sending report file via Blat, terminating"
		exit 1
	fi

fi


for c in $STANDARDLIST
do
	t="${c%%/*}"
	f="${c##*/}"

	accept_new_data "${t}"
done

}

out_dir=$XLSDATA/ipid_${aname}
logout=${out_dir}/ipid_${aname}`date +%y%m%d`.log

mkdir -p ${out_dir}

(run) > $logout 2>&1
if [ $? -ne 0 ]
then
   blat $logout -t administrators@alacra.com -s "FFIEC History update failed" 
   exit 1
fi
exit 0
