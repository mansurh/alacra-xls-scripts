XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/check_arguments.fn
. $XLSUTILS/check_bcp_errors.fn
. $XLSUTILS/check_return_code.fn
. $XLSUTILS/get_registry_value.fn

check_arguments $# 4 "Usage: xrefdb_othergda.sh dbserver concordancesvr dbname sourceid" 1

dbserver=$1
concsvr=$2
dbname=$3
sourceid=$4

concuser=`get_xls_registry_value concordance user`
concpass=`get_xls_registry_value concordance password`

DATADIR=$XLSDATA/${dbname}
mkdir -p ${DATADIR}
cd ${DATADIR}
check_return_code $? "Error switching to directory ${DATADIR}, exiting" 1

echo "build a list of companies covered for each collection..."
query="select distinct type, entry_text from index_entries where entry_name like 'xlsid%' and entry_text is not null and len(entry_text) > 0 order by type, entry_text"

TMPFILE=${dbname}_matching.dat
echo ""
echo "bcp \"${query}\" queryout ${TMPFILE} -S ${dbserver} -U ${dbname} -P ${dbname} -c -o ${dbname}_matching.errout"
echo ""
bcp "${query}" queryout ${TMPFILE} -S ${dbserver} -U ${dbname} -P ${dbname} -c -o ${dbname}_matching.errout

check_return_code $? "Failed to bcp out companies list, exiting" 1
check_bcp_errors ${dbname}_matching.errout 1 "Failed to bcp out companies list, exiting" 1

# delete existing company matching table records
echo "delete existing acm_company_list records where ip=${sourceid}..."
isql -S ${concsvr} -U ${concuser} -P ${concpass} -Q " delete from acm_company_list where ip = ${sourceid} "
check_return_code $? "Failed to delete existing acm_company_list records, exiting" 1

echo "load the new list of companies..."
echo ""
echo "bcp acm_company_list in ${TMPFILE} -S ${concsvr} -U ${concuser} -P ${concpass} -c -o ${dbname}_matching.errout"
echo ""
bcp acm_company_list in ${TMPFILE} -S ${concsvr} -U ${concuser} -P ${concpass} -c -o ${dbname}_matching.errout
check_return_code $? "Failed to bcp in companies list, exiting" 1
check_bcp_errors ${dbname}_matching.errout 1 "Failed to bcp in companies list, exiting" 1

compcnt=`isql -S ${concsvr} -U ${concuser} -P ${concpass} -Q " select 'count:' + convert(varchar(16), count(*)) + ':end' from acm_company_list where ip = ${sourceid} "`
compcnt=`echo ${compcnt} | sed -e "s/.*count://g" | sed -e "s/:end.*//g"`
echo "finished - companies mapped: ${compcnt}"

echo "delete all previous ${dbname} entries from company_map table"
isql -S ${concsvr} -U ${concuser} -P ${concpass} -Q " delete from company_map where source = ${sourceid} "
check_return_code $? "Failed to delete previous ${dbname} entries from company map, exiting" 1

echo "inserting new ${dbname} entries into company_map table"
isql -S ${concsvr} -U ${concuser} -P ${concpass} -Q " insert into company_map (xlsid, source, sourcekey, date, primary_key) select distinct xlsid, ip, xlsid, getdate(), null from acm_company_list where xlsid != 0 and xlsid is not null and ip = ${sourceid} "
check_return_code $? "Failed to insert new ${dbname} entries into company map, exiting" 1

rm ${TMPFILE}
rm ${dbname}_matching.errout

exit 0
