# Script file to generate the IP matching reports for ibisworld
# 1 = output filename
# 2 = ipid server
# 3 = ipid login
# 4 = ipid password
if [ $# -lt 4 ]
then
    echo "Usage: ipid_ibisworld_report.sh reportfile ipid_server ipid_login ipid_password"
    exit 1
fi

REPORTFILE=$1
IPIDSERVER=$2
IPIDLOGIN=$3
IPIDPASSWORD=$4

ipname=ibisworld

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${IPIDSERVER} -U${IPIDLOGIN} -P${IPIDPASSWORD} -n -w1000 -h-1 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
select "New or updated company reports:"
select "-------------------------------"
select ipid.name + '|' + ipid.ANZSIC + '|' + convert(varchar(16), ipid.last_update, 101)
from ipid_ibisworld ipid
where ipid.matchkey not in (select sourcekey from company_map where source = 280)

/* Required Report - Companies which have disappeared from database */
select ""
select "Matched companies that have disappeared from ibisworld:"
select "-------------------------------------------------------"
select convert(varchar(16), cmp.id) + '|' + cmp.name 
from company cmp, company_map map
where
    cmp.id = map.xlsid and 
    map.source = 280 and
    map.sourcekey not in (select distinct matchkey from ipid_ibisworld ipid)

HERE
