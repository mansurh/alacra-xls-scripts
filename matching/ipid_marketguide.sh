# Script file to build the IP matching table for Investext
# 1 = IPID Server
# 2 = IPID database
# 3 = IPID Login
# 4 = IPID Password
# 5 = MARKETG Server
# 6 = MARKETG Login
# 7 = MARKETG Password
set -x
if [ $# -lt 6 ]
then
	echo "Usage: ipid_marketguide.sh IPID_server IPID_database IPID_login IPID_password marketguide_server marketguide_login marketguide_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
ipserver=$5
iplogin=$6
ippassword=$7

ipname=marketguide

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the ${ipname} data into a temporary file
isql -S${ipserver} -U${iplogin} -P${ippassword} -s"|" -w500 -n -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON

select g.company_id matchkey, company_name name, cusip, g.ticker ,exchange_descr exchange, city, state_reg,country ,
cik , url from general_info g, securities s , web_links wl , company_flags cf
where 
issue_type='C' and 
issue_order=1
and s.company_id=g.company_id 
and co_status=1
and wl.category='Home Page' and 
wl.company_id =* g.company_id
and cf.company_id= g.company_id
HERE

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	matchkey varchar(5) NULL,
        name varchar(60) NULL,
	cusip varchar(20) NULL,
	ticker varchar(20) NULL,
	exchange varchar(60) NULL,
	city varchar(60) NULL,
	state_reg varchar(6) NULL,
	country varchar(20) NULL,
	cik numeric(23,0) NULL,
        url varchar(200) null
)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} -S ${IPIDserver} -U ${IPIDlogin} -P ${IPIDpassword} -f ${XLS}/src/scripts/matching/${FORMAT_FILE} -b 100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create index ${TABLENAME}_01 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(cusip)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(ticker)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(exchange)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(city)
GO
create index ${TABLENAME}_07 on ${TABLENAME}
	(state_reg)
GO

create index ${TABLENAME}_09 on ${TABLENAME}
	(country)
GO
HERE

# Step 9 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
