# Script file to build the IP matching table for Extel
# 1 = IPID Server
# 2 = IPID Database
# 3 = IPID Login
# 4 = IPID Password
# 5 = EXTEL Server
# 6 = EXTEL Login
# 7 = EXTEL Password
ARGS=7
if [ $# -ne $ARGS ]
then
	echo "Usage: ipid_extel.sh ipid_server ipid_database ipid_login ipid_password extel_server extel_login extel_password"
	exit 1
fi

IPIDserver=$1
IPIDdatabase=$2
IPIDlogin=$3
IPIDpassword=$4
extelserver=$5
extellogin=$6
extelpassword=$7

# Name of the temp file to use
TMPFILE1=ipid_extel1.tmp
TMPFILE2=ipid_extel2.tmp

# Name of the table to use
TABLENAME=ipid_extel

# Name of the format file to use
FORMAT_FILE=$XLS/src/scripts/matching/ipid_extel.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the extel data into a temporary file
isql -S${extelserver} -U${extellogin} -P${extelpassword} -s"|" -w1000 -n  -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select r.extelNo, r.name, 
address, country, telephone, website, issuer, valoren, cusip, sedol, isin, substring(USSICcodes,1,4), status, statusCode, d.id disclosure_id
from reference r, disclosure d
where statusCode != 9         
and d.extelNo=*r.extelNo
order by r.name
HERE

# Step 3 - post-process the temp file
sed -e "s/,/\^/g;s/|/,/g" < ${TMPFILE1} > temp1.tmp 
qcd2pipe -l -r < temp1.tmp > temp2.tmp
sed -e "s/\^/,/g;s/NULL//g" < temp2.tmp > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} << HERE
create table ${TABLENAME} (
	matchkey varchar(6) NOT NULL,
	name varchar(120) NOT NULL,
--	liveCard int NULL,
	address varchar(160) NULL,
	country varchar(32) NULL,
	telephone varchar(32) NULL,
	website varchar(128) NULL,
	issuer varchar(32) NULL,
	valoren varchar(20) NULL,
	cusip char(12) NULL,
	sedol char(8) NULL,
	isin varchar(32) NULL,
	USSICcode char(5) NULL,
	status varchar(32) NULL,
	statusCode int NULL,
	disclosure_id varchar(16) NULL
--	hasCard int NULL,
--	hasNews int NULL
)
create index ${TABLENAME}_02 on ${TABLENAME}
	(matchkey)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(country)
GO
create index ${TABLENAME}_05 on ${TABLENAME}
	(telephone)
GO
create index ${TABLENAME}_06 on ${TABLENAME}
	(issuer)
GO
create index ${TABLENAME}_07 on ${TABLENAME}
	(valoren)
GO
create index ${TABLENAME}_08 on ${TABLENAME}
	(cusip)
GO
create index ${TABLENAME}_09 on ${TABLENAME}
	(sedol)
GO
create index ${TABLENAME}_10 on ${TABLENAME}
	(isin)
GO
create index ${TABLENAME}_11 on ${TABLENAME}
	(USSICcode)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /t"|" /c /b1000
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 7 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2} temp1.tmp temp2.tmp
