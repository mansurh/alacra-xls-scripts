TITLEBAR="Fitch Mapping IDs"

if [ $# -lt 1 ]
then
    echo "usage: [fullrefresh=0|1]"
    exit 1
fi


echo "Running fitch_map_ids.sh"

fullrefresh=0
if [ $# -gt 0 ]
then
    fullrefresh=$1
fi


updatedate=`date +%Y%m%d%H%M%S`
datalog=$XLSDATA/matching/fitch_map_ids
logfile="$datalog/fitch_map_ids_${updatedate}.log"


mkdir -p ${datalog}

echo "" > $logfile


#-------------------------------------------------------
check_return_code()
{
ERROR_CODE=$1
MESSAGE=$2
EXIT_CODE=$3
if [ $ERROR_CODE != 0 ]
then
	echo "$MESSAGE"
	rm $UPD_PROGRESS
	exit $EXIT_CODE
fi
}
#-------------------------------------------------------


fitch_map_ids.exe ${fullrefresh} >> $logfile 2>&1
check_return_code $? "fitch_map_ids.exe failed to update IDs" 1
returncode=$?
