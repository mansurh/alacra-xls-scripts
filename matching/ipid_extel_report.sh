# Script file to generate the IP matching reports for extel
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_extel.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=extel

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in extel"
select * from ipid_extel
where matchkey not in (select sourcekey from company_map where source=18)


/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from extel"
select x.id, x.name from company x, company_map m
where
x.id = m.xlsid and m.source=18
and
m.sourcekey not in (select matchkey from ipid_extel)

/* Optional report - new information available in database */
PRINT "Companies with new web pages"
select x.id, x.name, x.parent, i.website from company x, company_map m, ipid_extel i
where
x.id = m.xlsid and m.sourcekey = i.matchkey
and m.source=18
and (x.url is null or x.url = " ")
and i.website != NULL

HERE
