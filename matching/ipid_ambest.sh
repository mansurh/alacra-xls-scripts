XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
# Script to process update for ambest Database - called by Start_Update.sh
#

# Script file to build the IP matching table for ambest 
# 1 = IPIDserver
# 2 = IPIDdatabase
# 3 = IPIDlogin
# 4 = IPIDpassword
# 5 = ambest Server
# 6 = ambest Login
# 7 = ambest Password

if [ $# -lt 4 ]
then
	echo "Usage: ipid_ambest.sh IPIDserver IPIDdatabase IPIDlogin IPIDpassword"
	exit 1
fi


IPIDserver=$1
IPIDlogin=$3
IPIDpassword=$4

IPIDname=ambest
AMBEST_FTP=`get_xls_registry_value ${IPIDname} Server 6`
AMBEST_LOGIN=`get_xls_registry_value ${IPIDname} User 6`
AMBEST_PW=`get_xls_registry_value ${IPIDname} Password 6`

# - remove any old temp files
rm -f AMBestForAlacra*

# Name of the temp file to use
LOADFILE=CompanyMatrix_f1133_`date +%m%d%Y`.csv

# Name of the table to use
TABLENAME=ipid_${IPIDname}
FTPFILE=${TABLENAME}.ftp
echo "user ${AMBEST_LOGIN} ${AMBEST_PW}" > ${FTPFILE}
echo "get ${LOADFILE} " >> ${FTPFILE}
echo "quit " >> ${FTPFILE}

ftp -i -n -s:${FTPFILE} ${AMBEST_FTP}
if [ $? -ne 0 ]; then echo "error in ftp, exiting ..."; exit 1; fi;


# create new ipid_ambest table
isql /U${IPIDlogin} /P${IPIDpassword} /S${IPIDserver} /Q "drop table ${TABLENAME}_new"
isql /b /U${IPIDlogin} /P${IPIDpassword} /S${IPIDserver} /Q "select top 0 * into ${TABLENAME}_new from ${TABLENAME}"
if [ $? -ne 0 ]; then echo "error creating ${TABLENAME}_new, exiting ..."; exit 1; fi;

# - bcp in the select results
bcp ${TABLENAME}_new in ${LOADFILE}  /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /c /t"|" /CRAW /e ${LOADFILE}.err
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

echo "done with bcp to concordance"

typeset -i filecount=`wc -l ${LOADFILE} |cut -f1 -d" "`
$XLS/src/scripts/matching/check_ipid_row_counts.sh ${IPIDserver} ${IPIDlogin} ${IPIDpassword} ${TABLENAME} ${filecount}
if [ $? -ne 0 ]
then
	echo "Counts check failed, exiting ..."
	exit 1
fi

$XLS/src/scripts/utility/create_index_sql.sh ${IPIDserver} ${IPIDlogin} ${IPIDpassword} ${TABLENAME} ${TABLENAME}_index.sql
if [ $? -ne 0 ]
then
	echo "error in $XLS/src/scripts/utility/create_index_sql.sh, exiting ..."
	exit 1
fi

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b < ${TABLENAME}_index.sql
if [ $? -ne 0 ]
then
	echo "error creating indexes on ${TABLENAME}_new table, exiting ..."
	exit 1
fi

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b /Q "drop table ${TABLENAME}_bak"

isql /S${IPIDserver} /U${IPIDlogin} /P${IPIDpassword} /b /Q "exec sp_rename '${TABLENAME}', '${TABLENAME}_bak'; exec sp_rename '${TABLENAME}_new', '${TABLENAME}'"
if [ $? -ne 0 ]
then
	echo "error renaming ${ipname}_new table, exiting ..."
	exit 1
fi

