update ipid_sdcvc set state = '' where state in ('Not applicable', 'Unknown', 'Non-US')
go
update ipid_sdcvc set country='Antigua & Barbuda' where country='Antigua'
go
update ipid_sdcvc set country='Bosnia & Herzegovina' where country='Bosnia/Herz.'
go
update ipid_sdcvc set country='Cameroun' where country='Cameroon'
go
update ipid_sdcvc set country='Central African Republic' where country='C. African Rep'
go
update ipid_sdcvc set country='Cote d''Ivoire' where country='Ivory Coast'
go
update ipid_sdcvc set country='Dominican Republic' where country='Dominican Rep.'
go
update ipid_sdcvc set country='Equatorial Guinea' where country='Equator Guinea'
go
update ipid_sdcvc set country='French Polynesia' where country='Fr Polynesia'
go
update ipid_sdcvc set country='Guinea Republic' where country='Guinea'
go
update ipid_sdcvc set country='Guinea-Bisseau' where country='Guinae-Bussau'
go
update ipid_sdcvc set country='Marshall Islands' where country='Marshall Is.'
go
update ipid_sdcvc set country='Myanmar' where country='Burma'
go
update ipid_sdcvc set country='Netherlands Antilles' where country='Neth. Antilles'
go
update ipid_sdcvc set country='Papua New Guinea' where country='Papua N Guinea'
go
update ipid_sdcvc set country='Republic of Ireland' where country='Ireland'
go
update ipid_sdcvc set country='Samoa' where country='Somoa'
go
update ipid_sdcvc set country='Sao Tome & Principe' where country='Sao Tome'
go
update ipid_sdcvc set country='Solomon Islands' where country='Solomon Isl.'
go
update ipid_sdcvc set country='St Kitts & Nevis' where country='St Kitts/Nevis'
go
update ipid_sdcvc set country='St Vincent & Grenadines' where country='Grenadines'
go
update ipid_sdcvc set country='Suriname' where country='Surinam'
go
update ipid_sdcvc set country='Trinidad & Tobago' where country='Trinidad/Tob.'
go
update ipid_sdcvc set country='United Arab Emirates' where country='Utd. Arab Em.'
go
update ipid_sdcvc set country='Virgin Islands' where country='Am. Virgin Is.'
go
