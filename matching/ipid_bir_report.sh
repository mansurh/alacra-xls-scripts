# Script file to generate the IP matching reports for extel
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_extel.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

blatfile=$1
xls_server=$2
xls_logon=$3
xls_password=$4

LOADDIR=${XLS}/src/scripts/loading/bir
TEMPDIR=${XLSDATA}/bir/matching

# Now build an email to send to company matching
blatfiletmp1=${blatfile}.tmp1
blatfiletmp2=${blatfile}.tmp2

rm -f ${blatfile}
rm -f ${blatfiletmp1}
rm -f ${blatfiletmp2}


echo ""
echo "Getting list of companies which were just added to ipid_bir"
echo ""

echo "*** ipid_bir update info: *** " > ${blatfile}
echo "" >> ${blatfile}
echo "*** Companies added: ***" >> ${blatfile}
echo "" >> ${blatfile}
#isql -U ${xls_logon} -P ${xls_password} -S ${xls_server} -r -w 256 -x 1024000 -Q "set nocount on select distinct matchkey + '|' + name + '|' + bir_id + '|' + ticker + '|' + convert(varchar(20), naics) + '|' + exchange  + '|??' from ipid_bir where matchkey not in (select distinct sourcekey from company_map where source=9969)" > ${blatfiletmp1}
#tail +5 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_bir.sed > ${blatfiletmp2}
#
#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#
#    fi
#
#done < ${blatfiletmp2} >> ${blatfile}

bcp "select distinct matchkey , name , bir_id , ticker , convert(varchar(20), naics) , exchange from ipid_bir where matchkey not in (select distinct sourcekey from company_map where source=9969)" queryout ${blatfiletmp1} -U ${xls_logon} -P ${xls_password} -S ${xls_server} -c -t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out ADDED companies, exiting..."
	exit 1
fi
cat ${blatfiletmp1} >> ${blatfile}

echo ""
echo "Get list of companies which were just deleted from ipid_bir"
echo ""

echo "" >> ${blatfile}
echo "*** Companies deleted: ***" >> ${blatfile}
echo "" >> ${blatfile}

#isql -U ${xls_logon} -P ${xls_password} -S ${xls_server} -s"|" -r -x 1024000 -w 256 -Q "set nocount on select distinct m.sourcekey + '|' + convert(varchar(255), m.xlsid) + '|' + c.name + '|' + cou.name + '|' + s.cusipcins + '|' + convert(varchar(20), s.id) + '|' + ltrim(rtrim(s.ticker)) + '|' + e.name from company_map m, company c, country cou, security s, exchange e where source=9969 and c.id=m.xlsid and cou.id=c.country and s.issuer = c.id and s.exchange=e.id and ''!=isnull(m.sourcekey, '') and s.type=3 and sourcekey not in (select distinct matchkey from ipid_bir)" > ${blatfiletmp1}
#tail +5 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_bir.sed > ${blatfiletmp2}

#while read -r line
#do
#
#    line=${line%% }
#
#    if [ "${line}" != '' ]
#    then
#
#        print "${line}"
#
#    fi
#
#done < ${blatfiletmp2} >> ${blatfile}

bcp "select distinct m.sourcekey, convert(varchar(255), m.xlsid), c.name, cou.name, s.cusipcins, convert(varchar(20), s.id), ltrim(rtrim(s.ticker)), e.name from company_map m, company c, country cou, security s, exchange e where source=9969 and c.id=m.xlsid and cou.id=c.country and s.issuer = c.id and s.exchange=e.id and ''!=isnull(m.sourcekey, '') and s.type=3 and sourcekey not in (select distinct matchkey from ipid_bir)" queryout ${blatfiletmp2} -U ${xls_logon} -P ${xls_password} -S ${xls_server} -c -t"|"
if [ $? -ne 0 ]
then
	echo "error in bcp out DELETED companies, exiting..."
	exit 1
fi
cat ${blatfiletmp2} >> ${blatfile}

