# Script file to generate the IP matching reports for snl
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_snl_report.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

LOADDIR=${XLS}/src/scripts/loading/snl
TEMPDIR=${XLSDATA}/matching

# Now build an email to send to company matching
blatfile="${TEMPDIR}/ipid_snl.txt"
blatfiletmp1=${blatfile}.tmp1
blatfiletmp2=${blatfile}.tmp2

finalblatfile=${REPORTFILE}

echo "*** ipid_snl update info: *** " > ${finalblatfile}
echo "" >> ${finalblatfile}
echo "*** Companies added: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}
isql -U ${xlslogin} -P ${xlspassword} -S ${xlsserver} -r -Q "set nocount on select distinct matchkey + '|' + name + '|??' from ipid_snl where matchkey not in (select distinct sourcekey from company_map where source=9964)" > ${blatfiletmp1}
tail +9 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_snl.sed > ${blatfiletmp2}

while read -r line
do

    line=${line%% }

    if [ "${line}" != '' ]
    then

        print "${line}"

    fi

done < ${blatfiletmp2} >> ${finalblatfile}

echo "" >> ${finalblatfile}
echo "*** Companies deleted: ***" >> ${finalblatfile}
echo "" >> ${finalblatfile}
isql -U ${xlslogin} -P ${xlspassword} -S ${xlsserver} -s"|" -r -x 255 -w 255 -Q "set nocount on select distinct m.sourcekey + '|' + convert(varchar(255), m.xlsid) + '|' + c.name + '|' + cou.name + '|' + s.cusipcins + '|' + ltrim(rtrim(s.ticker)) + '|' + e.name from company_map m, company c, country cou, security s, exchange e where source=9958 and c.id=m.xlsid and cou.id=c.country and s.cusipcins=m.sourcekey and s.exchange=e.id and sourcekey not in (select distinct matchkey from ipid_snl) and 0!=isnull(sourcekey, 0)" > ${blatfiletmp1}
tail +5 < ${blatfiletmp1} | sed -f ${LOADDIR}/ipid_snl.sed > ${blatfiletmp2}

while read -r line
do

    line=${line%% }

    if [ "${line}" != '' ]
    then

        print "${line}"

    fi

done < ${blatfiletmp2} >> ${finalblatfile}

exit 0
