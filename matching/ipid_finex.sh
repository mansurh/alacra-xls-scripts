# Script file to build the IP matching table for finex
# 1 = XLS Server
# 2 = XLS Login
# 3 = XLS Password
# 4 = finex Server
# 5 = finex Login
# 6 = finex Password
if [ $# -lt 6 ]
then
	echo "Usage: ipid_finex.sh xls_server xls_login xls_password finex_server finex_login finex_password"
	exit 1
fi

xlsserver=$1
xlslogin=$2
xlspassword=$3
finexserver=$4
finexlogin=$5
finexpassword=$6

ipname=finex

# Name of the temp file to use
TMPFILE1=ipid_${ipname}1.tmp
TMPFILE2=ipid_${ipname}2.tmp

# Name of the table to use
TABLENAME=ipid_${ipname}

# Name of the format file to use
FORMAT_FILE=ipid_${ipname}.fmt

# Step 1 - remove any old temp files
rm -f ${TMPFILE1} ${TMPFILE2}

# Step 2 - select the finex data into a temporary file
isql -S${finexserver} -U${finexlogin} -P${finexpassword} -s"|" -n -w1000 -h-1 >${TMPFILE1} << HERE
SET NOCOUNT ON
select 	
	f.id, 
	f.name, 
	f.parent_id,
	f.alphasort,
	f.former_name_1,
	f.former_name_2,
	f.ticker,
	f.web_address,
	a.telephone,
	a.fax,
	a.country_code,
	a.state_prov,
	a.zip,
	a.address_1,
	a.address_2,
	a.address_3,
	a.city
	from firm f, address a
	where f.id=a.firm_id and a.address_code = 1
HERE

# Step 3 - post-process the temp file
sed -f ${XLS}/src/scripts/matching/match.sed < ${TMPFILE1} > ${TMPFILE2}

# Step 4 - drop the old id table - don't check for error
# as it may not exist
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
drop table ${TABLENAME}
go
HERE

# Step 5 - create the new table
isql /S${xlsserver} /U${xlslogin} /P${xlspassword} << HERE
create table ${TABLENAME} (
	id int NULL,
	name varchar(90) NULL,
	parent int NULL,
	alphasort varchar(90) NULL,
	former_name_1 varchar(64) NULL,
	former_name_2 varchar(64) NULL,

	ticker varchar(6) NULL,
	web_address varchar(64) NULL,
	telephone varchar(16) NULL,
	fax varchar(16) NULL,
	country_code int NULL,
	state_prov varchar(3) NULL,
	zip varchar(10) NULL,

	address_1 varchar(36) NULL,
	address_2 varchar(36) NULL,
	address_3 varchar(36) NULL,
	city varchar(24) NULL
)
GO
create index ${TABLENAME}_01 on ${TABLENAME}
	(id)
GO
create index ${TABLENAME}_02 on ${TABLENAME}
	(name)
GO
create index ${TABLENAME}_03 on ${TABLENAME}
	(parent)
GO
create index ${TABLENAME}_04 on ${TABLENAME}
	(country_code)
GO
HERE
if [ $? -ne 0 ]
then
	echo "Error creating table, exiting"
	exit 1
fi

# Step 6 - bcp in the select results
bcp ${TABLENAME} in ${TMPFILE2} /S${xlsserver} /U${xlslogin} /P${xlspassword} /f${XLS}/src/scripts/matching/${FORMAT_FILE} /b100
if [ $? -ne 0 ]
then
	echo "Error in BCP, exiting"
	exit 1
fi

# Step 8 - clean up
#rm -f ${TMPFILE1} ${TMPFILE2}
