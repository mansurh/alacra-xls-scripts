# Script file to generate the IP matching reports for extel
# 1 = Output filename
# 2 = XLS Server
# 3 = XLS Login
# 4 = XLS Password
if [ $# -lt 4 ]
then
	echo "Usage: ipid_ted.sh reportfile xls_server xls_login xls_password"
	exit 1
fi

REPORTFILE=$1
xlsserver=$2
xlslogin=$3
xlspassword=$4

ipname=ted

# Name of the table to use
TABLENAME=ipid_${ipname}

isql -S${xlsserver} -U${xlslogin} -P${xlspassword} -s"|" -n -w1000 -h0 >${REPORTFILE} << HERE
SET NOCOUNT ON

/* Required Report - New companies to appear in database */
PRINT "Potentially new companies in ted"
select * from ipid_ted
where companyid not in (select sourcekey from company_map where source=16)


/* Required Report - Companies which have disappeared from database */
PRINT "Matched companies that have disappeared from ted"
select x.id, x.name, "Country"=c.name
from company x, company_map m, country c
where
x.id = m.xlsid and m.source=16
and
m.sourcekey not in (select companyid from ipid_ted)
and x.country=c.id
order by c.name

/* Optional report - new information available in database */

HERE
