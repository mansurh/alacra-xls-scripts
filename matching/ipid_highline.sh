# Script file to generate the IP table for highline

# Save the runstring parameters in local variables
XLSUTILS=$XLS/src/scripts/loading/dba/
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn


server=`get_xls_registry_value highline server`
user=`get_xls_registry_value highline user`
password=`get_xls_registry_value highline password`

SCRIPTDIR=${XLS}/src/scripts/loading/highline

echo "Backup current ipid_highline table"
isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} -r -Q "drop table ipid_highline_backup"
isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} -r -Q "select * into ipid_highline_backup from ipid_highline"

echo "Create ipid_highline table and load it with new Highline data"
isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} < ${SCRIPTDIR}/create_ipid_highline.sql
isql -w 255 -x 255 -U ${user} -P ${password} -S ${server} < ${SCRIPTDIR}/populate_ipid_highline.sql

exit 0
