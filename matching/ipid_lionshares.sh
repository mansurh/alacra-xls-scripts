# Script file to build the IP matching table for lionshares
# 1 = IPID Server
# 2 = IPID Database
# 2 = IPID Login
# 3 = IPID Password
# 4 = datamonitor Server
# 5 = lionshares Login
# 6 = lionshares Password
ARGS=7 #Number of arguments expected
if [ $# -ne $ARGS ]
then
    echo "Usage: ipid_lionshares.sh ipid_server ipid_database ipid_login ipid_password ln_server ln_login ln_password"
    exit 1
fi

destdataserver=$1
destdb=$2
destlogon=$3
destpassword=$4
srcdataserver=$5
srclogon=$6
srcpassword=$7


LOADDIR=${XLS}/src/scripts/loading/lionshares
TEMPDIR=${XLSDATA}/matching/lionshares

XLSUTILS=$XLS/src/scripts/loading/dba/

. $XLSUTILS/check_return_code.fn
. $XLSUTILS/check_bcp_errors.fn

mkdir -p ${TEMPDIR}


#
# Put new data in a batch file.
#

echo ""
echo "Building bulk load file from companies table from ${srcdataserver}"
echo ""

#isql -w 255 -x 255 -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} -r -Q "set nocount on select distinct CUSIP + '|' + Name + '|' + Country_Name + '|' + ticker + '|' + Stock_Exchange + '|' + CUSIP + '|' + SEDOL + '|' + Issue_Type_Name + '|' + convert(varchar(64), Price_Date) from lionshares.dbo.secmas where Issue_Type_Name in ('ADR/GDR', 'Closed-End Mutual Fund', 'Equity', 'Exchange Traded Fund', 'Hedge Fund', 'N/A', 'Open-End Mutual Fund', 'Unit Invt Trust')" > ${TEMPDIR}/ipid_ln.dat.temp1
bcp "select distinct CUSIP, Name, Country_Name, ticker, Stock_Exchange, CUSIP, SEDOL, Issue_Type_Name, convert(varchar(64), Price_Date) from lionshares.dbo.secmas where Issue_Type_Name in ('ADR/GDR', 'Closed-End Mutual Fund', 'Equity', 'Exchange Traded Fund', 'Hedge Fund', 'N/A', 'Open-End Mutual Fund', 'Unit Invt Trust')" queryout ${TEMPDIR}/ipid_ln.dat.temp1 -U ${srclogon} -P ${srcpassword} -S ${srcdataserver} -c -t"|"
check_return_code $? "Failed to select out lionshares data to build ipid table. Aborting" 1

cat ${TEMPDIR}/ipid_ln.dat.temp1 | sed -f ${LOADDIR}/ipid_lionshares.sed > ${TEMPDIR}/ipid_ln.dat.temp2

#while read -r line
#do

#    line=${line%% }

#    if [ "${line}" != '' ]
#    then

#        echo "${line}"

#    fi

#done < ${TEMPDIR}/ipid_ln.dat.temp2 > ${TEMPDIR}/ipid_ln.dat
cp ${TEMPDIR}/ipid_ln.dat.temp2  ${TEMPDIR}/ipid_ln.dat
#
# Drop old data from xls table ipid_lionshares
#

echo ""
echo "Dropping old ipid_lionshares data on ${destdataserver}"
echo ""

isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/create_ipid_lionshares.sql


# drop the indices for the table
echo ""
echo "Dropping indices for ipid_lionshares for faster loading"
echo ""

isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/dropindex_ipid_lionshares.sql


#
# Load all the new data...
#

DATFILE="${TEMPDIR}/ipid_ln.dat"

# Load the ipid_lionshares table
echo "Loading the ipid_lionshares table" 


bcp ${destdb}.dbo.ipid_lionshares in ${DATFILE} /U${destlogon} /P${destpassword} /S${destdataserver} /f${LOADDIR}/ipid_lionshares.fmt /e${TEMPDIR}/ipid_lionshares.log /o${TEMPDIR}/ipid_lionshares.errout
check_return_code $? "Failed to bcp in ipid_lionshares table. Aborting" 1
check_bcp_errors ${TEMPDIR}/ipid_lionshares.errout 100 "Failed to bcp in ipid_lionshares table (too many errors). Aborting" 2

# Set exclude flags forconcordance record keeping
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% TRUST%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% TR %' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% WT %' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% WARRANTS%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% INVESTMENT TR%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% INVT TR%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% INV TRT%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% FUND %' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% VVPR%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% INCOME FD%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% NUVEEN%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '%TIERS %' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% SCHRODER %' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% SPECIAL UTILITIES INV%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% REIT%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% SH BEN%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% TR UNIT%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% TIERS%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% DEP RCPTS%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% LTD PART%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% LP %' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% CTF%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% CAP YLD SHS%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% CHESS %' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% PFD%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% INT %' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% DIVID SHS%' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where name like  '% SDCV %' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where type =  'Unit Invt Trust' "
isql -w 255 -x 255 -U ${destlogon} -P ${destpassword} -S ${destdataserver} -r -Q " update ipid_lionshares set exclude=1 where type =  'Unit' "


# Rebuild the indices for the table
echo ""
echo "Rebuilding indices for ipid_lionshares for faster loading"
echo ""

isql /U${destlogon} /P${destpassword} /S${destdataserver} < ${LOADDIR}/createindex_ipid_lionshares.sql

echo ""
echo "Updated table ipid_lionshares for lionshares OK."
echo ""

exit 0
