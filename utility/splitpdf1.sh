TITLEBAR="split pdf file into pages"
. $XLS/src/scripts/loading/dba/wait_with_timeout.fn

# Parse the command line arguments.
#	1 = pdf file path
#	2 = output path
#	3 = pages to ignore [optional, if known]
#		e.g. non-billable boilerplate pages in investment research reports
shname=$0
if [ $# -lt 2 ]
then
    echo "Usage: ${shname} pdffile outputpath [ignore_pages]"
    exit 1
fi

pdffile=$1
outpath=$2
ignore_pages=$3

if [ ! -d ${outpath} ]
then
    mkdir -p ${outpath}
fi

if [ ! -s ${pdffile} ]
then
    echo "${shname}: ${pdffile} not found."
    exit 1
fi

filename="${pdffile##*\\}"
filename="${filename##*/}"
prefix="${filename%.[pP][dD][fF]}"

# check if text file already exists
if [ -s "${outpath}/$prefix.txt" ]
then
	# check if the pdf file is newer.  if not, we're done
	if [ "${outpath}/$prefix.txt" -nt "$pdffile" ]
	then
#		echo "${shname}: $prefix.txt already exists."
		exit 0
	fi
fi

mkdir -p $XLSDATA/splitting
cp  "${pdffile}" $XLSDATA/splitting/
cd $XLSDATA/splitting

#####################################
### retrieve page number from pdf ###
#####################################
#size="$(pdls $filename | grep -i ^page | wc -l | sed 's/[^[:digit:]]//g')"
#echo "${shname}: split $filename into $size pages"

#if [ $size -lt 1 ]
#then
#    echo "${shname}: no page found in ${filename}"
#    rm -f ./${filename}
#    exit 1
#fi

####################################
### split pdf into indivual page ###
####################################

# first output the entire text file
#pdw $filename > $prefix.txt.tmp
#cmd="pdw $filename"
#timeout=10
#wait_with_timeout "$cmd" $timeout $prefix.txt.tmp
#pdw "$filename" > $prefix.txt.tmp
#if [ $? != 0 ]
#then
#    echo "${shname}: Error converting pdf to text, exiting."
#    rm -f ./$prefix.txt.tmp
#    rm -f ./${filename}
#    exit 1
#fi

#pageindex="index.$prefix.txt.tmp"
# build the page index file
#if [ -e "$pageindex" ]
#then
#	rm -f -- "$pageindex"
#fi

# create the description paragraph - before we delete the tmp file
# no need to exit if this does not work
#PDFAnalyze "$prefix.txt.tmp" "$prefix.desc.txt" "$ignore_pages"
#if [ $? != 0 ]
#then
#    echo "${shname}: Error converting creating the description paragraph"
#fi

#if [ -e "$prefix.txt.tmp" ]
#then
#	awk -f $XLS/src/scripts/utility/pageIndex.awk -- "$prefix.txt.tmp" > $prefix.txt.tmp2
#	rm -f -- "$prefix.txt.tmp"
#fi

#if [ -e "$prefix.txt.tmp2" ]
#then
#	cat -- "$pageindex" "$prefix.txt.tmp2" > $prefix.txt
#	rm -f -- "$prefix.txt.tmp2"
#fi

#if [ -e "$pageindex" ]
#then
#	rm -f -- "$pageindex"
#fi

#echo "${shname}: splitting $filename into $size pages done"

AlacraPdfToText "$filename" "$prefix.txt" "$prefix.desc.txt" "$ignore_pages"

ret=$?

if [ ${ret} -eq 1 ]
then
	echo "${shname}: Error creating the description paragraph."
	mv ./$prefix.txt $outpath/
	rm -f ./$prefix.desc.txt
	rm -f ./${filename}
	exit 0
elif [ ${ret} -eq 2 ]
then
	echo "${shname}: No text extracted."
	rm -f ./$prefix.txt
	rm -f ./$prefix.desc.txt
	rm -f ./${filename}
	exit 1
elif [ ${ret} -eq 0 ]
then
	mv ./$prefix.txt $outpath/
	mv ./$prefix.desc.txt  $outpath/
	rm -f ./${filename}
	exit 0
else
	echo "${shname}: Error converting pdf to text, exiting."
	rm -f ./$prefix.txt
	rm -f ./$prefix.desc.txt
	rm -f ./${filename}
	exit 1
fi

#mv ./$prefix.txt $outpath/
#mv ./$prefix.desc.txt  $outpath/
#rm -f ./${filename}
#exit 0
