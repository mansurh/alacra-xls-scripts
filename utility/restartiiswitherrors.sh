main() {
  searchstr=`date "+%b %e %H"`.*exception
#  searchstr=`date "+%b %e"`.*exception
  echo "searching for $searchstr"
  egrep "$searchstr" w3wp.err > tmpfile
  if [ $? -eq 0 ]
  then
    blat tmpfile -t operations@alacra.com -s "Errors in w3wp.err on $COMPUTERNAME"
    echo "found exceptions, restarting iis"
    $XLS/src/scripts/install/stopservice.sh availability 1
    if [ $? != 0 ]; then	return 1; fi

    iisreset
    if [ $? -ne 0 ]; then return 1; fi
    $XLS/src/scripts/install/startservice.sh availability 1
    if [ $? -ne 0 ]; then return 1; fi
  fi
  rm -f tmpfile
  return 0
}

dir=`date +%Y/%m/%d`
cd c:/logfiles/$dir
if [ $? -ne 0 ]; then exit 1; fi
logfile=checkw3wp`date +%H`.log
main > $logfile 2>&1
if [ $? -ne 0 ]
then
  blat $logfile -t administrators@alacra.com -s "Error restarting iis on $COMPUTERNAME"
  exit 1
fi
