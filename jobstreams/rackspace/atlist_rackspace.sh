

# 2018-12-20
# Merge from VSS into BB
# Branch: RefData/Merge_atlist_from_VSS

scriptname=$0
echo "DO NOT RUN WITHOUT PERMISSION FROM Information Systems personnel!!"
grep -q "^at " ${scriptname}
if [ $? -eq 0 ]
then
  echo "ERROR: Old command (at.exe) is used, no longer supported"
  echo "ERROR: No action taken, review the changes, and switch to SCHTASKS command"
  exit 1
fi

# Define the scheduling server
schedserver=825815-UACHP01P
if [ $COMPUTERNAME != $schedserver ]
then
    echo "Not $schedserver, exiting..."
    exit 1
fi

run()
{

#catch if any of these fail
set -ex
#SCHTASKS /F /Delete /TN "*"

# Create the Source Safe archive for backup
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 19:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ssarchive" /TN "Alacra ssarchive" /RU ""

# Set up the ipid weekly load
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 22:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_weekly" /TN "Alacra ipid_weekly" /RU ""

# Load panobis data
# SCHTASKS /Create /F /SC Weekly /D THU /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/panobis" /TN "Alacra panobis" /RU ""

# FinCEN 
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI,SAT /ST 02:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/FinCEN" /TN "Alacra FinCEN" /RU ""

#RDC Full refresh
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/rdcfull" /TN "Alacra rdcfull" /RU ""

#RDC Incremental - This should run every day except first day, but no way to tell the scheduler, for now, ignore errors on first day
SCHTASKS /Create /F /SC DAILY /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/rdcincremental" /TN "Alacra rdcincremental" /RU ""

#Dow Jones Watchlist Full refresh
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 08:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/djwatchlistFull" /TN "Alacra djwatchlistFull" /RU ""

#Dow Jones Watchlist Incremental update for previous day
SCHTASKS /Create /F /SC DAILY /ST 00:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/djwatchlist" /TN "Alacra djwatchlist" /RU ""

# Dow Jones monitored relationship count
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 00:25 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/dow_jones_relationship_count" /TN "Alacra Dow Jones monitored relationship count" /RU ""

# Archive ACE monitoring results
SCHTASKS /Create /F /SC Weekly /D SAT /ST 18:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ace_archive_monitor_results"  /TN "Alacra Archive ACE monitoring results" /RU ""

# wcheck load
SCHTASKS /Create /F /SC DAILY /ST 01:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wcheck0" /TN "Alacra wcheck update" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 15:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wcheck1" /TN "Alacra wcheck second update" /RU ""

# wcheck monthly refresh
SCHTASKS /Create /F /SC MONTHLY /MO FIRST /d SUN /st 05:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wcheck_full" /TN "Alacra wcheck_full" /RU ""

# ipid_bvdorbis monthly load
SCHTASKS /Create /F /SC MONTHLY /D 7 /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bvdorbis " /TN "Alacra ipid_bvdorbis monthly" /RU ""
#DNB Load
SCHTASKS /Create /F /SC MONTHLY /MO SECOND /D THU /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/dnb2load" /TN "Alacra dnb2load" /RU ""

# Set up the almost daily incremental IBES load
SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI /ST 04:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ibesdaily" /TN "Alacra ibes daily" /RU ""

# Set up the weekly IBES load
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 13:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ibesweekly" /TN "Alacra ibes weekly" /RU ""

# "WF Factiva Select every 60 min
SCHTASKS /Create /F /SC HOURLY /ST 00:15 /MO 1 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/factivaselect_load" /TN "Alacra factivaselect" /RU ""

# "WF Factiva Select every 60 min
SCHTASKS /Create /F /SC HOURLY /ST 00:45 /MO 1 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/factivaselect1_load" /TN "Alacra factivaselect1" /RU ""

# Run Factiva Alert every 30 min on each prod server
SCHTASKS /Create /F /SC MINUTE /MO 30 /st 00:16 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/alertfactiva3833prod2" /TN "Alacra alertfactiva3833prod2" /RU ""

# Set up the EIU Country Data & Market Indicators weekly load
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/eiu_files" /TN "Alacra eiu_files" /RU ""

# Set up the EIUFDI weekly load
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/eiufdi" /TN "Alacra eiufdi" /RU ""

# eiuft xml feed
SCHTASKS /Create /F /SC DAILY /ST 19:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/eiuftxml" /TN "Alacra eiuftxml" /RU ""

# Atradius
SCHTASKS /Create /F /SC MONTHLY /d 16 /st 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_atradius" /TN "Alacra atradius" /RU ""

# AFG load
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 19:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/afg_load" /TN "Alacra afg_load" /RU ""

# FDSE ICB Weekly load
SCHTASKS /Create /F /SC Weekly /D SAT /ST 07:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/icb_load" /TN "Alacra icb_load" /RU ""

#Firstcall
SCHTASKS /Create /F /SC DAILY /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/firstcallearnings" /TN "Alacra firstcallearnings" /RU ""

# audita
#SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/audita" /TN "Alacra audita" /RU ""

# zephyr - old and new
SCHTASKS /Create /F /SC MONTHLY /D 10 /ST 16:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bvdzephyrnew"  /TN "Alacra bvdzephyrnew" /RU ""
SCHTASKS /Create /F /SC MONTHLY /D 8 /ST 20:20 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bvdfame" /TN "Alacra bvdfame" /RU ""

# compliance sites status check
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 08:55 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/sitessearch" /TN "Alacra sitessearch#1" /RU ""

# update our metadata for the datamonitor api - Disabled on 2017/09/26 by Dave Burke
# SCHTASKS /Create /F /SC WEEKLY /D MON /ST 19:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/dmapi" /TN "Alacra dmapi" /RU ""

# Edgar load
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/edgar" /TN "Alacra edgar" /RU ""

# Set up the MarkIntel New Feed Daily Load
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 12:40 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/markintel_new" /TN "Alacra markintel_new" /RU ""

# Set up the MarkIntel New Feed Weekly Load with full reindex
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 09:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/markintelfull_iad_new" /TN "Alacra markintelfull_iad_new" /RU ""
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 09:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/markintelfull_dfw_new" /TN "Alacra markintelfull_dfw_new" /RU ""

# Mergermarket Daily Updates, every 2 hours, from 1:15 to 5:15PM, and mmi_incr_comp at 07:15PM, & mmideals at 08:15PM
SCHTASKS /Create /F /SC DAILY /ST 20:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mmideals" /TN "Alacra mmideals" /RU ""

# ofac 
SCHTASKS /Create /F /SC DAILY /ST 10:20 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ofac" /TN "Alacra ofac" /RU ""

#ftimes full reindex-SAT
#SCHTASKS /Create /F /SC Weekly /D SAT /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ftimes_index" /TN "Alacra ftimes_index" /RU ""

#Ftimes news daily -- Need to figure out how to run it
#SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 00:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ftimes_news" /TN "Alacra ftimes_news" /RU ""

# Edgar Alerts
SCHTASKS /Create /F /SC DAILY /ST 22:52 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/alertDailyEdgar" /TN "Alacra alertDailyEdgar" /RU ""

# Set up the rkd daily alerts, hourly
SCHTASKS /Create /F /SC HOURLY /MO 1 /st 00:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/rkdFCResearchAlerts" /TN "Alacra rkdFCResearchAlerts" /RU ""

# Set up the rkd daily contributors
SCHTASKS /Create /F /SC DAILY /ST 00:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/rkdFCResearchDriver" /TN "Alacra rkdFCResearchDriver" /RU ""

# LexisNexis News Feed RSS
SCHTASKS /Create /F /SC MINUTE /MO 30 /st 00:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/lexisalacrafeed" /TN "Alacra lexisalacrafeed" /RU ""

# Onesource RSS
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 12:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/onesource_rss" /TN "Alacra onesource_rss" /RU ""

# Hoovers Update
SCHTASKS /Create /F /SC WEEKLY /D WED /ST 21:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/hoovers" /TN "Alacra hoovers" /RU ""

# Set up the IBES Aggregate load
SCHTASKS /Create /F /SC MONTHLY /D 25 /ST 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ibesagg" /TN "Alacra ibes aggregate" /RU ""

# run the new itext loading program every morning
SCHTASKS /Create /F /SC WEEKLY /D SUN,MON,TUE,WED,THU,FRI,SAT /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/itext_new_load_data" /TN "Alacra itext_new_load_data" /RU ""
# run the new itext incremental index every morning except Saturday and Sunday
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 11:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/itext_new_idx_incr" /TN "Alacra itext_new_idx_incr" /RU ""

# rebuild the new itext full text index each week on Friday
SCHTASKS /Create /F /SC MONTHLY /MO FIRST /D FRI /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/itext_new_idx_full_odd" /TN "Alacra itext_new_idx_full_odd" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO SECOND /D FRI /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/itext_new_idx_full_even" /TN "Alacra itext_new_idx_full_even" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO THIRD /D FRI /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/itext_new_idx_full_odd" /TN "Alacra itext_new_idx_full_odd#2" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO FOURTH /D FRI /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/itext_new_idx_full_even" /TN "Alacra itext_new_idx_full_even#2" /RU ""

# CCBN, every hour on weekday, every 4 hours on weekend
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 00:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn#1" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 04:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn#2" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 08:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn#3" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 10:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn#4" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 12:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn#5" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 14:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn#6" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 16:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn#7" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 18:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn#8" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 20:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn#9" /RU ""
SCHTASKS /Create /F /SC Weekly /D SAT,SUN /ST 22:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn" /TN "Alacra ccbn weekend" /RU ""
SCHTASKS /Create /F /SC Weekly /D SUN /ST 19:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ccbn_cleanup" /TN "Alacra ccbn_cleanup" /RU ""

# callstreet, every hour on weekday, every 4 hours on weekend
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 00:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet#1" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 04:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet#2" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 08:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet#3" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 10:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet#4" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 12:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet#5" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 14:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet#6" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 16:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet#7" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 18:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet#8" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 20:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet#9" /RU ""
SCHTASKS /Create /F /SC Weekly /D SAT,SUN /ST 19:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/callstreet" /TN "Alacra callstreet weekend" /RU ""


# Valueline tearsheets
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 23:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/valueline" /TN "Alacra valueline" /RU ""

#icc images index rss
SCHTASKS /Create /F /SC DAILY /ST 07:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/icc_images_rss" /TN "Alacra icc_images_rss" /RU ""

# capiq
SCHTASKS /Create /F /SC DAILY /ST 11:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/capiq" /TN "Alacra capiq" /RU ""
SCHTASKS /Create /F /SC Weekly /D SAT /ST 07:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/capiqfull" /TN "Alacra capiqfull" /RU ""

#experian load
SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SAT /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/experian" /TN "Alacra experian" /RU ""

#experian index
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/experian_index" /TN "Alacra experian_index" /RU ""

#Experian US monthly load
SCHTASKS /Create /F /SC MONTHLY /D 28 /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/experianus" /TN "Alacra experianus" /RU ""

# MGE
SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SAT /ST 01:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mge" /TN "Alacra mge" /RU ""

# Mergermarket Daily Updates, every 2 hours, from 1:15 to 5:15PM, and mmi_incr_comp at 07:15PM, & mmideals at 08:15PM
#SCHTASKS /Create /F /SC HOURLY /ST 01:15 /ET 18:16 /MO 1 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mmi_incr" /TN "Alacra mmi_incr" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 19:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mmi_incr_comp" /TN "Alacra mmi_incr_comp" /RU ""

#DNB Load
SCHTASKS /Create /F /SC Monthly /M JAN,APR,JUL,OCT /D 10 /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/dnbfein" /TN "Alacra dnbfein" /RU ""

# set up the moodys cusip load - incremental refresh - Disabled on 2017/09/26 by Dave Burke
# SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 09:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_cusip_inc" /TN "Alacra moodys_cusip_inc" /RU ""

# Moodys Issuer Attribute File Delivery - disabled per Steve O on Aug 3, 2017
#SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 16:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_issuer" /TN "Alacra moodys_issuer" /RU ""

# BCG project code update  - Disabled per Ajit on Aprl 14 - no longer a client.
#SCHTASKS /Create /F /SC DAILY /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bcg"  /TN "Alacra bcg" /RU ""

# FactivaCompany update
SCHTASKS /Create /F /SC WEEKLY /D MON /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/factivacompany" /TN "Alacra factivacompany" /RU ""

# PWC canceled DNB contract at the end of November 2018
# Set up the weekly update of ICC
#SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/iccw" /TN "Alacra iccw" /RU ""
# Set up the weekly update of ICC
#SCHTASKS /Create /F /SC WEEKLY /D TUE /ST 17:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/iccd" /TN "Alacra iccd tuesday" /RU ""
#SCHTASKS /Create /F /SC WEEKLY /D WED,THU,FRI,SAT /ST 17:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/iccd" /TN "Alacra iccd" /RU ""

# Update Thomson Insider Shareholdings
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 05:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/insider" /TN "Alacra insider" /RU ""

# Perfect Info update
SCHTASKS /Create /F /SC WEEKLY /D WED /ST 07:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/pfci_co" /TN "Alacra pfci_co" /RU ""

# WSCOPE
SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SUN /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wscopedaily" /TN "Alacra wscopedaily" /RU ""

# Set up the daily incremental update of SDC
SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SAT /ST 01:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/sdc_d" /TN "Alacra sdc_d" /RU ""

# Set up the weekly full reindex of SDC sdcco index
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/sdc_w" /TN "Alacra sdc_w" /RU ""


# Set up the Market Guide Daily Update (New Feed). On Tuesday add ipid update and on Saturday add Pricing update
SCHTASKS /Create /F /SC WEEKLY /D TUE /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/marketguided_ipid" /TN "Alacra marketguided_ipid" /RU ""
SCHTASKS /Create /F /SC WEEKLY /D WED,THU,FRI,SAT /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/marketguided" /TN "Alacra marketguided" /RU ""
#SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/marketguidep" /TN "Alacra marketguidep" /RU ""

# Set up the Market Guide Weekly Update (New Feed)
SCHTASKS /Create /F /SC MONTHLY /MO FIRST /d SUN /st 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/marketguidew" /TN "Alacra marketguidew" /RU ""

# Set up the mergerstat load - removed per Matt S on April 28, 2017
#SCHTASKS /Create /F /SC DAILY /ST 04:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mergstat" /TN "Alacra mergstat" /RU ""

# Mergstat FULL REFRESH, first SAT of the month
#SCHTASKS /Create /F /SC MONTHLY /MO FIRST /d SAT /st 10:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mergstat_full" /TN "Alacra mergstat_full" /RU ""

# Mergerstat emonthly report
#SCHTASKS /Create /F /SC MONTHLY /D 16 /ST 13:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mergstat_emonthly" /TN "Alacra mergstat_emonthly" /RU ""

#Create XML for peopleload
SCHTASKS /Create /F /SC Weekly /D SAT /ST 13:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/people_create_xml" /TN "Alacra people_create_xml" /RU ""

# people connections (must be after dmx job)
SCHTASKS /Create /F /SC Weekly /D SAT /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/peopleload" /TN "Alacra peopleload" /RU ""

#Set Up Mintel per Alex L.
SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SAT /ST 09:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mintel" /TN "Alacra mintel" /RU ""

# set up the ipid_dnb_ernc daily update, twice a day
SCHTASKS /Create /F /SC DAILY /ST 04:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/dnber1" /TN "Alacra dnber first update" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 16:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/dnber2" /TN "Alacra dnber second update" /RU ""

#upload exchange rate
SCHTASKS /Create /F /SC DAILY /ST 13:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/exRate" /TN "Alacra exRate" /RU ""

# Load sanctions lists
SCHTASKS /Create /F /SC DAILY /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/sanctions"  /TN "Alacra sanctions" /RU ""

# Swift BIC Plus from UBS load
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 08:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/swift_bic_plus" /TN "Alacra swift_bic_plus" /RU ""

# MIC
SCHTASKS /Create /F /SC DAILY /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_MIC_Mods"  /TN "Alacra MIC Mods" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 14:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_MIC_Deletes"  /TN "Alacra MIC Deletes" /RU ""

# Fitch Public Finance Issue/Issuer
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 21:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/fitchissuer_incr" /TN "Alacra fitchissuer_incr" /RU ""
SCHTASKS /Create /F /SC MONTHLY /D 2 /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/fitchissuer_full" /TN "Alacra fitchissuer_full" /RU ""

# update the IP-to-Country table in the dnb2 database every month
SCHTASKS /Create /F /SC MONTHLY /D 2 /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/refreshIP2Country" /TN "Alacra refreshIP2Country" /RU ""

# load aws result files every 15 minutes
#SCHTASKS /Create /F /SC MINUTE /MO 15 /st 00:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/cloud_matcher" /TN "Alacra cloud_matcher" /RU ""

#Investext Source Type dump from itext to concordance
SCHTASKS /Create /F /SC MONTHLY /D 25 /ST 01:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/itext_source_type_put" /TN "Alacra itext_source_type_put" /RU ""

#Investext Source Type load from concordance to itext
SCHTASKS /Create /F /SC MONTHLY /D 28 /ST 18:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/itext_source_type_get" /TN "Alacra itext_source_type_get" /RU ""

#load regulator xls files generated by Mobius
SCHTASKS /Create /F /SC MONTHLY /D 3 /ST 15:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/xlsToIpidMobius"  /TN "Alacra xlsToIpidMobius" /RU ""

#load monthsly regulator xls files
SCHTASKS /Create /F /SC MONTHLY /D 5  /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/xlsToIpid5"  /TN "Alacra xlsToIpid5" /RU ""

# Swift BIC Codes load
SCHTASKS /Create /F /SC MONTHLY /D 5 /ST 08:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bic_load" /TN "Alacra bic_load" /RU ""

# GICS concordance monthly update
SCHTASKS /Create /F /SC MONTHLY /D 7 /ST 18:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/gics" /TN "Alacra gics" /RU ""

####################################################

# FFIEC load
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 05:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ffiec_load" /TN "Alacra ffiec_load" /RU ""

# Set up the D&B City Mismatch Report
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 06:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/dnbCityMismatch" /TN "Alacra dnbCityMismatch" /RU ""

#load site agent files generated by Mobius
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 10:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/site_agents"  /TN "Alacra site_agents" /RU ""

# FSA load
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 16:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/fsa_load" /TN "Alacra fsa load" /RU ""

# Set up the ipid_ffiec_history scrape
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 19:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_ffiec_history" /TN "Alacra ipid_ffiec_history" /RU ""

# Boardex ipid load
SCHTASKS /Create /F /SC WEEKLY /D MON /ST 08:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/boardex" /TN "Alacra boardex" /RU ""

# Fitch ipid/industry index load
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 02:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/fitchindustry" /TN "Alacra fitchindustry" /RU ""

# datastream ipid scripts
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_datastream_request" /TN "Alacra ipid_datastream_request" /RU ""
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_datastream" /TN "Alacra ipid_datastream" /RU ""

# London Stock Exchange Sedols
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 21:35 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/lse_sedol" /TN "Alacra lse_sedol" /RU ""
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/lse_sedol_full" /TN "Alacra lse_sedol_full" /RU ""

# S&P V3 incremental 
#SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 16:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_spcred_inc" /TN "Alacra S&P V3 inc" /RU ""
#S&P V4 incremental 
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 16:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_spcredV4_inc" /TN "Alacra S&P V4 inc" /RU ""


# set up the ipid_edi daily update
#SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 18:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_edi" /TN "Alacra ipid_edi" /RU ""

# Daily Teledata from UBS load
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/load_teledata" /TN "Alacra load_teledata" /RU ""

# ipid Thomson Reuters through UBS  Concordance
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_TR_M"  /TN "Alacra Thomsom Reuters daily updates from UBS" /RU ""
SCHTASKS /Create /F /SC Weekly /D SUN /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_TR_I"  /TN "Alacra Thomsom Reuters weekly full refresh from UBS" /RU ""

# Telekurs load
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 22:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/telekurs_load" /TN "Alacra telekurs_load" /RU ""
SCHTASKS /Create /F /SC Weekly /D SUN /ST 22:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/telekurs_sunday_load" /TN "Alacra telekurs_sunday" /RU ""

#SIX load (Telekurs2)
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 02:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/telekurs2" /TN "Alacra SIX (telekurs2)" /RU ""


# Bloomberg load
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SUN /ST 21:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bloomberg_load" /TN "Alacra bloomberg_load" /RU ""

# RICs daily updates - disabled by AT due to Citigroup negotiations with Thomson on RICs
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SUN /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/RICsDailyUpdate" /TN "Alacra RICsDailyUpdate" /RU ""

#load regulator xls files
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 13:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/xlsToIpidWeekly"  /TN "Alacra xlsToIpidWeekly" /RU ""

# RICs weekly updates
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/RICsWeeklyUpdate" /TN "Alacra RICsWeeklyUpdate" /RU ""

# S&P V3 full 
#SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 09:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_spcred_full" /TN "Alacra S&P V3 full" /RU ""
# S&P V4 full 
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 09:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_spcredV4_full" /TN "Alacra S&P V4 full" /RU ""


#Markit load
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/markit" /TN "Alacra markit" /RU ""

#Markit Composites load
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/markit_comp" /TN "Alacra markit_comp" /RU ""

# Markit Index Weekly Load
#SCHTASKS /Create /F /SC Weekly /D SUN /ST 11:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/markit_index" /TN "Alacra markit_index" /RU ""

# SNLFI load
SCHTASKS /Create /F /SC Weekly /D SUN /ST 12:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/snlfi_load" /TN "Alacra snlfi_load" /RU ""

# Set up the kmv ipid load
SCHTASKS /Create /F /SC WEEKLY /D TUE /ST 02:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_kmv" /TN "Alacra ipid_kmv" /RU ""

#cusip load
# SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SAT /ST 03:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/cusip" /TN "Alacra cusip" /RU ""

# Set up the ambest load
SCHTASKS /Create /F /SC WEEKLY /D WED /ST 17:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ambest" /TN "Alacra ambest" /RU ""

# snapdata load
SCHTASKS /Create /F /SC MONTHLY /D 3 /ST 04:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/snapdata" /TN "Alacra snapdata#1" /RU ""
SCHTASKS /Create /F /SC MONTHLY /D 18 /ST 04:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/snapdata" /TN "Alacra snapdata#2" /RU ""

# standard bank load
SCHTASKS /Create /F /SC WEEKLY /D MON /ST 07:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/standardbank_load" /TN "Alacra standardbank_load" /RU ""

# standard bank weekly delivery 
SCHTASKS /Create /F /SC WEEKLY /D WED /ST 07:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/standardbank_send" /TN "Alacra standardbank_send" /RU ""

# standard bank daily delivery 
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 06:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/standardbank_send_ICBC_MIFID" /TN "Alacra daily standardbank_send" /RU ""

# AT&T/Equifax delivery - reenabled on Jun 30,2014 (earlier disabled per Colin D, Apr 9. 2013)
SCHTASKS /Create /F /SC MONTHLY /D 9 /ST 15:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/att_equifax_send" /TN "Alacra att_equifax_send" /RU ""

# wachovia delivery
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 05:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wachovia_send" /TN "Alacra wachovia_send" /RU ""

# wachovia load
SCHTASKS /Create /F /SC WEEKLY /D THU /ST 16:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wachovia_load" /TN "Alacra wachovia_load" /RU ""

# wachovia priority load
SCHTASKS /Create /F /SC WEEKLY /D THU /ST 16:16 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wachovia_priority_load" /TN "Alacra wachovia_priority_load" /RU ""

# master credit report file delivery
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 11:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mastercredit_send" /TN "Alacra mastercredit_send" /RU ""

# Basel Concordance
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 01:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bofabasel" /TN "Alacra bofabasel" /RU ""

# Moodys CUSIP Matching
#SCHTASKS /Create /F /SC Weekly /D SUN /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/security_matching"  /TN "Alacra security_matching" /RU ""

# OpSpark Feed
SCHTASKS /Create /F /SC Weekly /D SUN /ST 21:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/opsparkfeed"  /TN "Alacra opsparkfeed" /RU ""

# KPMG delivery
#SCHTASKS /Create /F /SC WEEKLY /D THU /ST 08:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/kpmg_send" /TN "Alacra kpmg_send" /RU ""

# Union Bank delivery
SCHTASKS /Create /F /SC Weekly /D WED /ST 08:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/unionbank_send" /TN "Alacra unionbank_send" /RU ""

#Canceled in early 2018, removed by AT
# set up the PWCIRIS INCREMENTAL delivery
#SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SAT /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/pwciris_sendINCR" /TN "Alacra PWCIRIS send data from PROD" /RU ""

#KYC FILE IMPORT - US servers
SCHTASKS /Create /F /SC Weekly /D SUN /ST 18:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/kyc"  /TN "Alacra KYC Data load - US" /RU ""

# TFshare Load
SCHTASKS /Create /F /SC WEEKLY /D WED,THU,FRI,SAT,SUN /ST 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/tfshare" /TN "Alacra tfshare" /RU ""

# Set up the weekly S&P Stock Reports ("TearSheets") - Disabled 7/26/18 - (BRS-1202)
# SCHTASKS /Create /F /SC WEEKLY /D MON /ST 01:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/tsheets" /TN "Alacra tsheets" /RU ""

#Ahuntley update
SCHTASKS /Create /F /SC WEEKLY /D MON /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ahuntley" /TN "Alacra ahuntley" /RU ""

#bmi
SCHTASKS /Create /F /SC DAILY /ST 03:04 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bmi" /TN "Alacra bmi" /RU ""

# full as well as daily updates for lionshares
SCHTASKS /Create /F /SC DAILY /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/lionshares" /TN "Alacra lionshares" /RU ""

# S&P RatingsXPress: build the inverted index every SUN
SCHTASKS /Create /F /SC MONTHLY /MO FIRST  /D SUN /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_index_odd" /TN "Alacra spcred_index_odd 1 weekend" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO SECOND /D SUN /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_index_even" /TN "Alacra spcred_index_even 2 weekend" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO THIRD  /D SUN /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_index_odd" /TN "Alacra spcred_index_odd 3 weekend" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO FOURTH /D SUN /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_index_even" /TN "Alacra spcred_index_even 4 weekend" /RU ""

#Mintel reindex
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 09:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mintel_index" /TN "Alacra mintel_index" /RU ""

# Set up Moodys Issuer File loads for concordance
SCHTASKS /Create /F /SC DAILY /ST 15:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_ftp_xml_load" /TN "Alacra moodys_ftp_xml_load" /RU ""

# Set up the monthly cik load (this is really a quarterly load, where cik.sh figures out which months to run)
SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SAT /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/cik_daily" /TN "Alacra cik_daily" /RU ""
SCHTASKS /Create /F /SC MONTHLY /D 10 /ST 03:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/cik"  /TN "Alacra cik" /RU ""

# Set up Moodys Issuer File loads for concordance
SCHTASKS /Create /F /SC DAILY /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_issuerfile_bcp" /TN "Alacra moodys_issuerfile_bcp" /RU ""

# set up the moodys eod ipid load - incremental refresh
SCHTASKS /Create /F /SC DAILY /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_moodys_eod_inc"  /TN "Alacra ipid_moodys_eod_inc" /RU ""

# Set up the Moodys Bond weekly load
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 17:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_bond" /TN "Alacra moodys bond daily" /RU ""

# Set up moodys_famtree
SCHTASKS /Create /F /SC Weekly /D SUN /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_famtree" /TN "Alacra moodys_famtree" /RU ""

# ERISA Monthly Load
SCHTASKS /Create /F /SC MONTHLY /D 1 /st 08:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/finra_scheduler2" /TN "Alacra finra_scheduler2" /RU ""

# Auto Investment Advisors
SCHTASKS /Create /F /SC MONTHLY /D 5 /st 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/auto_ia_scheduler" /TN "Alacra auto_ia_scheduler" /RU ""

# UBS LEM Daily Delivery at 19:00 - Full Refresh 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SUN /ST 19:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ubs_send_full " /TN "Alacra UBS LEM daily reports - 3rd run" /RU ""

# UBS LEM Daily Delivery at 03:00 - Incremental 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 03:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ubs_send_incr " /TN "Alacra UBS LEM daily reports - 1st run" /RU ""

# UBS LEM Daily Delivery at 11:00 - Incremental 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 11:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ubs_send_incr " /TN "Alacra UBS LEM daily reports - 2nd run" /RU ""

# UBS LEM Stats Daily Delivery at 07:00
SCHTASKS /Create /F /SC DAILY /ST 07:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ubs_lem_stats " /TN "Alacra UBS LEM stats daily report" /RU ""

# CITI Daily Delivery at 19:00 - Full Refresh 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 18:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/citi_send " /TN "Alacra CITI daily reports" /RU ""

#citi amc
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 20:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_citi_amc"  /TN "Alacra ipid_citi_amc" /RU ""

# djwatchlist ipid
SCHTASKS /Create /F /SC DAILY /ST 08:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/djrc_scheduler_33" /TN "Alacra djrc_scheduler_33" /RU ""

# ipid_GLEIF 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 11:40 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_GLEIF " /TN "Alacra ipid_GLEIF" /RU ""

# Gleifparents
SCHTASKS /Create /F /SC DAILY /ST 11:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/gleifparents " /TN "Alacra Gleifparents" /RU ""

# ESMA ipid
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 08:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipid_ESMA" /TN "Alacra ESMA" /RU ""

# Daily Snapshot rebuild and AAF base file creation
SCHTASKS /Create /F /SC DAILY /ST 16:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/snapshot"  /TN "Alacra snapshot" /RU ""

# Accenture Load
SCHTASKS /Create /F /SC MONTHLY /D 15  /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/accenture_load"  /TN "Alacra Accenture Load" /RU ""

# Accenture Send
SCHTASKS /Create /F /SC MONTHLY /D 20  /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/accenture_send"  /TN "Alacra Accenture Send" /RU ""

# Concordance update
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 21:20 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/concordance" /TN "Alacra concordance" /RU ""

# DirectMapping/Concordance copy load
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/directmapping" /TN "Alacra directmapping" /RU ""

# Daily STATESTREET AAF delivery -- Removed 17th May 2018; OP-7222
# SCHTASKS /Create /F /SC DAILY /ST 19:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/statestreet_send"  /TN "Alacra statestreet_send" /RU ""


# Weekly BOFA Vendor Attributes
SCHTASKS /Create /F /SC Weekly /D SUN /ST 19:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bofa_extract_vendor_attributes" /TN "Alacra Bofa Vendor Extract Attributes Send" /RU ""
# Daily BOFA AAF delivery
SCHTASKS /Create /F /SC DAILY /ST 16:45 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bofa_send"  /TN "Alacra bofa_send" /RU ""
# Daily ICAP AAF delivery
SCHTASKS /Create /F /SC DAILY /ST 17:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/Icap_send"  /TN "Alacra Icap_send" /RU ""
# Daily NEX AAF delivery
SCHTASKS /Create /F /SC DAILY /ST 19:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/NEX_send"  /TN "Alacra NEX_send" /RU ""
# Daily Fitch AAF delivery
SCHTASKS /Create /F /SC DAILY /ST 09:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/fitch_send"  /TN "Alacra fitch_send" /RU ""

# JBast - 2018-05-10 - Commented this out
# Daily Fitchtest AAF delivery
#SCHTASKS /Create /F /SC DAILY /ST 09:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/fitchtest_send"  /TN "Alacra fitchtest_send" /RU ""

# Daily Fitch AAF delivery
SCHTASKS /Create /F /SC MONTHLY /D 22 /ST 09:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/atradius_send" /TN "Alacra atradius_send" /RU ""


# JBast - moved from 11:45 to 12:15
# Weekly HSBC2 AAF delivery
SCHTASKS /Create /F /SC Weekly /D SUN /ST 12:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/hsbc2_send"  /TN "Alacra hsbc2_send" /RU ""

# Weekly AFG AAF delivery
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 19:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/afg_send"  /TN "Alacra afg_send" /RU ""

# Weekly ofrtreasury AAF delivery
SCHTASKS /Create /F /SC Weekly /D WED /ST 21:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ofrtreasury_send"  /TN "Alacra ofrtreasury_send" /RU ""

# Ace mailer
SCHTASKS /Create /F /SC DAILY /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ace_inbox_monitoring_emails" /TN "Alacra ACE Enhanced Monitoring Emails" /RU ""

# "WF Factiva Daily Load 
SCHTASKS /Create /F /SC DAILY /ST 00:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wellsfargo_factiva_load"  /TN "Alacra wellsfargofactiva" /RU ""

# "WF4210 Daily Load 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 21:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wells4210_load"  /TN "Alacra Wellsfinra 4210 Load" /RU ""

# "WF4210 Daily Update 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 07:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wells4210_update"  /TN "Alacra Wellsfinra 4210 Update" /RU ""

# "WF4210 Daily Send
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wells4210_send"  /TN "Alacra Wellsfinra 4210 Send" /RU ""

# Nomura RR placement
SCHTASKS /Create /F /SC DAILY /ST 00:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acre_rr"  /TN "Alacra acre_rr" /RU ""

# ACE Rolling Review
SCHTASKS /Create /F /SC DAILY /ST 01:03 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ace_rr"  /TN "Alacra ace_rr" /RU ""

# nomura Files
SCHTASKS /Create /F /SC DAILY /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/nomuraFiles"  /TN "Alacra nomuraFiles" /RU ""

# nomura Files OBI
SCHTASKS /Create /F /SC Weekly /D SUN,MON,TUE,WED,THU /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/nomuraFilesLong"  /TN "Alacra nomuraFilesLong" /RU ""

# Set up the daily alerts to be sent every hour
SCHTASKS /Create /F /SC HOURLY /MO 1 /st 00:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/alerts" /TN "Alacra mail email alerts" /RU ""

# Wells Fargo user / region update
SCHTASKS /Create /F /SC DAILY /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wf" /TN "Alacra wf" /RU ""

# Disable Wells Fargo CRMS report
#SCHTASKS /Create /F /SC DAILY /ST 09:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/crmsreport" /TN "Alacra crmsreport" /RU ""

# Disable Wells Fargo Weekly CRMS report
#SCHTASKS /Create /F /SC WEEKLY /D MON /ST 02:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/crmsreport_weekly" /TN "Alacra crmsreport_weekly" /RU ""

# Disable Wells Fargo Monthly CRMS report
#SCHTASKS /Create /F /SC MONTHLY /D 3 /ST 02:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/crmsreport_monthly" /TN "Alacra crmsreport_monthly" /RU ""

# Wells Fargo Daily CRMS report
SCHTASKS /Create /F /SC DAILY /ST 08:02 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/crmsreport_daily" /TN "Alacra crmsreport_daily" /RU ""

#WF Usage - monthly report
SCHTASKS /Create /F /SC MONTHLY /D 2 /ST 13:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/WFUsage" /TN "Alacra WFUsage" /RU ""

# Wells Fargo Monthly User report
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wellsfargo_user_report" /TN "Alacra wellsfargo_user_report" /RU ""

# Wells Fargo Daily Monitoring Report
SCHTASKS /Create /F /SC DAILY /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wellsfargo_monitoring_report" /TN "Alacra wellsfargo_monitoring_report" /RU ""

# Wells Fargo Volcker List Load
SCHTASKS /Create /F /SC DAILY /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/volckerlist_load" /TN "Alacra volckerlist_load" /RU ""

# Wells Fargo Monthly Monitoring Report
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 07:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wellsfargo_monitoring_report_monthly" /TN "Alacra wellsfargo_monitoring_report_monthly" /RU ""

# Opus Monthly Usage Report
SCHTASKS /Create /F /SC MONTHLY /D 4 /ST 09:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/opus_usage_report" /TN "Alacra Monthly Usage Report" /RU ""

# Archive Wellsfargo data
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/wellsfargo_archive" /TN "Alacra wellsfargo_archive" /RU ""
# Concordance ACE aaf events
SCHTASKS /Create /F /SC DAILY /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/concordance_ace_aaf_events" /TN "Alacra concordance_ace_aaf_events" /RU ""

# TPRM Vendor List Update Weekly Load - suspended 7/15/15 - Per WF
SCHTASKS /Create /F /SC Weekly /D WED /ST 15:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/tprm_vendor_list_load"  /TN "Alacra TPRM Weekly Vendor List Update" /RU ""

# TPRM Usage Report Monthly Delivery
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 04:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/tprm_report_send"  /TN "Alacra TPRM Usage Report Monthly Delivery" /RU ""

# BAC Usage Delivery
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 09:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/bac_usage_send " /TN "Alacra bac usage monthly" /RU ""

# Mobius Daily Reports Delivery
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 09:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/daily_reports_send " /TN "Alacra mobius daily reports" /RU ""

# Mobius QA Reports
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 19:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mobius_qa_reports " /TN "Alacra mobius qa reports" /RU ""

# Duplicate Regulator Mappings Weekly Report Delivery
SCHTASKS /Create /F /SC Weekly /D SUN /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/regulator_report_send " /TN "Alacra duplicate regulator mappings weekly report" /RU ""

# Set up the concordance statistics update
SCHTASKS /Create /F /SC WEEKLY /D MON /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/concordancestats" /TN "Alacra concordancestats" /RU ""

# Set up the amount of work for daily processing of dmo events
SCHTASKS /Create /F /SC DAILY /ST 00:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/setdmoevents" /TN "Alacra setdmoevents" /RU ""

# Run recurrent ad-hoc tasks
SCHTASKS /Create /F /SC DAILY /ST 23:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/adhoctasks" /TN "Alacra adhoctasks" /RU ""

#newclientdnb Load
SCHTASKS /Create /F /SC Monthly /D 22 /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/newclientdnb" /TN "Alacra newclientdnb" /RU ""


#dump_sitemaps_data Updates, every 2 hours
SCHTASKS /Create /F /SC HOURLY /MO 2 /st 00:35 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/dump_sitemaps_data" /TN "Alacra dump_sitemaps_data" /RU ""

#delete temp tables, check for missing jobs
SCHTASKS /Create /F /SC HOURLY /MO 1 /st 00:12 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/deltemptables" /TN "Alacra delete temp tables" /RU ""

# cleanup log files
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/cleanup_log_files" /TN "Alacra cleanup_log_files" /RU ""

# Dummy RSS file for nagios checks, hourly
SCHTASKS /Create /F /SC HOURLY /MO 1 /st 00:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/copy_nagios_rss" /TN "Alacra copy_nagios_rss" /RU ""

#Compliance batch alerts, every 15 minutes
SCHTASKS /Create /F /SC MINUTE /MO 15 /st 00:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/compbatch_alerts" /TN "Alacra compbatch_alerts" /RU ""

# Run BOFA EDD FTP SITE FILE CHECK EVERY HOUR
SCHTASKS /Create /F /SC HOURLY /MO 1 /st 00:45 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/compbatch_ftp_check" /TN "Alacra compbatch_ftp_check" /RU ""
# END BOFA EDD JOB

# Run Compliance Batch locker clean-up utility
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 11:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/compbatch_locker_cleanup" /TN "Alacra compbatch_locker_cleanup" /RU ""

# Run Compliance Batch auto-generated Court Express reports
SCHTASKS /Create /F /SC WEEKLY /D WED /ST 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/compbatch_courtexpress_reports" /TN "Alacra compbatch_courtexpress_reports#1" /RU ""
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/compbatch_courtexpress_reports" /TN "Alacra compbatch_courtexpress_reports#2" /RU ""

# Run Compliance Batch auto-generated Court Express monthly usage report
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 05:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/compbatch_courtexpress_usage_report" /TN "Alacra compbatch_courtexpress_usage_report" /RU ""

# Run Compliance Batch Pending Job Check EVERY HOUR
SCHTASKS /Create /F /SC HOURLY /MO 1 /st 00:40 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/compbatch_pending_check" /TN "Alacra compbatch_pending_check" /RU ""
# END Compliance Batch Pending Job Check

# Run Compliance Batch Court Express response file check
SCHTASKS /Create /F /SC DAILY /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/compbatch_skips_check" /TN "Alacra compbatch_skips_check" /RU ""

# Autotester
SCHTASKS /Create /F /SC DAILY /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/autotest" /TN "Alacra Autotester" /RU ""

# Union Bank Westlaw usage report
SCHTASKS /Create /F /SC MONTHLY /D 5 /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/unionbankwestlaw" /TN "Alacra unionbankwestlaw" /RU ""

# Run job to fetch automated matching jobs for concordance
SCHTASKS /Create /F /SC DAILY /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/matcher" /TN "Alacra matcher" /RU ""

# Concordance weekly cleanup
SCHTASKS /Create /F /SC WEEKLY /D SAT /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/concordance_cleanup" /TN "Alacra concordance_cleanup" /RU ""

# Set up weekly xls archive for 8AM
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/xls_archive" /TN "Alacra xls_archive" /RU ""

# usage statistics
SCHTASKS /Create /F /SC DAILY /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/usage_stat" /TN "Alacra usage_stat" /RU ""

# ip statements
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ipstatement" /TN "Alacra ipstatement" /RU ""

# Set up the daily sales reports
SCHTASKS /Create /F /SC DAILY /ST 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/salesreports" /TN "Alacra salesreports" /RU ""

# Setup AFG permissions report
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 01:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/afgperms" /TN "Alacra afgperms" /RU ""

# Set up the daily store usage report
SCHTASKS /Create /F /SC DAILY /ST 03:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/storeusagereports" /TN "Alacra storeusagereports" /RU ""

# AOD invoice accounts invoice mailing
# SCHTASKS /Create /F /SC MONTHLY /d 7 /st 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/aod_invoice_account_mail" /TN "Alacra AOD invioce accounts invoice mailing" /RU ""

# AOD usage stats for previous month; for Matt/Cat/Don
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 12:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/aod_usage_stats"  /TN "Alacra AOD Usage Stats" /RU ""

# Set up eiu usage reports
SCHTASKS /Create /F /SC WEEKLY /D MON /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/eiuusagereport" /TN "Alacra eiuusagereport" /RU ""
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 02:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/eiumonthlyusage" /TN "Alacra eiumonthlyusage" /RU ""

# Update AFG user permissions on rkdfcr datasets
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/updateAFGRkdfcrPermissions" /TN "Alacra updateAFGRkdfcrPermissions" /RU ""

# Resolve autocomplete index weekly update
SCHTASKS /Create /F /SC Weekly /D SAT /ST 18:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/lookup360" /TN "Alacra lookup360" /RU ""

# Email weekly results of sourcestat query
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/sourcestat" /TN "Alacra sourcestat" /RU ""

# CapitalIQAPI usage to ftp server
SCHTASKS /Create /F /SC DAILY /ST 00:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/CapitalIQUsage" /TN "Alacra CapitalIQUsage" /RU ""

# Set up the weekly billing check
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 21:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/billcheck" /TN "Alacra billcheck weekly" /RU ""
# Do one extra on the first of the month
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/billcheck" /TN "Alacra billcheck month" /RU ""

# Check for renewals - no longer needed, AT 11/21/2017
#SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 01:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/renewal" /TN "Alacra renewal 1" /RU ""
#SCHTASKS /Create /F /SC MONTHLY /D 15 /ST 01:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/renewal" /TN "Alacra renewal 15" /RU ""

# Set up the monthly Thomson Financial usage report
SCHTASKS /Create /F /SC MONTHLY /D 5 /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/tfnusagereports" /TN "Alacra tfnusagereports" /RU ""

# Set up the daily database reports
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 07:50 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/databasereports" /TN "Alacra databasereports" /RU ""

# Set up the Weekly Mintel usage reports
SCHTASKS /Create /F /SC WEEKLY /D MON /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/mintelusage" /TN "Alacra mintelusage" /RU ""

#Snapshot Usage from alacralog table
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 00:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/alacralog"  /TN "Alacra alacralog" /RU ""

#Thomson Book Usage report 
SCHTASKS /Create /F /SC DAILY /ST 00:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ThomsonUsage" /TN "Alacra ThomsonUsage" /RU ""

# Load DAILY data from mizuho into concordance
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 01:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/concordance_mizuhoDailyLoad" /TN "Alacra concordance_mizuhoDailyLoad" /RU ""

# Extract DAILY load for mizuho 
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 11:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/concordance_mizuhoDailySend" /TN "Alacra concordance_mizuhoDailySend" /RU ""

# Load MONTHLY data from mizuho into concordance
SCHTASKS /Create /F /SC MONTHLY /MO LASTDAY /M "*" /ST  14:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/concordance_mizuhoMonthlyLoad" /TN "Alacra concordance_mizuhoMonthlyLoad" /RU ""

# Extract MONTHLY load for mizuho 
SCHTASKS /Create /F /SC MONTHLY /MO LASTDAY /M "*" /ST 15:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/concordance_mizuhoMonthlySend" /TN "Alacra concordance_mizuhoMonthlySend" /RU ""

# S&P RatingsXPress: each evening, pull down wrap-up of previous day's research
SCHTASKS /Create /F /SC DAILY /ST 01:58 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_research" /TN "Alacra spcred_research" /RU ""

# S&P RatingsXPress: REALTIME updates occur few times a day
#SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 07:30 /ET 20:01 /RI 150 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_realtime" /TN "Alacra spcred_realtime" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 10:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_realtime" /TN "Alacra spcred_realtime#1" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 14:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_realtime" /TN "Alacra spcred_realtime#2" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 18:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_realtime" /TN "Alacra spcred_realtime#3" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/spcred_realtime" /TN "Alacra spcred_realtime last daily run" /RU ""

# Set up the Moodys GDA loads: Midnight runs 30 days back, 4AM job runs 7 days back, and hourly jobs from 8Am-8PM for current updates
SCHTASKS /Create /F /SC DAILY /ST 00:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys" /TN "Alacra moodys 30 days back" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 04:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_incr7" /TN "Alacra moodys_incr7" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 06:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_incr" /TN "Alacra moodys_incr#1" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 08:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_incr" /TN "Alacra moodys_incr#2" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 10:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_incr" /TN "Alacra moodys_incr#3" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 12:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_incr" /TN "Alacra moodys_incr#4" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 14:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_incr" /TN "Alacra moodys_incr#5" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 16:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_incr" /TN "Alacra moodys_incr#6" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 18:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_incr" /TN "Alacra moodys_incr#7" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 20:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodys_incr" /TN "Alacra moodys_incr#8" /RU ""

# FitchResearch Ratings
SCHTASKS /Create /F /SC DAILY /ST 10:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/fitch_incr" /TN "Alacra fitch_incr #1" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/fitch_incr" /TN "Alacra fitch_incr #2" /RU ""
SCHTASKS /Create /F /SC DAILY /ST 23:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/fitch_incr" /TN "Alacra fitch_incr #3" /RU ""

# Set up the twice a week BARRA polling
SCHTASKS /Create /F /SC WEEKLY /D TUE,FRI /ST 03:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/barra_new" /TN "Alacra barra_new" /RU ""

# poll for new acm data
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_poller_iad" /TN "Alacra acm_poller_iad#1" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 18:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_poller_iad" /TN "Alacra acm_poller_iad#2" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 08:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_poller_dfw" /TN "Alacra acm_poller_dfw#1" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 20:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_poller_dfw" /TN "Alacra acm_poller_dfw#2" /RU ""


# rebuild the acm full text index each week
SCHTASKS /Create /F /SC MONTHLY /MO FIRST /D SUN /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx_full_even" /TN "Alacra acm_idx_full_even" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO SECOND /D SUN /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx_full_odd" /TN "Alacra acm_idx_full_odd" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO THIRD /D SUN /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx_full_even" /TN "Alacra acm_idx_full_even#2" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO FOURTH /D SUN /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx_full_odd" /TN "Alacra acm_idx_full_odd#2" /RU ""

SCHTASKS /Create /F /SC MONTHLY /MO FIRST /D SUN /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx_full_odd" /TN "Alacra acm2_idx_full_odd" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO SECOND /D SUN /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx_full_even" /TN "Alacra acm2_idx_full_even" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO THIRD /D SUN /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx_full_odd" /TN "Alacra acm2_idx_full_odd#2" /RU ""
SCHTASKS /Create /F /SC MONTHLY /MO FOURTH /D SUN /ST 00:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx_full_even" /TN "Alacra acm2_idx_full_even#2" /RU ""

# do an incremental index of the acm database every 15 minutes throughout the day, Mon-Sat
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 00:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#1" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 02:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#2" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 04:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#3" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 06:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#4" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 08:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#5" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 10:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#6" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 12:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#7" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 14:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#8" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 16:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#9" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 18:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#10" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 20:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#11" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 22:15 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_idx" /TN "Alacra acm_idx#12" /RU ""

SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 01:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#1" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 03:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#2" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 05:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#3" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 07:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#4" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 09:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#5" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 11:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#6" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 13:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#7" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 15:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#8" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 17:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#9" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 19:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#10" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 21:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#11" /RU ""
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 23:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm2_idx" /TN "Alacra acm2_idx#12" /RU ""

# run acm matching (concordance once per day)
SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI,SAT /ST 18:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_matching" /TN "Alacra acm_matching" /RU ""

# distribute ACM resource files
SCHTASKS /Create /F /SC DAILY /ST 03:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_distribute" /TN "Alacra acm_distribute" /RU ""

# Load the moodys Moody's Capital Markets Research Group
#SCHTASKS /Create /F /SC DAILY /ST 03:01 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/moodysacm" /TN "Alacra moodysacm" /RU ""

# load idc acm collection data
SCHTASKS /Create /F /SC WEEKLY /D SUN /ST 22:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/idc_acm" /TN "Alacra idc_acm" /RU ""

# Set up the weekly S&P Poller (for acmspind and spglobacm) - Disabled 7/26/2018 (BRS-1202) 
# SCHTASKS /Create /F /SC WEEKLY /D WED /ST 16:45 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/snpweekly" /TN "Alacra snpweekly" /RU ""

# Set up the daily Euromonitor acm load script
SCHTASKS /Create /F /SC WEEKLY /D MON /ST 13:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_euromonitor" /TN "Alacra acm_euromonitor" /RU ""

# Set up the daily Privco acm load script - disabled by AT 5/1/2017 , no longer active
#SCHTASKS /Create /F /SC MONTHLY /D 25 /ST 13:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/acm_privco" /TN "Alacra acm_privco" /RU ""

#deals for db - disabled by AT 4/18/2016, no longer active collection
#SCHTASKS /Create /F /SC DAILY /ST 03:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/dbdeals" /TN "Alacra dbdeals" /RU ""

#Morning Star Earnings Reports
# per BRS-1199 SCHTASKS /Create /F /SC Weekly /D MON,TUE,WED,THU,FRI /ST 23:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/morningstarearnings" /TN "Alacra morningstarearnings" /RU ""

#Morning Star Equity Reports
SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SAT /ST 01:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/morningstar" /TN "Alacra morningstar" /RU ""

#Morning Star Credit Reports
SCHTASKS /Create /F /SC WEEKLY /D TUE,WED,THU,FRI,SAT /ST 06:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/morningstarcredit" /TN "Alacra morningstarcredit" /RU ""

#Morning Star Mutual Funds report
SCHTASKS /Create /F /SC MONTHLY /D 15 /ST 09:10 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/morningstarmf" /TN "Alacra morningstarmf" /RU ""

# Resolve AAFPlus index weekly update
SCHTASKS /Create /F /SC Weekly /D SAT /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/resolve_aafplus" /TN "Alacra Resolve AAFPlus" /RU ""

# Resolve LEDBBG index weekly update
SCHTASKS /Create /F /SC Weekly /D SUN /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/resolve_ledbbg" /TN "Alacra Resolve LEDBBG" /RU ""

#Lexisnexis World Compliance Full refresh
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 22:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/lnwcFull" /TN "Alacra lnwcFull" /RU ""

#Lexisnexis World Compliance Incremental update for today
SCHTASKS /Create /F /SC WEEKLY /D MON,TUE,WED,THU,FRI /ST 22:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/lnwc" /TN "Alacra lnwc" /RU ""

#Lexisnexis World Compliance Incremental update for today
SCHTASKS /Create /F /SC MONTHLY /D 3 /ST 22:05 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/file_load_audit_report_smbc" /TN "Alacra FLARsmbc" /RU ""

#Email audit report
SCHTASKS /Create /F /SC MONTHLY /D 1 /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/file_load_audit_report_smbc_nikko" /TN "Alacra SMBC_Nikko" /RU ""

#C6 Weekly Full Refresh Sundays Sundays Only
SCHTASKS /Create /F /SC Weekly /D SUN /ST 14:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/c6weekly" /TN "Alacra C6FSundaysOnly" /RU ""

#C6 Daily Incremental Update 
SCHTASKS /Create /F /SC Weekly /D TUE,WED,THU,FRI,SAT,SUN /ST 02:30 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/c6daily" /TN "Alacra C6IDaily" /RU ""

# Companieshouse monthly update full
SCHTASKS /Create /F /SC MONTHLY /D 10 /ST 01:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/companieshouse" /TN "Alacra Companieshouse" /RU ""

##################
# ANZ_CHARM jobs #
##################

# ANZ_CHARM - send subscription list to BvD. PROD instance.
# NOTE: This process can only run from PROD (not TEST or STAGE)
SCHTASKS /Create /F /SC WEEKLY /D TUE /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ANZ_CHARM_BVD_Subscription_List_PROD" /TN "Alacra ANZ_CHARM BvD Subscription List PROD" /RU ""

# ANZ_CHARM - wait for BvD files to be produced, then fetch them. PROD instance.
# This runs on TEST, STAGE and PROD
SCHTASKS /Create /F /SC WEEKLY /D WED /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ANZ_CHARM_BVD_Wait_And_Fetch_PROD" /TN "Alacra ANZ_CHARM await and fetch BVD files PROD" /RU ""

# ANZ_CHARM - weekly update cycle. PROD instance.
# This runs on PROD, for now. TEST and STAGE run manually as needed.
SCHTASKS /Create /F /SC WEEKLY /D FRI /ST 06:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ANZ_CHARM_Weekly_Update_PROD" /TN "Alacra ANZ_CHARM weekly update PROD" /RU ""

# ANZ_CHARM - housekeeping script - PROD instance.
# This runs after every successful update. On PROD, this is on Monday at 14:00. TEST and STAGE run manually as needed.
SCHTASKS /Create /F /SC WEEKLY /D MON /ST 14:00 /TR "loadjob.exe c:/users/xls/src/scripts/jobstreams/rackspace/ANZ_CHARM_Housekeeping_PROD" /TN "Alacra ANZ_CHARM Housekeeping PROD" /RU ""

}

rm -f *.log
egrep -i "^(SCHTASKS.*jobstreams)" atlist_rackspace.sh |grep -v jobstreams/rackspace
if [ $? -eq 0 ]; then
  echo
  echo
  echo
  echo "FOUND JOBS WITH WRONG DIRECTORY LISTED IN ${scriptname}"
  echo "ALL JOBS SHOULD BE in jobstreams/rackspace folder"
  echo "Log file name is $logfile"
  exit 1
fi

egrep -i "^(SCHTASKS.*jobstreams)" atlist_rackspace.sh |grep -v Alacra
if [ $? -eq 0 ]; then
  echo
  echo
  echo
  echo "FOUND JOBS WITH WRONG TASK NAME LISTED IN ${scriptname}"
  echo "ALL JOBS SHOULD BE have 'Alacra' in its task name"
  echo "Log file name is $logfile"
  exit 1
fi

logfile=atlist`date +%m%d`.log
echo "Logfile is: $logfile"
(run) > $logfile 2>&1
rc=$?

#Get Number of jobs listed in this file, and the number loaded into scheduler
typeset -i loaded
typeset -i listed
loaded=`SCHTASKS /query /NH |grep -v "^$"|grep Alacra|wc -l`
listed=`egrep -i "^(SCHTASKS /Create) " ${scriptname}|grep -iv " /delete"|wc -l`

if [ $rc -ne 0 ]
then
  #SCHTASKS /F /Delete /TN "*" > /dev/null
  echo
  echo
  echo
  echo "LAST JOB FAILED TO LOAD.  LOADED ONLY $loaded JOBS OUT OF $listed LISTED IN ${scriptname}"
  echo "TAKE A LOOK AT ITS CALL, AND TRY TO FIX IT"
  echo "Log file name is $logfile"
elif [ $loaded -ne $listed ]
then
  #If number of jobs loaded is different from listed, report a problem
  #This could happen if the same job is listed more than once for SCHTASKS
  echo
  echo
  echo "POSSIBLE ERRORS IN LOADING JOBS"
  echo "Number of jobs loaded ($loaded) mismatched number of listed jobs ($listed)."
  echo "All other jobs are scheduled to run, please review the difference for possible problems."
  echo "Log file name is $logfile"
  echo ""
  echo "Here's the difference between whats in the task scheduler and in :"
  SCHTASKS /query /NH |grep -v "^$"|grep Alacra|cut -c1-40 |sort > SCHTASKS.txt
  egrep -i "^(SCHTASKS /Create)" atlist_rackspace.sh|grep -iv " /delete"|awk -v FS="/TN" '{print $2}' |sed 's/^ "//g;s/" \/RU ""//g'|cut -c1-40 |sort > atlist.txt
  diff -ib SCHTASKS.txt atlist.txt
else
  echo "$loaded jobs loaded successfully"
  echo "Do you want to inspect the log file?(Y/N)"
  read zzz
  if [ "$zzz" = "y" -o "$zzz" = "Y" ]
  then
    echo "Log file name is $logfile"
  else
    rm -f $logfile
  fi
fi

exit 0


