origin|select distinct NameDetailsDJW.id, NameDetailsDJW.entityName, NameDetailsDJW.nameType
						from NameDetailsDJW
						where NameDetailsDJW.entityName is not null
						and NameDetailsDJW.nameType != 'Primary Name'
						union
						select distinct n.id, n.entityName, NameTypeListSOC.value
						from NameDetailsSOC n
						left join NameTypeListSOC on n.nameType = NameTypeListSOC.NameTypeId
						where nameType != '1'
						and entityName is not null
