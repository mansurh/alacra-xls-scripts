exec ipid_moodys_eod_python_proc
--------------------------------
-- =============================================    
-- Author:  Moodys and Friends    
-- Create date: 8/29/2016    
-- Description: Creates or updates ipid_moodys_eod_python_proc    
-- =============================================    
    
-- =============================================  
-- Author:  Moodys and Friends  
-- Create date: 8/29/2016  
-- Description: Creates or updates ipid_moodys_eod_python_proc  
-- =============================================  
  
Create PROCEDURE [dbo].[ipid_moodys_eod_python_proc]  
AS      
BEGIN      
      
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ipid_moodys_eod_python]') AND type in (N'U'))      
DROP TABLE [dbo].[ipid_moodys_eod_python]      
      
CREATE TABLE #a(      
cusip varchar(50),      
isin varchar(50),      
common_code varchar(9),      
cedel varchar(7),      
cf_pf_indicator varchar(1),      
moodys_issuer_number varchar(10),      
moodys_deal_number varchar(10),      
moodys_debt_number varchar(10),      
moodys_unique_number varchar(10),      
org_issuer_name varchar(255),      
security_desc varchar(255),      
debt_class varchar(3),      
currency varchar(5),      
coupon varchar(50),      
maturity_date varchar(8),      
seniority varchar(3),      
shelf_registration_type varchar(4096),      
support varchar(1),      
endorsement_indicator varchar(255),      
office_code varchar(4096),      
withdrawal_reason_code varchar(255),      
unsolicited_indicator varchar(255),      
reserved_future_use1 varchar(15),      
indicator varchar(5),      
reserved_future_use2 varchar(50),      
reserved_future_use3 varchar(1),      
current_rating varchar(12),      
rating_date varchar(50),      
rating_direction varchar(4096),      
rating_type varchar(3),      
watchlist_indicator varchar(3),      
watchlist_date varchar(4096),      
watchlist_reason varchar(3),      
redenomination_indicator varchar(1),      
previous_isin varchar(12),      
batch_number varchar(6),      
batch_date varchar(8),      
action_code varchar(1),      
correction_code varchar(40)      
) ON [PRIMARY]      
      
-- distinct grabs all Moodys_Rating_ID along with other columns in the same table      
-- ***moodys debt number is different in the old tables      
-- moodys_unique_number has changed for many records from 1.8 to 2.0 with no way to convert?
INSERT INTO #a(moodys_debt_number, moodys_unique_number,debt_class, seniority, shelf_registration_type, current_rating, rating_date, rating_direction, rating_type)      
SELECT   
CASE     
when Instrument_ID > -1    
THEN replicate('0',10-len(Instrument_ID))+convert(varchar,Instrument_ID) 
ELSE Instrument_ID   
END,   
CASE     
when Moodys_Rating_ID > -1     
THEN replicate('0',10-len(Moodys_Rating_ID))+convert(varchar,Moodys_Rating_ID)     
ELSE Moodys_Rating_ID   
END, Security_Class_Short_Description, Seniority_Short_Description, Shelf_Type_Text,      
Rating_Text, CONVERT(VARCHAR(10), convert(datetime, Rating_Local_Date), 112), Rating_Direction_Short_Description, Rating_Type_Short_Description      
FROM CFG_moodysload_instrument_ratings      
where Rating_Termination_Date IS NULL       
      
UPDATE #a      
SET cf_pf_indicator='C'      
      
UPDATE #a      
SET cusip= cmii.Instrument_ID_Value      
FROM CFG_moodysload_instrument_identifiers cmii      
WHERE convert(int,moodys_debt_number)=cmii.Instrument_ID and cmii.Id_Type_Short_Description = 'CUS'      
      
UPDATE #a      
SET isin= cmii.Instrument_ID_Value      
FROM CFG_moodysload_instrument_identifiers cmii      
WHERE convert(int,moodys_debt_number)=cmii.Instrument_ID and cmii.Id_Type_Short_Description = 'ISI'      
      
      
      
      
UPDATE #a      
SET moodys_deal_number= cmirr.Deal_number,      
security_desc=cmirr.Security_Description,      
coupon=cmirr.Coupon_Rate,      
maturity_date=REPLACE(CONVERT (VARCHAR(10), cmirr.Maturity_Date, 112), '-','')      
FROM CFG_moodysload_instrument_rating_root cmirr      
WHERE convert(int,moodys_debt_number)=Instrument_ID      
-- moodys_deal_number is 000000000 for those not populated      
UPDATE #a      
SET moodys_deal_number='000000000'      
WHERE moodys_deal_number IS NULL      
  
      
UPDATE #a      
SET moodys_issuer_number =  
CASE     
WHEN Organization_ID > -1     
THEN replicate('0',10-len(Organization_ID))+convert(varchar,Organization_ID)     
ELSE Organization_ID   
END,      
org_issuer_name=cmio.Moodys_Legal_Name      
FROM CFG_moodysload_instrument_organization cmio      
WHERE cmio.Organization_Role_Code=129      
AND convert(int,moodys_debt_number)=cmio.Instrument_ID      
      
      
UPDATE #a      
SET currency=cmirr.ISO_Currency_Code      
FROM CFG_moodysload_instrument_rating_root cmirr      
WHERE convert(int,moodys_debt_number)=cmirr.Instrument_ID      
      
--support      
UPDATE #a      
SET support='N'      
UPDATE #a      
SET support='Y'      
FROM CFG_moodysload_instrument_support cmis      
where cmis.Support_Termination_Date is null      
OR cmis.Support_Termination_Date > GETDATE()      
AND convert(int,moodys_debt_number)=cmis.Instrument_ID      
      
/*      
select support from ipid_moodys_eod      
      
select distinct instrument_ID into #z from CFG_moodysload_instrument_support      
where Support_Termination_Date is null      
or Support_Termination_Date > GETDATE()      
      
select instrument_ID from #z      
where not exists (select moodys_debt_number from #a where #a.moodys_debt_number=#z.Instrument_ID)      
      
select distinct Instrument_ID from CFG_moodysload_instrument_ratings      
WHERE shadow_code in (2655,5734)      
*/      
      
/*      
select * from ipid_moodys_eod z      
inner join #a a      
on a.moodys_debt_number=z.moodys_debt_number      
      
select moodys_debt_number from #a order by moodys_debt_number      
select moodys_debt_number from ipid_moodys_eod order by moodys_debt_number      
      
select moodys_debt_number,support from ipid_moodys_eod where support = 'N' order by moodys_debt_number      
0000004244      
select * from #a where moodys_debt_number=10004244      
*/      
      
      
UPDATE #a      
SET endorsement_indicator=(case
 when Rating_Attribute_Text = 'EU Rated' then '1A'
 when Rating_Attribute_Text = 'EU Endorsed' then '1B'
 when Rating_Attribute_Text = 'EU Qualified by Extension' then '1C'
 end)      
FROM CFG_moodysload_instrument_ratings_attribute cmira      
WHERE Rating_Attribute_Type_Code=5156627      
AND Rating_Attribute_Type_Text='EU Endorsement'      
and convert(int,moodys_unique_number)=cmira.Moodys_Rating_ID       
      
UPDATE #a      
SET withdrawal_reason_code=cmira.Rating_Attribute_Text      
FROM CFG_moodysload_instrument_ratings_attribute cmira      
WHERE Rating_Attribute_Type_Code=5156692      
and convert(int,moodys_unique_number)=cmira.Moodys_Rating_ID      
      
/*      
unsolicited indicator      
select Instrument_ID, Moodys_Rating_ID, Rating_Attribute_Type_Code, Rating_Attribute_Type_Text, Rating_Attribute_Text      
from CFG_moodysload_instrument_ratings_attribute where Rating_Attribute_Type_Code in (5156628,5159693, 999999)      
*/      
      
/*      
UPDATE #a      
SET rating_outlook=cmira.Rating_Attribute_Text      
FROM CFG_moodysload_instrument_ratings_attribute cmira      
WHERE Rating_Attribute_Type_Code=5160019      
AND Termination_Date IS NULL      
AND moodys_unique_number=cmira.Moodys_Rating_ID      
*/      
      
      
      
UPDATE #a      
SET indicator=cmia.Rating_Attribute_Type_Text      
FROM CFG_moodysload_instrument_ratings_attribute cmia      
WHERE Rating_Attribute_Type_Code in (5156438, 5156630)      
AND Termination_Date IS NULL      
AND convert(int,moodys_debt_number)=cmia.Instrument_ID      
      
/*      
UPDATE #a      
SET rating_outlook_date=cmira.Effective_Date      
FROM CFG_moodysload_instrument_ratings_attribute cmira      
WHERE moodys_unique_number=cmira.Moodys_Rating_ID      
*/      
      
      
UPDATE #a      
SET watchlist_indicator='ON'      
FROM CFG_moodysload_instrument_watchlist cmiw      
WHERE cmiw.Watchlist_Termination_Date IS NULL      
AND convert(int,moodys_unique_number)=cmiw.Moodys_Rating_ID      
UPDATE #a      
SET watchlist_indicator='OFF'      
FROM CFG_moodysload_instrument_watchlist cmiw      
WHERE cmiw.Watchlist_Termination_Date IS NOT NULL     
AND Watchlist_Direction_Code <> 720      
AND convert(int,moodys_unique_number)=cmiw.Moodys_Rating_ID      
UPDATE #a      
SET watchlist_indicator='CFO'      
FROM CFG_moodysload_instrument_watchlist cmiw      
WHERE cmiw.Watchlist_Termination_Date IS NOT NULL      
AND Watchlist_Direction_Code = 720      
AND convert(int,moodys_unique_number)=cmiw.Moodys_Rating_ID      
      
UPDATE #a      
SET watchlist_date=REPLACE(CONVERT (VARCHAR(10), cmiw.Watchlist_Local_Date, 112), '-',''),      
watchlist_reason=cmiw.Watchlist_Direction_Short_Description      
FROM CFG_moodysload_instrument_watchlist cmiw      
WHERE convert(int,moodys_unique_number)=cmiw.Moodys_Rating_ID      
      
UPDATE #a      
SET previous_isin=cmii.Instrument_ID_Value      
FROM CFG_moodysload_instrument_identifiers cmii      
WHERE ID_Type_Code=24988      
AND convert(int,moodys_debt_number)=cmii.Instrument_ID      
      
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
/*      
batch number      
batch date      
action code      
correction code      
*/      
      
      
/**********************************************************************************************/      
      
CREATE TABLE #b(      
cusip varchar(50),      
isin varchar(50),      
common_code varchar(9),      
cedel varchar(7),      
cf_pf_indicator varchar(1),      
moodys_issuer_number varchar(10),      
moodys_deal_number varchar(10),      
moodys_debt_number varchar(10),      
moodys_unique_number varchar(10),      
org_issuer_name varchar(255),      
security_desc varchar(255),      
debt_class varchar(3),      
currency varchar(5),      
coupon varchar(50),      
maturity_date varchar(8),      
seniority varchar(3),      
shelf_registration_type varchar(4096),      
support varchar(1),      
endorsement_indicator varchar(255),      
office_code varchar(4096),      
withdrawal_reason_code varchar(255),      
unsolicited_indicator varchar(255),      
reserved_future_use1 varchar(15),      
indicator varchar(5),      
reserved_future_use2 varchar(50),      
reserved_future_use3 varchar(1),      
current_rating varchar(12),      
rating_date varchar(50),      
rating_direction varchar(4096),      
rating_type varchar(3),      
watchlist_indicator varchar(3),      
watchlist_date varchar(4096),      
watchlist_reason varchar(3),      
redenomination_indicator varchar(1),      
previous_isin varchar(12),      
batch_number varchar(6),      
batch_date varchar(8),      
action_code varchar(1),      
correction_code varchar(40)      
) ON [PRIMARY]      
      
INSERT INTO #b(moodys_issuer_number, moodys_unique_number, debt_class, seniority, current_rating, rating_date, rating_direction, rating_type)      
SELECT CASE     
when Organization_ID > -1     
THEN replicate('0',10-len(Organization_ID))+convert(varchar,Organization_ID)     
ELSE Organization_ID END, CASE     
when Moodys_Rating_ID > -1     
THEN replicate('0',10-len(Moodys_Rating_ID))+convert(varchar,Moodys_Rating_ID)     
ELSE Moodys_Rating_ID END AS moodys_unique_number, Security_Class_Short_Description, Seniority_Short_Description,      
Rating_Text, CONVERT(VARCHAR(10), CONVERT(datetime, Rating_Local_Date), 112), Rating_Direction_Short_Description, Rating_Type_Short_Description      
FROM CFG_moodysload_organization_ratings WHERE      
Rating_Termination_Date is null order by Organization_ID      
      
UPDATE #b      
SET cusip= CASE      
 WHEN cmoi.Id_Type_Short_Description='CUS' THEN cmoi.Organization_ID_Value      
 END,      
 isin= CASE      
 WHEN cmoi.Id_Type_Short_Description='ISI' THEN cmoi.Organization_ID_Value      
 END      
FROM CFG_moodysload_organization_identifiers cmoi      
WHERE convert(int,moodys_issuer_number)=cmoi.Organization_ID      
      
--cf/pf indicator      
--select Organization_ID, Organization_CFG_Indicator, Organization_PFG_Indicator from CFG_moodysload_organization_rating_root      
      
      
      
UPDATE #b      
SET cf_pf_indicator='C',      
org_issuer_name=cmorr.Moodys_Legal_Name,      
currency=cmorr.Organization_Currency_ISO_Code      
FROM  CFG_moodysload_organization_rating_root cmorr      
WHERE cmorr.Organization_CFG_Indicator='Y'      
AND convert(int,moodys_issuer_number)=cmorr.Organization_ID      
      
UPDATE #b      
SET cf_pf_indicator='P',      
org_issuer_name=cmorr.Moodys_Legal_Name,      
currency=cmorr.Organization_Currency_ISO_Code      
FROM  CFG_moodysload_organization_rating_root cmorr      
WHERE cmorr.Organization_PFG_Indicator='Y'      
AND convert(int,moodys_issuer_number)=cmorr.Organization_ID      
      
UPDATE #b      
SET support='N'      
UPDATE #b      
SET support='Y'      
FROM CFG_moodysload_organization_ratings cmor      
WHERE cmor.Rating_Termination_Date IS NULL      
AND cmor.Shadow_Code=2655      
OR cmor.Shadow_Code=5734      
AND convert(int,moodys_unique_number)=cmor.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmor.Organization_ID      
      
UPDATE #b      
SET endorsement_indicator=(case
 when Rating_Attribute_Text = 'EU Rated' then '1A'
 when Rating_Attribute_Text = 'EU Endorsed' then '1B'
 when Rating_Attribute_Text = 'EU Qualified by Extension' then '1C'
 end)      
FROM CFG_moodysload_organization_ratings_attribute cmora      
WHERE cmora.Termination_Date IS NULL      
AND cmora.Rating_Attribute_Type_Code=5156627      
AND convert(int,moodys_unique_number)=cmora.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmora.Organization_ID      
      
UPDATE #b      
SET withdrawal_reason_code=Rating_Attribute_Text      
FROM CFG_moodysload_organization_ratings_attribute cmora      
WHERE cmora.Termination_Date IS NULL      
AND cmora.Rating_Attribute_Type_Code=5156692      
AND convert(int,moodys_unique_number)=cmora.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmora.Organization_ID      
      
      
UPDATE #b      
SET unsolicited_indicator=Rating_Attribute_Text      
FROM CFG_moodysload_organization_ratings_attribute cmora      
WHERE cmora.Termination_Date IS NULL      
AND cmora.Rating_Attribute_Type_Code in (5156628, 5159693, 999999)      
AND convert(int,moodys_unique_number)=cmora.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmora.Organization_ID      
      
      
UPDATE #b      
SET indicator='(sf)'      
FROM CFG_moodysload_organization_ratings_attribute cmora      
WHERE cmora.Termination_Date IS NULL      
AND cmora.Rating_Attribute_Type_Code=5156438      
AND convert(int,moodys_unique_number)=cmora.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmora.Organization_ID      
      
UPDATE #b      
SET indicator='(hyb)'      
FROM CFG_moodysload_organization_ratings_attribute cmora      
WHERE cmora.Termination_Date IS NULL      
AND cmora.Rating_Attribute_Type_Code=5156630      
AND convert(int,moodys_unique_number)=cmora.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmora.Organization_ID      
      
UPDATE #b      
SET indicator='(LD)'      
FROM CFG_moodysload_organization_ratings_attribute cmora      
WHERE cmora.Termination_Date IS NULL      
AND cmora.Rating_Attribute_Type_Code=5160446      
AND convert(int,moodys_unique_number)=cmora.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmora.Organization_ID      
      
UPDATE #b      
SET watchlist_indicator='ON',      
watchlist_date=REPLACE(CONVERT (VARCHAR(10), cmow.Watchlist_Local_Date, 112), '-',''),      
watchlist_reason=cmow.Watchlist_Direction_Short_Description      
FROM CFG_moodysload_organization_watchlist cmow      
WHERE Watchlist_Termination_Date IS NULL      
AND convert(int,moodys_unique_number)=cmow.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmow.Organization_ID      
      
      
UPDATE #b      
SET watchlist_indicator='OFF',      
watchlist_date=REPLACE(CONVERT (VARCHAR(10), cmow.Watchlist_Local_Date, 112), '-',''),      
watchlist_reason=cmow.Watchlist_Direction_Short_Description      
FROM CFG_moodysload_organization_watchlist cmow      
WHERE Watchlist_Termination_Date IS NOT NULL      
AND cmow.Watchlist_Direction_Code <>720      
AND convert(int,moodys_unique_number)=cmow.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmow.Organization_ID      
      
UPDATE #b      
SET watchlist_indicator='CFO',      
watchlist_date=REPLACE(CONVERT (VARCHAR(10), cmow.Watchlist_Local_Date, 112), '-',''),      
watchlist_reason=cmow.Watchlist_Direction_Short_Description      
FROM CFG_moodysload_organization_watchlist cmow      
WHERE Watchlist_Termination_Date IS NULL      
AND cmow.Watchlist_Direction_Code=720      
AND convert(int,moodys_unique_number)=cmow.Moodys_Rating_ID      
AND convert(int,moodys_issuer_number)=cmow.Organization_ID      
      
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
/*      
batch number      
batch date      
action code      
      
correction code      
*/      
      
/**********************************************************************************************/      
      
CREATE TABLE ipid_moodys_eod_python(      
cusip varchar(50),      
isin varchar(50),      
common_code varchar(9),      
cedel varchar(7),      
cf_pf_indicator varchar(1),      
moodys_issuer_number varchar(10),      
moodys_deal_number varchar(10),      
moodys_debt_number varchar(10),      
moodys_unique_number varchar(10),      
org_issuer_name varchar(255),      
security_desc varchar(255),      
debt_class varchar(3),      
currency varchar(5),      
coupon varchar(50),      
maturity_date varchar(8),      
seniority varchar(3),      
shelf_registration_type varchar(4096),      
support varchar(1),      
endorsement_indicator varchar(255),      
office_code varchar(4096),      
withdrawal_reason_code varchar(255),      
unsolicited_indicator varchar(255),      
reserved_future_use1 varchar(15),      
indicator varchar(5),      
reserved_future_use2 varchar(50),      
reserved_future_use3 varchar(1),      
current_rating varchar(12),      
rating_date varchar(50),      
rating_direction varchar(4096),      
rating_type varchar(3),      
watchlist_indicator varchar(3),      
watchlist_date varchar(4096),      
watchlist_reason varchar(3),      
redenomination_indicator varchar(1),      
previous_isin varchar(12),      
batch_number varchar(6),      
batch_date varchar(8),      
action_code varchar(1),      
correction_code varchar(40)      
) ON [PRIMARY]      
      
INSERT INTO ipid_moodys_eod_python      
SELECT * FROM #a      
UNION      
SELECT * FROM #b      

update ipid_moodys_eod_python
set moodys_debt_number = replicate('0',10-len(convert(int,moodys_debt_number)-10000000))+convert(varchar,convert(int,moodys_debt_number)-10000000)
where moodys_debt_number < 800000000 and moodys_debt_number > -1
        
update ipid_moodys_eod_python
set rating_direction = 'AFF' 
where rating_direction = 'AFFIRM'
      
end