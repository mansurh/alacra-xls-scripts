exec moodys_org_python_proc
---------------------------
ALTER PROCEDURE moodys_org_python_proc    
AS            
BEGIN            
        
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[org_python_table]') AND type in (N'U'))        
DROP TABLE [dbo].[org_python_table]            
      
create table org_python_table (      
[id] int,
[name] varchar(255)    
)      
            
insert into org_python_table(id)      
select distinct Organization_ID  
from CFG_moodysload_organization_rating_root 


update org_python_table      
set [name] = CFG_moodysload_organization_rating_root.Moodys_Legal_Name     
from  CFG_moodysload_organization_rating_root     
where  CFG_moodysload_organization_rating_root.Organization_ID = [id] 

END