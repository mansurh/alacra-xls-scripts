from db_retriever import DB_Retriever
from file_hand import File_Hand
import argparse
import sys
import os
import time
import traceback
from log_me import logger

def main():
	#command line parsing
	def msg(name='ipid_load'):
		return "python " + name + ".py TODO msg()"
		
	parser = argparse.ArgumentParser(prog='ipid_load', usage=msg())
	parser.add_argument('client_dir', type=str)
	parser.add_argument('origin', type=str)
	parser.add_argument('destination', type=str, nargs='?')
	parser.add_argument('--tablename', type=str, metavar='', nargs='?')
	parser.add_argument('--max_dev', type=str, metavar='', nargs='?')
	parser.add_argument('--stage', type=str, metavar='', nargs='?')
	parser.add_argument('--origin_server', type=str, metavar='', nargs='?')
	parser.add_argument('--dest_server', type=str, metavar='', nargs='?')
	
	args = parser.parse_args()
	client_dir = args.client_dir
	stage = 'prod'
	if args.stage:
		stage = args.stage
	
	scriptdir = os.environ.get('XLS') + '/src/scripts/ReferencePython/generic_loading_tables/'
	loaddir = loaddir = os.environ.get('XLSDATA') + '/' + client_dir + '/'
	unix_command = 'mkdir -p ' + loaddir + '/log'
	print(os.popen(unix_command).read())
	today = time.strftime('%Y%m%d')
	unix_command = 'mkdir -p ' + loaddir + '/back/' + today
	print(os.popen(unix_command).read())
	
	logging = None
	if args.tablename:
		logging = logger(args.tablename.replace("'","") + '_python_.log',loaddir,loaddir+'log/',stage)
	else:
		logging = logger(args.client_dir + '_python_.log',loaddir,loaddir+'log/',stage)
	fh = File_Hand(client_dir,logging)
	
	print(args)
	logging.info(str(args))
	
	origin = args.origin
	destination = args.destination
	if destination == None: destination = 'concordance'
	files = None
	if args.tablename:
		files = fh.get_arg_files(client_dir, args.tablename.replace("'",""))
	else:
		files = fh.get_arg_files(client_dir, None)
	max_deviation = 20
	if args.max_dev: max_deviation = int(args.max_dev)
	
	for key in files:
		file_name = key + '.txt'
		file_name_err = key + '.err'
		files[key] = sorted(files[key])
		logging.info("Argument files are: " + str(files))
		try:
			columns = fh.get_table_columns(scriptdir + 'arg_files/' + client_dir + '/' + files[key][0])   # create_ipid_db_client.txt
			sql_command = fh.get_sql_command(scriptdir + 'arg_files/' + client_dir + '/' + files[key][1])   # load_ipid_db_client.txt
		except IndexError:
			logging.error("Create and/or Load file(s) are formatted incorrectly, do not exist, or are in the incorrect directory.")
			traceback.print_exc()
		try:
			param = fh.get_parser_param(scriptdir + 'arg_files/' + client_dir + '/' + files[key][2])   # parse_ipid_db_client.txt
		except IndexError:
			param = None
		try:
			procedure = fh.get_procedure(scriptdir + 'arg_files/' + client_dir + '/' + files[key][3])   # procedure_ipid_db_client.txt
		except IndexError:
			procedure = None
	
		sql_command = sql_command.replace('\n','').split('<><>')
		origin_commands = []
		destination_commands = []
		cmd_types = ['upda','dele','exec','with']
		for sql_cmd in sql_command:
			if sql_cmd.split('|',1)[0] == 'origin':
				origin_commands += [sql_cmd.split('|',1)]
			elif sql_cmd.split('|',1)[0] == 'destination':
				destination_commands += [sql_cmd.split('|',1)]
			else:
				print(sql_command)
				print("ERROR: invalid sql command parsed from load file")
		
		djw_worker = DB_Retriever(origin, columns, sql_command, client_dir, logging) # origin database worker
		creds = djw_worker.get_creds("Data Servers")
		origin_server = args.origin_server
		if origin_server:
			creds['dba_server'] = origin_server
		djw_conn = djw_worker.open_connection(creds) # origin connection
		logging.info("Established connection with the origin database")
		djw_worker.set_connection(djw_conn)
		
		con_worker = DB_Retriever(destination, columns, sql_command, client_dir, logging) # destination database worker
		creds = con_worker.get_creds("Data Servers") # credentials for the destination db
		dest_server = args.dest_server
		if dest_server:
			creds['dba_server'] = dest_server
		con_conn = con_worker.open_connection(creds) # destination connection
		logging.info("Established connection with the destination database")
		con_worker.set_connection(con_conn)
		#print creds / sys.exit()
		con_worker.create_table(key,'_new') # key = table name, columns come from the create file
		logging.info("Creating new table: " + key + "_new")
		data = None
		if procedure != None:
			djw_worker.execute_command(procedure) # runs the procedure if there is one in the procedure file
			logging.info("Procedure has been run: " + procedure)
			
		for sql_command in origin_commands:
			if sql_command[1][0:6] == 'select':
				data = djw_worker.get_data(sql_command[1]).fetchall()
			else:
				djw_worker.execute_command(sql_command[1])
			#else:
			#	print("error - sql_command didnt start w/ select or update: " + sql_command[1][0:4])
			#	sys.exit()
			
		#data = djw_worker.get_data(sql_command).fetchall() # data from the table resulting from the load file
		logging.info("Data from the load file has been fetched: " + str(len(data)))
		if param != None: # parse or no parse?
			parsed = fh.parser(param[0],param[1],param[2].split(','), data)
			data = fh.update_data_with_parsed(param[0],data,parsed)
			logging.info("Data has been parsed using the parse file.")
		fh.put_data_in_file(file_name, data)
		logging.info("BCP upload file has been created.")
		
		unix_command = 'bcp ' + destination + '..' + con_worker.myTable +  '_new in ' + loaddir + file_name + ' -U' + creds['dba_user'] + ' -P' + creds['dba_pass'] + ' -S' + creds['dba_server'] + ' -c -t"|" -CRAW -e ' + loaddir + file_name_err
		print(os.popen(unix_command).read())
		logging.info("Data has been uploaded to the table via BCP.")
		for sql_command in destination_commands:
			if sql_command[1][0:6] == 'select':
				data = con_worker.get_data(sql_command[1]).fetchall()
			else:
				con_worker.execute_command(sql_command[1])
		
		
		if con_worker.find_deviation(con_conn, max_deviation):
			con_worker.rename_tables()
			logging.info("Deviation test has passed, tables have been renamed.")
		else:
			logging.error("Deviation test stopped the system.")
			
		logging.info("Load directories have been created.")
		if fh.check_file_size(file_name, 50):
			fh.move_files(file_name)
			#for key in files:
				#fh.move_arg_files(client_dir, files[key])
			logging.info("Files have been moved to their load directory.\n\n")
		else:
			logging.info('File size check has stopped the system.')
			
	logging.end_success()
	logging.info("Script completed successfully.")
	print("\nScript completed successfully.\n")
	sys.exit(0);

if __name__ == '__main__':
	main()
	
	