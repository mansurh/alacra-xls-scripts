client_dir=""
origin=""
destination="concordance"
tablename=""
stage="--stage 'prod'"
dest_server=""
origin_server=""
max_dev=""

if [ $# -gt 0 ]
then
  client_dir=$1 
fi
if [ $# -gt 1 ]
then
  origin=$2
fi
if [ $# -gt 2 ]
then
  destination=$3 
fi
if [ $# -gt 3 ]
then
  tablename="--tablename $4" 
fi
if [ $# -gt 4 ]
then
  stage="--stage $5"
fi
if [ $# -gt 5 ]
then
  dest_server="--dest_server $6"
fi
if [ $# -gt 6 ]
then
  origin_server="--origin_server $7"
fi
if [ $# -gt 7 ]
then
  max_dev="--max_dev $8"
fi

loaddir="$XLSDATA/${client_dir}"
logfile="${loaddir}/sh_log.log"
mkdir -p ${loaddir}


main()
{
 echo "python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/ipid_loader.py ${client_dir} ${origin} ${destination} ${tablename} ${stage} ${dest_server} ${origin_server} ${max_dev}"
 python3 ${XLS}/src/scripts/ReferencePython/generic_loading_tables/ipid_loader.py ${client_dir} ${origin} ${destination} ${tablename} ${stage} ${dest_server} ${origin_server} ${max_dev}

}
(main) > ${logfile} 2>&1
if [ $? -ne 0 ] 
then
 
    #blat ${LOGFILE} -t "administrators@alacra.com","daniel.lundergreen@alacra.com" -f "reference-data-feed@alacra.com" -s "Moodys loading Delivery Failed "
	#blat ${LOGFILE} -t "matthew.pitcher@alacra.com" -f "reference-data-feed@alacra.com" -s "Moodys loading Delivery Failed "
    exit 1
fi

exit 0