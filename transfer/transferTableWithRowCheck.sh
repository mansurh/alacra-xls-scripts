
XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn

if [ $# -lt 1 ]
then
	echo "Usage: transferTableWithRowCheck.sh dbname_from dbname_to component [SkipCountCheckFlag 1/0]"
	exit 1
fi

if [ $# -eq 3 ]
then
	skipCountCheck=0
else
	skipCountCheck=$4  #1 - skip check
fi

echo "Hello!"
dbname_from=$1
dbname_to=$2
comp=$3

srcloc=${XLS}/src/scripts/loading/transferScript
out_dir=$XLSDATA/$dbname_from_$comp
echo $out_dir
mkdir -p $out_dir

run() {
cd $out_dir
server_to=`get_xls_registry_value ${dbname_to} "Server" "0"`
user_to=`get_xls_registry_value ${dbname_to} "User" "0"`
pass_to=`get_xls_registry_value ${dbname_to} "Password" "0"`

server_from=`get_xls_registry_value ${dbname_from} "Server" "0"`
user_from=`get_xls_registry_value ${dbname_from} "User" "0"`
pass_from=`get_xls_registry_value ${dbname_from} "Password" "0"`

echo "Database from:           $dbname_from"
echo "Database to:             $dbname_to"

#dbname_from=${dbname_from}${comp}
tempfile=${dbname_from}_${comp}.dat

echo "bcp select * from ${comp} ..."
bcp "select * from ${comp}" queryout ${tempfile} -S$server_from -U$user_from -P$pass_from -c -t"|"
if [ $? -ne 0 ]
then
	echo "Error bcp-out select * from ${comp}, exiting ..."
	return 1
fi

echo "create new table ${comp}_new ..."
isql /S${server_to} /U${user_to} /P${pass_to} /Q "drop table ${comp}_new"

isql /S${server_to} /U${user_to} /P${pass_to} /b /Q "select top 0 * into ${comp}_new from ${comp} "
if [ $? -ne 0 ]
then
	echo "Error creating new table ${comp}_new, exiting ..."
	return 1
fi

bcp ${comp}_new in ${tempfile} -S$server_to -U$user_to -P$pass_to -c -t"|"
if [ $? -ne 0 ]
then
	echo "Error in bcp ${dbname_to} ${comp}_new, exiting ..."
	return 1
fi

echo "skip row count check flag: ${skipCountCheck}"
if  [ ! ${skipCountCheck} == '1' ]
then
	typeset -i filecount=`wc -l ${tempfile} |cut -f1 -d" "`
	echo "Row count check"
	$srcloc/check_row_counts.sh ${server_to} ${user_to} ${pass_to} ${comp} ${filecount}
	if [ $? -ne 0 ]
	then
		echo "ERROR: ${comp} count failed, exiting ..."
		return 1
	fi
else
	echo "Skipping the row count check"
fi

#echo "create index ${comp}01"
#isql /S${server_to} /U${user_to} /P${pass_to} /Q "CREATE NONCLUSTERED INDEX [${comp}] ON [dbo].[${comp}_new] ([${comp}_value] ASC) WITH (SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF) ON [PRIMARY] " /b
#if [ $? -ne 0 ]
#then
#	echo "Error creating index ${comp}01 on ${comp}_new, exiting ..."
#	return 1
#fi

isql /b /S${server_to} /U${user_to} /P${pass_to} /Q "drop table ${comp}_backup"

isql /S${server_to} /U${user_to} /P${pass_to} /b << HERE
exec sp_rename '${comp}', '${comp}_backup'
go
exec sp_rename '${comp}_new', '${comp}'
go
HERE
if [ $? -ne 0 ]
then
	echo "Error renaming tables, exiting ..."
	return 1
fi

echo "DONE"
}

filename=$out_dir/${dbname_from}_$comp`date +%y%m%d`.log
run > ${filename} 2>&1

if [ $? -ne 0 ]
then
	#blat ${filename} -t administrators@alacra.com -s "${dbname_from} $comp update failed."
	exit 1
else
	#blat ${filename} -t administrators@alacra.com -s "${dbname_from} $comp update."
	#rm ${filename}
	exit 0
fi

exit 0


