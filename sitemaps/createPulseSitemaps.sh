XLSUTILS=$XLS/src/scripts/loading/dba
. $XLSUTILS/get_registry_value.fn
. $XLSUTILS/check_return_code.fn

if [ $# -lt 1 ]
then
	echo "usage: $0 server"
	exit 1
fi

server=$1
localdatabase=weblogdb
localdbuser=`get_xls_registry_value ${localdatabase} User`
localdbpass=`get_xls_registry_value ${localdatabase} Password`

database=sitemapdb
siteserver=`get_xls_registry_value ${database} Server`
siteuser=`get_xls_registry_value ${database} User`
sitepass=`get_xls_registry_value ${database} Password`

run() {
#  $XLS/src/scripts/loading/dba/startupdate.sh ${localdatabase} ${COMPUTERNAME} ${localdatabase}
#  check_return_code $? "error in startupdate.sh, exiting ..." $?

  # where is the index located?
  currentloc=`reginvidx get ${localdatabase}`

  scratch=${currentloc%/*}
  indexdir=${scratch}

  cd $indexdir
  mkdir -p new
  cd new
  check_return_code $? "error in cd to ${indexdir}/new, exiting ..." $?

  rm -f *smx*

  perl $XLS/src/scripts/sitemaps/createPulseSitemaps.pl ${server} ${localdatabase} ${localdbuser} ${localdbpass} $COMPUTERNAME
  check_return_code $? "error in perl createPulseSitemaps.pl, exiting ..." $?

  #sanity check (make sure you have at least 2 files)
#  if [ `ls *smx* |wc -l` -lt 2 ]
#  then
#	echo "createPulseSitemaps.pl created less than 2 sitemaps, exiting ..."
#	exit 1
#  fi

  test -s sitemap.sql
  check_return_code $? "sitemap.sql is empty, exiting ..." $?

  isql /S${siteserver} /U${siteuser} /P${sitepass} /b < sitemap.sql
  check_return_code $? "error running sitemap.sql for street pulse data, exiting ..." $?

  #remove old sitemap index files
  rm -f ${indexdir}/${localdatabase}_${localdatabase}_*.smx*
  mv ${localdatabase}_${localdatabase}*_0??.smx* ${indexdir}/
  check_return_code $? "error moving pulse sitemaps to $indexdir, exiting ..." $?

#  $XLS/src/scripts/loading/dba/endupdate.sh ${localdatabase} ${COMPUTERNAME} ${localdatabase}
  echo "complete"

}

mkdir -p $XLSDATA/${localdatabase}
logfilename=$XLSDATA/${localdatabase}/sitemapindex`date +%y%m%d`.log
`run > ${logfilename} 2>&1`
returncode=$?
if [ $returncode -ne 0 ]
then
	blat ${logfilename} -t administrators@alacra.com -s "Pulse Sitemap generation failed"
	exit ${returncode}
fi
